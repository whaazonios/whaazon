//
//  SliderTests.swift
//  SliderTests
//
//  Created by Zhan, C. (Can) on 8/8/17.
//  Copyright © 2017 Zhan, C. (Can). All rights reserved.
//

import XCTest
@testable import Slider

class SliderTests: XCTestCase {
    
    func testTimeScaleAndLabelGenerator() {
        print("************ begin generating timeMarkers with beginX: 1051275.5, endX: 1052299.5, actualBeginTimeInterval: 1514815140.0, actualEndTimeInterval: 1514876580.0")
        let timeMarkers = TimeScaleAndLabelGenerator.generateTimeScale(beginX: 1051275.5, endX: 1052299.5, actualBeginTimeInterval: 1514815140.0, actualEndTimeInterval: 1514876580.0, timeZone: TimeZone.current)
        print("************ timeMarkers = \(timeMarkers)")
    }
    
}
