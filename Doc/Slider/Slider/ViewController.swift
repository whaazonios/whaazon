//
//  ViewController.swift
//  Slider
//
//  Created by Zhan, C. (Can) on 8/8/17.
//  Copyright © 2017 Zhan, C. (Can). All rights reserved.
//

import UIKit
import CoreGraphics

class ViewController: UIViewController {

    @IBOutlet weak var sliderView: SliderView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let now = Date()
    
    var startOfToday: Date!
    var twoYearsAgo: Date!
    var twoYearsLater: Date!
    // distance from screen left to middle
    var halfScreenWidth: CGFloat!
    var delegate: SliderViewDelegate?
    
    fileprivate var startTime: Date?
    fileprivate var endTime: Date?

    var totalTimeRangeInSeconds: Int {
        return Int(twoYearsLater.timeIntervalSince(twoYearsAgo))
    }
    
    var startOfTodaySinceTwoYearsAgoInSeconds: Int {
        return -Int(twoYearsAgo.timeIntervalSince(startOfToday))
    }
    
    var lastContentOffsetX: CGFloat = 0
    var currentContentOffsetX: CGFloat = 0
    var isPinching = false
    var isDragging = false
    var isDecelerating = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Dates initial values
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current
        startOfToday = calendar.startOfDay(for: now)
        twoYearsAgo = Date(timeInterval: -(3600 * 24 * 365 * 2), since: startOfToday)
        twoYearsLater = Date(timeInterval: (3600 * 24 * 365 * 2), since: startOfToday)
        // Views initial values
        sliderView.initialDate = twoYearsAgo
        sliderView.delegate = self
        scrollView.delegate = self
        halfScreenWidth = sliderView.bounds.width / 2
        refreshSliderView()
        setupSliderView()
    }
    
    private func refreshSliderView(centerTimeInSeconds: CGFloat? = nil) {
        // scroll view content size with four years time in seconds range
        scrollView.contentSize = CGSize(width: CGFloat(totalTimeRangeInSeconds) / sliderView.unitPerPoint, height: sliderView.frame.height)
        if let centerTimeInSeconds = centerTimeInSeconds {
            // keep slider view center position points to the same time while zooming
            let offsetX = centerTimeInSeconds / sliderView.unitPerPoint - halfScreenWidth
            scrollView.contentOffset = CGPoint(x: offsetX, y: scrollView.contentOffset.y)
        } else {
            // initial slider view left edge position needs to be current time
            scrollView.contentOffset = CGPoint(x:  CGFloat(startOfTodaySinceTwoYearsAgoInSeconds) / sliderView.unitPerPoint, y: scrollView.contentOffset.y)
        }
        currentContentOffsetX = scrollView.contentOffset.x
        lastContentOffsetX = currentContentOffsetX
        sliderView.firstNumberOnImage = lastContentOffsetX * sliderView.unitPerPoint
//        print("3. time of first number on image = \(Date(timeInterval: Double(sliderView.firstNumberOnImage), since: sliderView.initialDate!))")
    }
    
    private func setupSliderView() {
        let sliderView = UIView(frame: CGRect(origin: CGPoint.zero, size: scrollView.contentSize))
        sliderView.backgroundColor = .clear
        scrollView.addSubview(sliderView)
    }
    
    @IBAction func pinchedScrollView(_ recognizer: UIPinchGestureRecognizer) {
        if recognizer.state == .began {
            isPinching = true
        } else if recognizer.state == .changed {
//            print("************* scale = \(recognizer.scale), transform.scale.x = \((recognizer.view?.layer.value(forKeyPath: "transform.scale.x"))!)")
            guard let recognizerView = recognizer.view,
                let currentScale = recognizer.view?.layer.value(forKeyPath: "transform.scale.x") as? CGFloat
                else { return }
            let minScale: CGFloat = 1.0, maxScale: CGFloat = 2.0, zoomSpeed: CGFloat = 0.5
            var deltaScale = recognizer.scale
            deltaScale = ((deltaScale - 1) * zoomSpeed) + 1
            deltaScale = min(deltaScale, maxScale / currentScale)
            deltaScale = max(deltaScale, minScale / currentScale)
//            print("************* deltaScale = \(deltaScale)")
            
            let zoomTransform = recognizerView.transform.scaledBy(x: deltaScale, y: 1)
            recognizerView.transform = zoomTransform
            
            // the sliderView center time before pinching
            let lastCenterTimeInSeconds = (scrollView.contentOffset.x + halfScreenWidth) * sliderView.unitPerPoint
            
//            print("************* sliderView.unitPerPoint before = \(sliderView.unitPerPoint)")
            if recognizer.scale > 1 {
                sliderView.unitPerPoint = max(sliderView.minUnitPerPoint, sliderView.unitPerPoint / recognizer.scale)
            } else {
                sliderView.unitPerPoint = min(sliderView.maxUnitPerPoint, sliderView.unitPerPoint / recognizer.scale)
            }
//            print("************* sliderView.unitPerPoint after = \(sliderView.unitPerPoint)")
            refreshSliderView(centerTimeInSeconds: lastCenterTimeInSeconds)
            
            recognizer.scale = 1
            
        } else if recognizer.state == .ended {
            if abs(recognizer.scale - 1) >= 0.5 {
                if recognizer.scale < 1 {
                    sliderView.unitPerPoint = min(sliderView.maxUnitPerPoint, sliderView.unitPerPoint * 2)
                } else {
                    sliderView.unitPerPoint = max(sliderView.minUnitPerPoint, sliderView.unitPerPoint / 2)
                }
                refreshSliderView()
            }
            isPinching = false
        } else if recognizer.state == .cancelled || recognizer.state == .failed {
            isPinching = false
        }
    }

}

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentContentOffsetX = scrollView.contentOffset.x
        let horizontalVelocity = currentContentOffsetX - lastContentOffsetX
        sliderView.horizontalVelocity = horizontalVelocity
        lastContentOffsetX = scrollView.contentOffset.x
        guard !isPinching, !isDecelerating, !isDragging else { return }
        guard let startTime = startTime, let endTime = endTime else { return }
        sliderViewDidSlide2(startTime: startTime, endTime: endTime)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDragging = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isDragging = false
        print("scrollViewDidEndDragging")
        guard !isPinching, !isDecelerating, !isDragging else { return }
        guard let startTime = startTime, let endTime = endTime else { return }
        sliderViewDidSlide2(startTime: startTime, endTime: endTime)
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        isDecelerating = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isDecelerating = false
        print("scrollViewDidEndDecelerating")
        guard !isPinching, !isDecelerating, !isDragging else { return }
        guard let startTime = startTime, let endTime = endTime else { return }
        sliderViewDidSlide2(startTime: startTime, endTime: endTime)
    }
    func sliderViewDidSlide2(startTime: Date, endTime: Date) {
//        self.startTime = startTime
//        self.endTime = endTime
//        print("isPinching = \(isPinching), isDecelerating = \(isDecelerating), isDragging = \(isDragging)")
//        print("************ begin generating timeMarkers with beginX: \(Double(scrollView.contentOffset.x)), endX: \(Double(scrollView.contentOffset.x + sliderView.bounds.width)), actualBeginTimeInterval: \(startTime.timeIntervalSince1970), actualEndTimeInterval: \(endTime.timeIntervalSince1970)")
//        let timeMarkers = TimeScaleAndLabelGenerator.generateTimeScale(beginX: 0.0,
//                                                                       endX: Double(sliderView.bounds.width),
//                                                                       actualBeginTimeInterval: startTime.timeIntervalSince1970,
//                                                                       actualEndTimeInterval: endTime.timeIntervalSince1970,
//                                                                       timeZone: TimeZone.current)
//        print("************ timeMarkers = \(timeMarkers)")
    }
}

extension ViewController: SliderViewDelegate {
    func sliderViewDidSlide(startTime: Date, endTime: Date) {
        self.startTime = startTime
        self.endTime = endTime
//        print("isPinching = \(isPinching), isDecelerating = \(isDecelerating), isDragging = \(isDragging)")
//        print("************ begin generating timeMarkers with beginX: \(Double(scrollView.contentOffset.x)), endX: \(Double(scrollView.contentOffset.x + sliderView.bounds.width)), actualBeginTimeInterval: \(startTime.timeIntervalSince1970), actualEndTimeInterval: \(endTime.timeIntervalSince1970)")
//        let timeMarkers = TimeScaleAndLabelGenerator.generateTimeScale(beginX: 0.0,
//                                                                       endX: Double(sliderView.bounds.width),
//                                                                       actualBeginTimeInterval: startTime.timeIntervalSince1970,
//                                                                       actualEndTimeInterval: endTime.timeIntervalSince1970,
//                                                                       timeZone: TimeZone.current)
//        print("************ timeMarkers = \(timeMarkers)")
    }
}
