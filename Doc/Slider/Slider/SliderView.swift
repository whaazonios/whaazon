//
//  SliderView.swift
//  Slider
//
//  Created by Zhan, C. (Can) on 14/8/17.
//  Copyright © 2017 Zhan, C. (Can). All rights reserved.
//

import UIKit
import CoreGraphics

protocol SliderViewDelegate: class {
    /// The time presented by slider view's left and right edges
    func sliderViewDidSlide(startTime: Date, endTime: Date)
}

@IBDesignable class SliderView: UIView {
    
    private struct ScaleHeight {
        static let small: CGFloat = 8
        static let medium: CGFloat = 16
        static let large: CGFloat = 25
        static let label: CGFloat = 20
    }
    
    private struct LabelFontSize {
        static let minor: CGFloat = 8
        static let major: CGFloat = 10
        static let superr: CGFloat = 12
        static let none: CGFloat = 0
    }
    
    private typealias Scale = (x: CGFloat, y: CGFloat, textWithColor: TextWithColor)
    private typealias TextWithColor = (text: String?, fontSize: CGFloat?, color: UIColor?)
    
    private var startTimeLabel: UILabel?
    private var endTimeLabel: UILabel?
    /// Format the start/end date label
    private var dateLabelFormatter: DateFormatter?
    /// Format the date/time on top of each scale
    private var scaleDateFormatter: DateFormatter?
    
    weak var delegate: SliderViewDelegate?
    var initialDate: Date?
    let minUnitPerPoint: CGFloat = 5
    let maxUnitPerPoint: CGFloat = 60 * 24 * 30
    
    /// The time interval since initialDate at the left edge of image
    var firstNumberOnImage: CGFloat = 0 {
        didSet {
            if let initialDate = initialDate,
                firstNumberOnImage > 0 {
//                print("1. time of first number on image = \(Date(timeInterval: Double(firstNumberOnImage), since: initialDate))")
                imageLeftOffset = offsetInPointsOfCeiling(date: Date(timeInterval: Double(firstNumberOnImage), since: initialDate))
//                print("2. time of first number on image = \(Date(timeInterval: Double(firstNumberOnImage), since: initialDate))")
            }
        }
    }
    
    var horizontalVelocity: CGFloat = 0 {
        didSet {
            let remainder = Int(horizontalVelocity) % Int(inset)
            firstNumberOnImage += CGFloat(remainder) * unitPerPoint
        }
    }

    /// The number of time intervals a point on the image represents. Default value is 1 min.
    var unitPerPoint: CGFloat = 60 {
        didSet {
            print("redraw with new unitPerPoint = \(unitPerPoint)")
            drawScales()
        }
    }
    
    /// The time that the left edge of slider view represents
    private var visibleStartTime: Date?
    /// The time that the right edge of slider view represents
    private var visibleEndTime:Date?
    private var imageWidth = UIScreen.main.bounds.width
    private var imageHeight: CGFloat = 64
    let inset: CGFloat = 60
    private let scaleWidth: CGFloat = 1
    
    private var imageView: UIImageView?
    
    private var imageLeftOffset: CGFloat = 0 {
        didSet {
            drawScales()
        }
    }
    
    // MARK: - Drawing line
    
    override func draw(_ rect: CGRect) {
        let divider = UIView(frame: CGRect(x: 0, y: frame.height, width: imageWidth, height: 1))
        divider.backgroundColor = UIColor.black
        addSubview(divider)
        setupLabels()
    }
    
    private func setupLabels() {
        if startTimeLabel == nil {
            startTimeLabel = UILabel()
        }
        if endTimeLabel == nil {
            endTimeLabel = UILabel()
        }
        guard let startTimeLabel = startTimeLabel, let endTimeLabel = endTimeLabel else { return }
        addSubview(startTimeLabel)
        addSubview(endTimeLabel)
        bringSubview(toFront: startTimeLabel)
        bringSubview(toFront: endTimeLabel)
        let startTimeLabelLeadingConstraint = NSLayoutConstraint(item: startTimeLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 8)
        let startTimeLabelTopConstraint = NSLayoutConstraint(item: startTimeLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)
        let endTimeLabelTrailingConstraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: endTimeLabel, attribute: .trailing, multiplier: 1.0, constant: 8)
        let endTimeLabelTopConstraint = NSLayoutConstraint(item: endTimeLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)
        addConstraints([startTimeLabelLeadingConstraint, startTimeLabelTopConstraint, endTimeLabelTrailingConstraint, endTimeLabelTopConstraint])
        startTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        endTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        startTimeLabel.font = UIFont.systemFont(ofSize: 12)
        endTimeLabel.font = UIFont.systemFont(ofSize: 12)
        // set initial values
        if let initialDate = initialDate {
            let newStartTime = Date(timeInterval: Double(firstNumberOnImage), since: initialDate)
            let newEndTime = Date(timeInterval: Double(firstNumberOnImage + imageWidth * unitPerPoint), since: initialDate)
            startTimeLabel.text = dateString(of: newStartTime)
            endTimeLabel.text = dateString(of: newEndTime)
            // trigger a initial delegate call to notify delegate about the initial start/end time
            delegate?.sliderViewDidSlide(startTime: newStartTime, endTime: newEndTime)
        }
    }
    
    // MARK: - Drawing lines & texts
    
    /// Use the new TimeScaleAndLabelGenerator
    private func drawScales() {
        guard let initialDate = initialDate else { return }
        
        // update delegate with new dates
        let newStartTime = Date(timeInterval: Double(firstNumberOnImage), since: initialDate)
        let newEndTime = Date(timeInterval: Double(firstNumberOnImage + imageWidth * unitPerPoint), since: initialDate)
        startTimeLabel?.text = dateString(of: newStartTime)
        endTimeLabel?.text = dateString(of: newEndTime)
        if visibleStartTime != newStartTime || visibleEndTime != newEndTime {
            visibleStartTime = newStartTime
            visibleEndTime = newEndTime
            delegate?.sliderViewDidSlide(startTime: newStartTime, endTime: newEndTime)
        }
        
        // generate time scales
//        print("************ begin generating timeMarkers with beginX: \(0.0), endX: \(bounds.width), actualBeginTimeInterval: \(Date(timeIntervalSince1970: newStartTime.timeIntervalSince1970)), actualEndTimeInterval: \(Date(timeIntervalSince1970: newEndTime.timeIntervalSince1970))")
        let timeMarkers = TimeScaleAndLabelGenerator.generateTimeScale(beginX: 0.0,
                                                                       endX: Double(bounds.width),
                                                                       actualBeginTimeInterval: newStartTime.timeIntervalSince1970,
                                                                       actualEndTimeInterval: newEndTime.timeIntervalSince1970,
                                                                       timeZone: TimeZone.current)
        
        imageHeight = bounds.height
        let imageViewRect = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        if imageView == nil {
            imageView = UIImageView(frame: imageViewRect)
            imageView?.contentMode = .center
            addSubview(imageView!)
        }
        guard let imageView = imageView else { return }
        let imageSize = CGSize(width: imageWidth, height: imageHeight)
        // clear all old text layers
        layer.sublayers = layer.sublayers?.filter({ (layer) -> Bool in
            return !(layer is CATextLayer)
        })
        
        UIGraphicsBeginImageContext(imageSize)
        if let context = UIGraphicsGetCurrentContext() {
            context.setLineCap(.round)
            context.setLineWidth(scaleWidth)
            context.setBlendMode(.normal)
            // clean canvas
            context.setFillColor(UIColor.white.cgColor)
            context.fill(imageViewRect)
            // draw scales
            context.setStrokeColor(UIColor.blue.cgColor)
            
            // do the drawing
            for tm in timeMarkers {
//                print("************ timeMarker = \(tm)")
                let scaleHeight: CGFloat
                switch tm.scaleStyle {
                case .small: scaleHeight = ScaleHeight.small
                case .medium: scaleHeight = ScaleHeight.medium
                case .large: scaleHeight = ScaleHeight.large
                case .none: scaleHeight = bounds.height
                }
                let scaleFromPoint = CGPoint(x: CGFloat(tm.position), y: bounds.height)
                let scaleToPoint = CGPoint(x: CGFloat(tm.position), y: bounds.height - scaleHeight)
                drawLineFrom(fromPoint: scaleFromPoint, toPoint: scaleToPoint, with: TextWithColor(text: tm.label, fontSize: 10.0, color: .blue), on: context)
                
                let fontSize: CGFloat, textColor: UIColor
                switch tm.labelStyle {
                case .minor:
                    fontSize = LabelFontSize.minor
                    textColor = .gray
                case .major:
                    fontSize = LabelFontSize.major
                    textColor = .black
                case .superr:
                    fontSize = LabelFontSize.superr
                    textColor = .blue
                case .none:
                    fontSize = LabelFontSize.none
                    textColor = .white
                }
                let labelY = bounds.height - scaleHeight - CGFloat(ScaleHeight.label)
                let size = tm.label.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: fontSize)])
                let labelRect = CGRect(x: CGFloat(tm.position) - size.width / 2, y: labelY, width: size.width, height: CGFloat(ScaleHeight.label))
                drawText(textWithColor: TextWithColor(text: tm.label, fontSize: fontSize, color: textColor), at: labelRect)
            }
            imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
    }
    
    private func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint, with textWithColor: TextWithColor, on context: CGContext) {
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        context.strokePath()
    }
    
    private func drawText(textWithColor: TextWithColor, at rect:CGRect) {
        guard let text = textWithColor.text,
            let color = textWithColor.color,
            let fontSize = textWithColor.fontSize
            else { return }
        let textLayer = CATextLayer()
        textLayer.frame = rect
        textLayer.foregroundColor = color.cgColor
        textLayer.font = UIFont.systemFont(ofSize: 1)
        textLayer.fontSize = fontSize
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.string = text
        layer.addSublayer(textLayer)
    }
    
    /// offset in seconds by ceiling given date to next hour/time unit
    private func offsetInSecondsOfCeiling(date: Date) -> TimeInterval {
        let unit = Int(inset * unitPerPoint)
        return Double(unit - Int(date.timeIntervalSince1970) % unit)
    }
    
    private func offsetInPointsOfCeiling(date: Date) -> CGFloat {
        return CGFloat(offsetInSecondsOfCeiling(date: date)) / unitPerPoint
    }

    /// determine what text to display for a date and what the color should it be, used by divider
    private func dateString(of timeInterval: Double) -> TextWithColor {
        guard let initialDate = initialDate else { return (nil, nil, nil) }
        let displayString: String?
        let displayColor: UIColor
        let displayingDate = Date(timeInterval: timeInterval, since: initialDate)
        if scaleDateFormatter == nil {
            scaleDateFormatter = DateFormatter()
        }
        // display date for 0:00am of a day and hours for the others
        if unitPerPoint * inset >= 3600 * 24 {
            // if one inset represents one or more whole days, show days only
            scaleDateFormatter?.dateStyle = .short
            scaleDateFormatter?.timeStyle = .none
            displayColor = .black
        } else if Int(displayingDate.timeIntervalSince1970 + 10 * 3600) % (3600 * 24) == 0 {
            scaleDateFormatter?.dateStyle = .short
            scaleDateFormatter?.timeStyle = .none
            displayColor = .black
        } else {
            scaleDateFormatter?.dateStyle = .none
            scaleDateFormatter?.timeStyle = .short
            displayColor = .blue
        }
        displayString = scaleDateFormatter?.string(from: Date(timeInterval: timeInterval, since: initialDate))
        return TextWithColor(displayString, 10.0, displayColor)
    }
    
    /// format a date time string for start/end date labels
    private func dateString(of date: Date) -> String? {
        if dateLabelFormatter == nil {
            dateLabelFormatter = DateFormatter()
            dateLabelFormatter?.dateStyle = .medium
            dateLabelFormatter?.timeStyle = .short
        }
        return dateLabelFormatter?.string(from: date)
    }
    
}
