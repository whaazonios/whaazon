//
//  UIView+Extension.swift
//  whaazon
//
//  Created by Can Zhan on 9/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Apply border to view. Default to grey round border.
    func applyBorder(_ borderWidth: CGFloat = 1.0, _ borderColor: UIColor = .lightGray, _ cornerRadius: CGFloat = 5.0) {
        clipsToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.cornerRadius = cornerRadius
    }
}
