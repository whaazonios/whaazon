//
//  PropertyFilterViewController.swift
//  whaazon
//
//  Created by Can on 25/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit


let attributeCellIdentifier = "attributeCellIdentifier"
let attributeOptionCellIdentifier = "attributeOptionCellIdentifier"

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Filter
    
    /// Switch Domain Filters
    @objc func switchFilter(gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            if gestureRecognizer.direction == .up {
                userDefaults.filteringDomain = userDefaults.previousFilteringDomain
            } else if gestureRecognizer.direction == .down {
                userDefaults.filteringDomain = userDefaults.nextFilteringDomain
            } else {
                return
            }
            // refresh filter attributes and options
            FilterManager.setCurrentFilterAttribute(nil)
            refreshAttributeAndOptionView()
            filterAndRefreshEvents()
        }
    }
    
    // MARK: - FilterView related methods
    
    /**
    Refresh both Attribute and AttributeOption views based on new domain selection
    */
    func refreshAttributeAndOptionView() {
        filterAttributeCollectionView.reloadData()
        refreshAttributeOptionView()
    }
    
    /**
     Show proper Attribute Option View based on current selected filter attribute type
     */
    func refreshAttributeOptionView() {
        if let attribute = FilterManager.getCurrentFilterAttribute() {
            if attribute.isEqualComparatorAttribute || attribute.isInComparatorAttribute {
                // refresh options collections views if touching attribute is 'equal' type
                setupOptionViewWithFilterAttribute(attribute)
            } else if attribute.isRangeComparatorAttribute {
                setupRangeSliderViewWithFilterAttribute(attribute)
            }
            updateAnnotationsPinImage()
        }
    }
    
    fileprivate func setupOptionViewWithFilterAttribute(_ attribute: FilterAttribute) {
        self.filterView.bringSubview(toFront: self.filterAttributeOptionView)
        self.filterAttributeOptionView.isHidden = false
        self.filterAttributeRangeSliderView.isHidden = true
        
        filterAttributeOptionCollectionView.reloadData()
    }
    
    fileprivate func setupRangeSliderViewWithFilterAttribute(_ attribute: FilterAttribute) {
        self.filterView.bringSubview(toFront: self.filterAttributeRangeSliderView)
        self.filterAttributeOptionView.isHidden = true
        self.filterAttributeRangeSliderView.isHidden = false
        
        filterAttributeRangeSlider.delegate = self
        filterAttributeRangeSlider.maxValue = Float(attribute.maxValue ?? 0)
        filterAttributeRangeSlider.minValue = Float(attribute.minValue ?? 0)
        filterAttributeRangeSlider.selectedMaximum = Float(attribute.selectedMaxValue ?? 0)
        filterAttributeRangeSlider.selectedMinimum = Float(attribute.selectedMinValue ?? 0)
        filterAttributeRangeSlider.step = Float(attribute.stepValue ?? 0)
        filterAttributeRangeSlider.updateLabelPositions()
    }
    
    // MARK: - UICollectionViewDataSource & UICollectionViewDelegateFlowLayout for collection views
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isAttributeCollectionView(collectionView) ? attributeCollectionViewCount() : attributeOptionCollectionViewCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return isAttributeCollectionView(collectionView) ? attributeCollectionView(collectionView, cellForItemAtIndexPath: indexPath) : attributeOptionCollectionView(collectionView, cellForItemAtIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return isAttributeCollectionView(collectionView) ? attributeCollectionView(collectionView, didSelectItemAtIndexPath: indexPath) : attributeOptionCollectionView(collectionView, didSelectItemAtIndexPath: indexPath)
    }
    
    fileprivate func isAttributeCollectionView(_ collectionView: UICollectionView) -> Bool {
        return collectionView.tag == filterAttributeCollectionViewTag
    }
    
    fileprivate func attributeCollectionViewCount() -> Int {
        return FilterManager.currentDomainFilterAttributes().count
    }
    
    fileprivate func attributeOptionCollectionViewCount() -> Int {
        if let attribute = FilterManager.getCurrentFilterAttribute() {
            return attribute.attributeOptions?.count ?? 0
        }
        return 0
    }
    
    fileprivate func attributeCollectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> FilterCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: attributeCellIdentifier, for: indexPath) as! FilterCollectionViewCell
        let attribute = FilterManager.currentDomainFilterAttributes()[indexPath.item]
        cell.imageView.image = attribute.equals(FilterManager.getCurrentFilterAttribute()) ? ( UIImage(named: attribute.attributeIconSelected.absoluteString) ?? UIImage(named: "icon_category_ticked_default_filled") ) : ( UIImage(named: attribute.attributeIcon.absoluteString) ?? UIImage(named: "icon_category_default_filled") )
        cell.label.text = attribute.attributeName
        return cell
    }
    
    fileprivate func attributeOptionCollectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> FilterCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: attributeOptionCellIdentifier, for: indexPath) as! FilterCollectionViewCell
        if let attribute = FilterManager.getCurrentFilterAttribute() {
            if let option = attribute.attributeOptions?[indexPath.item] {
                if attribute.isSelectedOption(option) {
                    cell.imageView.image = UIImage(named: option.iconSelected.absoluteString) ?? UIImage(named: "icon_category_ticked_default")
                } else {
                    cell.imageView.image = UIImage(named: option.icon.absoluteString) ?? UIImage(named: "icon_category_ticked_default")
                }
//                cell.imageView.image = attribute.isSelectedOption(option) ? UIImage(named: "icon_category_ticked_default") : UIImage(named: "icon_category_default")
                cell.label.text = option.name
            }
        }
        return cell
    }
    
    /**
     Refresh Option View
     */
    fileprivate func attributeCollectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        let attribute = FilterManager.currentDomainFilterAttributes()[indexPath.item]
        if let currentAttribute = FilterManager.getCurrentFilterAttribute() {
            if !currentAttribute.equals(attribute) {
                FilterManager.setCurrentFilterAttribute(attribute)
                // show proper options view
                refreshAttributeAndOptionView()
            }
        }
    }
    
    /**
     Refresh Option View and Attribute View if applicable
     */
    fileprivate func attributeOptionCollectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        if let attribute = FilterManager.getCurrentFilterAttribute() {
            if let option = attribute.attributeOptions?[indexPath.item] {
                attribute.toggleOptionSelection(option)
                // refresh both attribute collection view and its options view if the options of an attribute with dependencies has changed. e.g. Buy or Rent of MarketType changed will cause Price of Selling and Price of Rent to be changed as well
                if attribute.hasDependency {
                    refreshAttributeAndOptionView()
                } else {
                    refreshAttributeOptionView()
                }
            }
        }
    }
    
    
}
