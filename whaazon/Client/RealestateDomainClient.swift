//
//  RealestateDomainClient.swift
//  whaazon
//
//  Created by Can on 30/11/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PropertyDomainAPIManager {
    
    static let baseURL = "\(serverURL)/realestate/"
    
    static var processing = false
    static var request: DataRequest?
    
    static func fetchPropertyEvents(matching criteria: SearchCriteria, eventDelegate: EventDelegate) {
        let url = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if processing {
//            request!.cancel()
            NSLog("drop off fetchPropertyEvents processing request")
            return
        }
        request = Alamofire.request(url, method: .get)
        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        request?.validate().responseJSON { response in
            processing = false
            switch response.result {
            case .success(let value):
                NSLog("received PropertyEvent events")
                
                let json = JSON((value))
                var events = [Event]()
                for (_, subjson):(String, JSON) in json {
                    let event = PropertyEvent(json: subjson)
                    events.append(event)
                }
                printThread(message: "\nupdate view with \(events.count) events\n")
                eventDelegate.updateViewWithEvents(events)
                AppData.events = events
            case .failure(let error):
                printThread(message: error.localizedDescription)
                eventDelegate.updateViewWithError("Failed get data, please retry later.")
                guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
            }
        }
    }
    
    /// Used by CacheManager
    static func fetchPropertyEvents(_ criteria: SearchCriteria, completion: @escaping ([Event]) -> Void) {
        Client.fetchEventsMatchingCriteria(domain: .RealEstate, baseURL: baseURL, criteria: criteria, completion: completion)
        /*
        let url = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if processing {
            NSLog("drop off fetchPropertyEvents processing request")
            return
        }
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(completion: completion)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                processing = false
                var events = [Event]()
                if let data = data, let json = try? JSON(data: data) {
                    for (_, subjson):(String, JSON) in json {
                        let event = PropertyEvent(json: subjson)
                        events.append(event)
                    }
                    AppData.events = events
                } else if let error = error {
                    printThread(message: error)
                }
                printThread(message: "update view with \(events.count) Property events")
                completion(events)
                }.resume()
        }
 */
    }
    
}
