//
//  CityOfSydneyDomainClient.swift
//  whaazon
//
//  Created by Can on 20/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CityOfOZDomainClient {
    
    static let baseURL = "\(serverURL)/chargeable/"
    
    static var processing = false
    static var request: DataRequest?
    
    static func fetchCityOfSydneyEventsMatchingCriteria(_ criteria: SearchCriteria, eventDelegate: EventDelegate) {
        let url = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if processing {
//            request!.cancel()
            NSLog("drop off fetchCityOfSydneyEventsMatchingCriteria processing request")
            return
        }
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(eventDelegate: eventDelegate)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
        request = Alamofire.request(url, method: .get)
        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        request?.validate().responseJSON { response in
            processing = false
            switch response.result {
            case .success(let value):
//                if let data = response.result.value {
                    printThread(message: "received CityOfOZEvent events")
                    let json = JSON(value)
                    var events = [Event]()
                    for (_, subjson):(String, JSON) in json {
                        let event = CityOfOZEvent(json: subjson)
                        events.append(event)
                    }
                    NSLog("update view with \(events.count) events")
                    eventDelegate.updateViewWithEvents(events, filterEvents: true)
                    AppData.events = events
//                }
            case .failure(let error):
//                guard error.code != NSURLErrorCancelled else {return}
                printThread(message: error.localizedDescription)
                eventDelegate.updateViewWithError("Failed get data, please retry later.")
                guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
            }
        }
    }
    
    static func searchEvents(_ keywords: String, searchDelegate: SearchDelegate) {
        let domains = Preferences.selectedDomains
        var results = [Event]()
        DispatchQueue.global().async {
            let group = DispatchGroup()
        
        for domain in domains {
                group.enter()
                
                let url = (baseURL + "getEventsByPattern/pattern/\(keywords)/domain/\(domain.rawValue)/publishBaseTime/\(0)").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                request = Alamofire.request(url, method: .get)
                printThread(message: "Accessing URL = \(url)")
                request!.validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        if let data = response.result.value {
                            let json = JSON(data)
                            var events = [Event]()
                            for (_, subjson):(String, JSON) in json {
                                let event = CityOfOZEvent(json: subjson)
                                events.append(event)
                            }
                            printThread(message: "update view with \(events.count) events")
                            DispatchQueue.main.async {
                                results.append(contentsOf: events)
                            }
                        }
                        
                    case .failure(let error):
                        guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
                        printThread(message: error.localizedDescription)
                    }
                    
                    group.leave()
                }
            }
            group.wait()
            
            DispatchQueue.main.async {
                printThread(message: "***** finished searching")
                searchDelegate.updateViewWithEvents(results)
            }
        }
        
        /*
        let url = (baseURL + "getEventsByPattern/pattern/\(keywords)/domain/\(Domain.CityOfOZ.rawValue)/publishBaseTime/\(0)").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if processing {
            request!.cancel()
        }
        
        request = Alamofire.request(url, method: .get)
        processing = true
        
        printThread(message: "Accessing URL = \(url)")
        request!.validate().responseJSON { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    let json = JSON(data)
                    var events = [Event]()
                    for (_, subjson):(String, JSON) in json {
                        let event = CityOfOZEvent(json: subjson)
                        events.append(event)
                    }
                    printThread(message: "update view with \(events.count) events")
                    searchDelegate.updateViewWithEvents(events)
                }
                processing = false
            case .failure(let error):
                guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
                printThread(message: error.localizedDescription)
                processing = false
                searchDelegate.updateViewWithError("Failed get data, please retry later.")
            }
        }
 */
    }
    
    /// Used by CacheManager
    static func fetchCityOfSydneyEventsMatchingCriteria(_ criteria: SearchCriteria, completion: @escaping ([Event]) -> Void) {
        /*
        guard let urlString = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url = URL(string: urlString)
            else { return }
        
//        if processing {
//            NSLog("drop off fetchCityOfSydneyEventsMatchingCriteria processing request")
//            return
//        }
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(completion: completion)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
//        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")

        let request = URLRequest(url: url)
        task?.cancel()
        task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//            processing = false
            var events = [Event]()
            if let data = data, let json = try? JSON(data: data) {
                for (_, subjson):(String, JSON) in json {
                    let event = CityOfOZEvent(json: subjson)
                    events.append(event)
                }
                AppData.events = events
                printThread(message: "~~~~~~~~~~~~~~~~~~~ update view with \(events.count) CityOfSydney events at \(Date().description) ~~~~~~~~~~~~~~~~~~~")
                completion(events)
            } else if let error = error {
                printThread(message: "~~~~~~~~~~~~~~~~~~~ \(error) ~~~~~~~~~~~~~~~~~~~")
            }
        }
        task?.resume()
 */
        Client.fetchEventsMatchingCriteria(domain: .CityOfOZ, baseURL: baseURL, criteria: criteria, completion: completion)
    }
    
}

struct Client {
    
    static var taskMap = [String : URLSessionDataTask]()

    static func fetchEventsMatchingCriteria(domain: Domain, baseURL: String, criteria: SearchCriteria, completion: @escaping ([Event]) -> Void) {
        guard let urlString = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url = URL(string: urlString)
            else { return }
        var task = taskMap[domain.rawValue]
        task?.cancel()
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(completion: completion)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        
        task = URLSession.shared.dataTask(with: URLRequest(url: url)) { (data, response, error) in
            var events = [Event]()
            if let data = data, let json = try? JSON(data: data) {
                printThread(message: "~~~~~~~~~~~~~~~~~~~ \(domain) : json = \n\(json)\n ~~~~~~~~~~~~~~~~~~~")
                for (_, subjson):(String, JSON) in json {
                    switch domain {
                    case .CityOfOZ:
                        let event = CityOfOZEvent(json: subjson)
                        events.append(event)
                    case .coffeeChase:
                        let event = CoffeeChaseEvent(json: subjson)
                        events.append(event)
                    case .RealEstate:
                        let event = PropertyEvent(json: subjson)
                        events.append(event)
                    case .foodAndDrinkSpecials:
                        let event = FoodSpecialsEvent(json: subjson)
                        events.append(event)
                    default:
                        break
                    }
                }
                AppData.events = events
                printThread(message: "~~~~~~~~~~~~~~~~~~~ \(domain) : updated with \(events.count) events at \(Date().description) ~~~~~~~~~~~~~~~~~~~")
            } else if let error = error {
                printThread(message: "~~~~~~~~~~~~~~~~~~~ \(domain) : fetch events failed due to \(error.localizedDescription) ~~~~~~~~~~~~~~~~~~~")
            }
            completion(events)
        }
        task?.resume()
        taskMap[domain.rawValue] = task
    }
    
}
