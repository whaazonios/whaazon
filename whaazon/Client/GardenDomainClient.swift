//
//  GardenDomainClient.swift
//  whaazon
//
//  Created by Can on 4/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GardenDomainClient {
    
    static let baseURL = "\(serverURL)/core/"
    
    static func fetchGardenEventsMatchingCriteria(_ criteria: SearchCriteria, eventDelegate: EventDelegate) {
        let url = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        print("Accessing URL = \(url)")
        
        Alamofire.request(
            url,
            method: .get)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    var events = [Event]()
                    for (index, subjson):(String, JSON) in json {
                        let event = GardenEvent(json: subjson)
                        print("\(index):\(event)")
                        events.append(event)
                    }
                    eventDelegate.updateViewWithEvents(events)
                case .failure(let error):
                    print(error)
                    eventDelegate.updateViewWithError("Failed get data, please retry later.")
                }
        }
    }
    
}
