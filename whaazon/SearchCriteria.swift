//
//  SearchCriteria.swift
//  whaazon
//
//  Created by Can on 1/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation

struct SearchCriteria {
    var area = ""
    var timeWin = ""
    var category = ""
    var domain = ""
    var publishBaseTime = ""
    var idList = ""
    
    var criteriaURL: String {
        return "getEventsByCriteria/\(area)/time/\(timeWin)/category/\(category)/domain/\(domain)/publishBaseTime/\(publishBaseTime)/idList/\(idList)"
    }
    
}
