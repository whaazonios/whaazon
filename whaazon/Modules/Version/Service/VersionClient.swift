//
//  VersionClient.swift
//  whaazon
//
//  Created by Can Zhan on 5/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Foundation

struct VersionClient {
    
    static func fetchVersions(success: @escaping ((Version?) -> Void), failure: @escaping ((Error?) -> Void)) {
        guard let url = URL(string: "\(serverURL)/versioncheck/releases/list?appname=whaazon&os=ios") else {
            failure(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
            } else if let data = data {
                let version = try? JSONDecoder().decode(Version.self, from: data)
                DispatchQueue.main.async {
                    success(version)
                }
            }
        }.resume()
    }
}
