//
//  VersionViewController.swift
//  whaazon
//
//  Created by Can Zhan on 5/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class VersionViewController: UIViewController {

    @IBOutlet weak var laterButton: UIButton!
    
    var viewModel: VersionViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        laterButton.isHidden = viewModel?.version?.isUpdateRequired ?? false
    }

    @IBAction func tappedLaterButton() {
        dismiss(nil)
    }
    
    @IBAction func tappedUpdateButton() {
        if let url = URL(string: "https://appsto.re/au/8fu7eb.i"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

class VersionViewModel {
    
    var version: Version?
    
    func fetchVersion(success: @escaping (() -> Void)) {
        VersionClient.fetchVersions(
            success: { [weak self] (version) in
                self?.version = version
                success()
            },
            failure: { _ in })
    }
}

