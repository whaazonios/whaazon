//
//  Version.swift
//  whaazon
//
//  Created by Can Zhan on 5/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Foundation

struct Version: Decodable {
    let versions: [String]
    let builds: [Int]
    
    /// Is App required to do a mandatory update
    var isUpdateRequired: Bool {
        guard versions.count >= 3 else { return false }
        let mandatoryVersionNumber = versions[2]
        return appVersion < mandatoryVersionNumber
    }
    
    /// Is App is suggested to do an update
    var isUpdateSuggested: Bool {
        guard versions.count >= 2 else { return false }
        let minorVersionNumber = versions[0]
        let recommendedVersionNumber = versions[1]
        return appVersion < minorVersionNumber || appVersion < recommendedVersionNumber
    }

    var appVersion: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "0.0.0"
    }
}

/* Sample JSON : The three highest versions are now places in an array in the order of [ MinorUpdate, RecommendedUpdate, CompulsoryUpdate]
 {"code":200,"summary":"OK","details":"","objectId":0,"versions":["2.0.4","2.0.3","2.0.0"],"builds":[60,58,32]}
*/
