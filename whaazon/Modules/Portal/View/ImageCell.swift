//
//  ImageCell.swift
//  Whaazon
//
//  Created by Can Zhan on 26/10/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
