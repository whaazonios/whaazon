//
//  OccurenceStartDateTableViewCell.swift
//  whaazon
//
//  Created by Can Zhan on 18/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class OccurenceStartDateTableViewCell: UITableViewCell {

    @IBOutlet weak var picker: UIDatePicker!
    
}
