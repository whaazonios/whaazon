//
//  WhaazonTableViewCell.swift
//  whaazon
//
//  Created by Can Zhan on 7/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import UIKit

class WhaazonTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    var viewModel: WhaazonTableViewCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            coverImageView.applyBorder()
            if let url = viewModel.event.imageURL {
                coverImageView.kf.setImage(with: url)
            }
            titleLabel.text = viewModel.event.name
            summaryLabel.text = viewModel.event.summary
            timeLabel.text = viewModel.event.eventTimeString
        }
    }
}

struct WhaazonTableViewCellViewModel {
    let event: WhaazonEvent
}
