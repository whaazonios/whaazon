//
//  OccurenceDaysInWeekTableViewCell.swift
//  whaazon
//
//  Created by Can Zhan on 18/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class OccurenceDaysInWeekTableViewCell: UITableViewCell {

    @IBOutlet weak var mondayTickBoxView: TickBoxView!
    @IBOutlet weak var tuesdayTickBoxView: TickBoxView!
    @IBOutlet weak var wednesdayTickBoxView: TickBoxView!
    @IBOutlet weak var thursdayTickBoxView: TickBoxView!
    @IBOutlet weak var fridayTickBoxView: TickBoxView!
    @IBOutlet weak var saturdayTickBoxView: TickBoxView!
    @IBOutlet weak var sundayTickBoxView: TickBoxView!

    @IBOutlet var days: [TickBoxView]!
}
