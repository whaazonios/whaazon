//
//  OccurenceEndDateTableViewCell.swift
//  whaazon
//
//  Created by Can Zhan on 18/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class OccurenceEndDateTableViewCell: UITableViewCell {

    @IBOutlet weak var picker: UIDatePicker!

}
