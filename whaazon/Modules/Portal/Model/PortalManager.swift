//
//  PortalManager.swift
//  whaazon
//
//  Created by Can Zhan on 9/6/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation
import Alamofire

class PortalManager {
    
    static let baseURL = "\(serverURL)/chargeable/"
    
    private struct EndPoint {
        static let postEvent = "uploadEvent/"
        static let listEvents = "listEvents"
        static let deleteEvent = "deleteEvent/Coffee Chase/%d"
    }
    
    /// Load html contents from given URL and response in main thread.
    static func loadHtml(urlString: String, success: @escaping ((String, [String]) -> Void), failure: @escaping ((Error?) -> Void), retries: Int = 3) {
        let urlString = "\(serverURL)/core/processURL/opmode/gettextnodes,imgList?url=\(urlString)"
        DispatchQueue.global().async {
            guard retries > 0 else { return }
            if let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: urlString) {
                do {
                    let contents = try String(contentsOf: url)
                    if let response = try? JSONDecoder().decode(OpmodeResponse.self, from: Data(contents.utf8)) {
                        let imageURLs = response.imgList.split(separator: "\n").map({ String($0) })
                        DispatchQueue.main.async {
                            success(response.gettextnodes, imageURLs)
                        }
                    }
                } catch {
                    printThread(message: "Failed to load html content of error = \(error)")
                    loadHtml(urlString: urlString, success: success, failure: failure, retries: retries - 1)
                }
            }
            DispatchQueue.main.async {
                failure(nil)
            }
        }
    }
    
    static func listEvents(success: @escaping (([WhaazonEvent]) -> Void), failure: @escaping ((Error?) -> Void)) {
        guard let url = URL(string: baseURL + EndPoint.listEvents) else {
            failure(nil)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("session_token=\(AppUserInfo.whaazonAccessToken)", forHTTPHeaderField: "Cookie")
        printThread(message: request.debugDescription)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
            } else {
//                if let data = data,
//                    let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
//                    printThread(message: "\n\nJSON:\n\(json)\n\n")
//                    var events = [WhaazonEvent]()
//                    success(events)
//                }
                if let data = data {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .millisecondsSince1970
                    let response = try? decoder.decode(ListEventsResponse.self, from: data)
                    printThread(message: response.debugDescription)
                    printThread(message: String(data: data, encoding: .utf8)!)
                    DispatchQueue.main.async {
                        let events: [WhaazonEvent]? = response?.results.compactMap {
                            var event = $0
                            event.latitudeE6 /= 1000000
                            event.longitudeE6 /= 1000000
                            return event
                        }
                        success(events ?? [])
                    }
                }
            }
        }.resume()
    }
    
    static func deleteEvent(id: Int, success: @escaping (() -> Void), failure: @escaping ((Error?) -> Void)) {
        let endPoint = String(format: EndPoint.deleteEvent, id).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        guard let url = URL(string: baseURL + endPoint) else {
            failure(nil)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("session_token=\(AppUserInfo.whaazonAccessToken)", forHTTPHeaderField: "Cookie")
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
            } else {
                if let data = data,
                    let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
                    printThread(message: "\n\nJSON:\n\(json)\n\n")
                    DispatchQueue.main.async {
                        success()
                    }
                }
            }
            }.resume()
    }
    
    static func post(event: WhaazonEvent, success: @escaping ((Data?) -> Void), failure: @escaping ((NSError?) -> Void)) {
        var parameters: Parameters = [
            "id": event.id,
            "category": "Other",
            "country": "Australia",
            "domain": "Coffee Chase",
            "eventHeader": event.name ?? "",
            "eventSummary": event.summary ?? "",
            "level": 0,
            "latitudeE6": (event.address.coordinate?.latitude)! <= 180 && (event.address.coordinate?.latitude)! >= -180 ? (event.address.coordinate?.latitude)! * 1000000 : (event.address.coordinate?.latitude)!,
            "longitudeE6": (event.address.coordinate?.longitude)! <= 180 && (event.address.coordinate?.longitude)! >= -180 ? (event.address.coordinate?.longitude)! * 1000000 : (event.address.coordinate?.longitude)!,
            "place": event.address.title,
            "postcode": event.address.postcode,
            "publishTime": Date().timeIntervalSince1970 * 1000,
            "publisher": AppUserInfo.account.id,
            "state": event.address.state,
            "suburb": event.address.suburb,
            "maxAmountCent": 0,
            "maxAmountDollar": 0,
            "minAmountCent": 0,
            "minAmountDollar": 0,
            "tags": event.tags ?? "",
            "occurrenceDTOs": [event.occurrence?.json ?? [:]]
        ]
        
//        var data: Data?
        if event.isImageReplaced {
//            do {
//                data = try Data(contentsOf: event.imageURL!)
//            } catch let error as NSError {
//                printThread(message: "Failed to read image file: \(error)")
//            }
        } else {
            parameters["imgSrc"] = event.imageURL?.absoluteString
        }
        printThread(message: "Post event request parameters:\n\(parameters)")
        
        var request  = URLRequest(url: URL(string: baseURL + EndPoint.postEvent + "true")!)
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("session_token=\(AppUserInfo.whaazonAccessToken)", forHTTPHeaderField: "Cookie")
        request.httpBody = createBody(parameters: parameters,
                                boundary: boundary,
//                                data: data,
                                images: event.images,
                                mimeType: "image/jpeg",
                                filename: event.isImageReplaced ? "\(AppUserInfo.account.email ?? "")_\(Date().timeIntervalSince1970)_coverImage.jpg" : nil,
                                html: event.html,
                                event: event)
        
        let d = URLSession.shared.dataTask(with: request) { (data, response, error) in
            printThread(message: "request:\n\(request)")
            if let e = error {
                printThread(message: e.localizedDescription)
                DispatchQueue.main.async {
                    failure(nil)
                }
            } else {
                printThread(message: "response: \n\(response?.debugDescription)")
                if let data = data,
                    let desc = String(data: data, encoding: .utf8),
                    let response = try? JSONDecoder().decode(ResponseModel.self, from: data)
                {
                    if response.code == 200 {
                        DispatchQueue.main.async {
                            success(data)
                        }
                        printThread(message: "data: \(desc)")
                    } else {
                        DispatchQueue.main.async {
                            failure(NSError(domain: "Whaazon", code: response.code, userInfo: ["summary": response.summary, "details": response.details]))
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        failure(NSError(domain: "Whaazon", code: 411, userInfo: ["summary": "No valid response data received."]))
                    }
                }
            }
        }
        d.resume()
    }
    
    static private func createBody(parameters: [String: Any],
                    boundary: String,
//                    data: Data?,
                    images: [UIImage],
                    mimeType: String,
                    filename: String?,
                    html: String?,
                    event: WhaazonEvent) -> Data {
        let descriptionString = html ?? (parameters["eventSummary"] as? String)?.escaped ?? ""
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"dto\"\r\nContent-Type: application/json\r\n\r\n")
        body.appendString(parameters.jsonString()!)
        body.appendString("\r\n")
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"description\"\r\nContent-Type: text/html\r\n")
        body.appendString("Content-Length: \(descriptionString.count)\r\n\r\n")
        body.appendString(descriptionString)
        body.appendString("\r\n")
        
        // Images
        var filenames = [String]()
        for (index, image) in images.enumerated() {
            let data = image.uncompressedPNGData
            let filename = "\(AppUserInfo.account.email ?? "")_\(index)_\(Date().timeIntervalSince1970)_coverImage.jpg"
            filenames.append(filename)
            
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"images\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n")
            if let filesize = UIImage(data: data)?.fileSize() {
                body.appendString("Content-Length: \(filesize)\r\n\r\n")
                body.append(data)
            } else {
                body.appendString("Content-Length: \(0)\r\n\r\n")
            }
            body.appendString("\r\n")
            if index != images.count - 1 {
                // the last one use the multiple ending boundary below
                body.appendString(boundaryPrefix)
            }
        }
        
        let bodyString = event.imageURLs.joined(separator: "\n") + "\n" + filenames.joined(separator: "\n")
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"imgDirectory\"\r\nContent-Type: text/html\r\n")
        body.appendString("Content-Length: \(bodyString.count)\r\n\r\n")
        body.appendString(bodyString)
        body.appendString("\r\n")
        
//        if let filename = filename, !filename.isEmpty {
//            body.appendString(boundaryPrefix)
//            body.appendString("Content-Disposition: form-data; name=\"images\"; filename=\"\(filename)\"\r\n")
//            body.appendString("Content-Type: \(mimeType)\r\n")
//            if let data = data,
//                let filesize = UIImage(data: data)?.fileSize() {
//                body.appendString("Content-Length: \(filesize)\r\n\r\n")
//                body.append(data)
//            } else {
//                body.appendString("Content-Length: \(0)\r\n\r\n")
//            }
//            body.appendString("\r\n")
//        }
        body.appendString("--".appending(boundary.appending("--")))
        printThread(message: "request body : \n\(String(data: body as Data, encoding: .utf8))\n")
        return body as Data
    }
    
    private struct ResponseModel: Decodable {
        let code: Int
        let summary: String?
        let details: String?
    }
    
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

private struct ListEventsResponse: Decodable {
    let code: Int
    let details: String?
    let results: [WhaazonEvent]
}

private struct OpmodeResponse: Decodable {
    let gettextnodes: String
    let imgList: String
}

extension String {
    
    /// \n to <br/>
    var escaped: String {
        return replacingOccurrences(of: "\n", with: #"<br/>"#)
    }
    
    /// <br/> to \n
    var unescaped: String {
        
        return replacingOccurrences(of: #"<br/>"#, with: "\n")
    }
    
    var contentLength: Int {
        return escaped.count
    }
}
