//
//  WhaazonEvent.swift
//  whaazon
//
//  Created by Can Zhan on 28/5/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftDate

struct WhaazonEvent: Codable {
    var id: Int
    var name: String?
    var summary: String?
    var startTime: Date?
    var endTime: Date?
//    var address: Address?
    /// The one for upload
    var occurrence: OccurenceDTO?
    /// The downloaded ones
    var occurrences: [OccurenceDTO]?
    /// Downloaded event details URL, generated from the server
    var html: String?
    var imageURL: URL?
    /// Upload html URL
    var htmlURL: URL?
    var tags: String?
    var images = [UIImage]()
    /// Previously uploaded existing image urls
    var imageURLs = [String]()
    
    // MARK: - Address
    var latitudeE6: Double
    var longitudeE6: Double
    var country: String
    var postcode: Int
    var state: String
    var suburb: String
    var place: String
    
    init() {
        id = 0
        latitudeE6 = 0
        longitudeE6 = 0
        country = ""
        postcode = 0
        state = ""
        suburb = ""
        place = ""
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "eventHeader"
        case summary = "eventSummary"
        case startTime = "overallStartTime"
        case endTime = "overallEndTime"
        case imageURL = "imgSrc"
        case html = "href"
        case occurrences = "occurrenceDTOs"
        case latitudeE6 = "latitudeE6"
        case longitudeE6 = "longitudeE6"
        case country = "country"
        case postcode = "postcode"
        case state = "state"
        case suburb = "suburb"
        case place = "place"
        case tags
    }
    
    /// Event time
    var eventTimeString: String? {
        guard let startTime = startTime, let endTime = endTime else { return nil }
        return "\(startTime.displayString) - \(endTime.displayString)"
    }
    
    /// A convenient wrapper of address related fields
    var address: Address {
        get {
            return Address(
                coordinate: CLLocationCoordinate2D(latitude: latitudeE6, longitude: longitudeE6),
                building: place,
                street: "",
                suburb: suburb,
                state: state,
                postcode: String(postcode),
                country: country)
        }
        set {
            latitudeE6 = newValue.coordinate?.latitude ?? 0
            longitudeE6 = newValue.coordinate?.longitude ?? 0
            country = newValue.country
            postcode = Int(newValue.postcode) ?? 0
            state = newValue.state
            suburb = newValue.suburb
            place = newValue.building
        }
    }
    
    /// If the event is a new one or updating an existing one
    var isNewEvent: Bool {
        return id == 0
    }
    
    /// If image is an existing one or a newly selected/captured one
    var isImageReplaced: Bool {
//        return isNewEvent || (imageURL?.isFileURL ?? false)
        return isNewEvent || !images.isEmpty
    }
}

struct Address {
    var coordinate: CLLocationCoordinate2D?
    var building = ""
    var street = ""
    var suburb = ""
    var state = ""
    var postcode = ""
    var country = ""
    
    var title: String {
        return "\(building) \(street)"
    }
    
    var subtitle: String {
        return "\(suburb) \(state) \(postcode), \(country)"
    }
    
    var description: String {
        return "\(title), \(subtitle)"
    }
}

struct OccurenceDTO: Codable {
    var id: Int
    var startDate: Date
    var endDate: Date
    var timezone: String
    var overallPeriodOnly: Bool
    var occurType: OccurType
    var monthsInYear: String
    var weeksInMonth: String
    var daysInWeek: String
    var intervalsInDay: String
    
    // Start and end time of an interval in day
    var startTime: Date = Date()
    var endTime: Date = Date()
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case startDate = "startDate"
        case endDate = "endDate"
        case timezone = "timezone"
        case overallPeriodOnly = "overallPeriodOnly"
        case occurType = "occurType"
        case monthsInYear = "monthsInYear"
        case weeksInMonth = "weeksInMonth"
        case daysInWeek = "daysInWeek"
        case intervalsInDay = "intervalsInDay"
    }
    
    // MARK: - Encodable
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(startDate, forKey: .startDate)
        try container.encode(endDate, forKey: .endDate)
        try container.encode(timezone, forKey: .timezone)
        try container.encode(overallPeriodOnly, forKey: .overallPeriodOnly)
        try container.encode(occurType.rawValue, forKey: .occurType)
        try container.encode(monthsInYear, forKey: .monthsInYear)
        try container.encode(weeksInMonth, forKey: .weeksInMonth)
        try container.encode(daysInWeek, forKey: .daysInWeek)
        try container.encode("\(startTime.hour):\(startTime.minute):\(startTime.second)-\(endTime.hour):\(endTime.minute):\(endTime.second)", forKey: .intervalsInDay)
    }
    
    // MARK: - Decodable
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        startDate = try container.decode(Date.self, forKey: .startDate)
        endDate = try container.decode(Date.self, forKey: .endDate)
        timezone = try container.decode(String.self, forKey: .timezone)
        overallPeriodOnly = try container.decode(Bool.self, forKey: .overallPeriodOnly)
        let occurTypeString = try container.decode(String.self, forKey: .occurType)
        occurType = OccurType(rawValue: occurTypeString) ?? .SINGLE
        monthsInYear = try container.decode(String.self, forKey: .monthsInYear)
        daysInWeek = try container.decode(String.self, forKey: .daysInWeek)
        weeksInMonth = try container.decode(String.self, forKey: .weeksInMonth)
        intervalsInDay = try container.decode(String.self, forKey: .intervalsInDay)
        let timeComponents = intervalsInDay.components(separatedBy: "-")
        let startTimeComponents = timeComponents[0].components(separatedBy: ":")
        let endTimeComponents = timeComponents[1].components(separatedBy: ":")
        if let hour = Int(startTimeComponents[0]), let minute = Int(startTimeComponents[1]), let second = Int(startTimeComponents[2]) {
            startTime = Date(year: startDate.year, month: startDate.month, day: startDate.day, hour: hour, minute: minute, second: second, region: Region.current)
        }
        if let hour = Int(endTimeComponents[0]), let minute = Int(endTimeComponents[1]), let second = Int(endTimeComponents[2]) {
            endTime = Date(year: startDate.year, month: startDate.month, day: startDate.day, hour: hour, minute: minute, second: second, region: Region.current)
        }
    }
    
    init() {
        id = 0
        startDate = Date()
        endDate = Date()
        timezone = "Australia/Sydney"
        overallPeriodOnly = false
        occurType = .SINGLE
        monthsInYear = ""
        weeksInMonth = ""
        daysInWeek = ""
        intervalsInDay = ""
    }
    
    var wordsDescription: String {
        switch occurType {
        case .SINGLE:
            return "\(DateFormatter.dateOnlyDateFormatter.string(from: startDate)) \(DateFormatter.intervalsInDayFormatter.string(from: startTime)) to \(DateFormatter.intervalsInDayFormatter.string(from: endTime))"
        case .REPEAT_DAILY:
            return "\(DateFormatter.intervalsInDayFormatter.string(from: startTime)) to \(DateFormatter.intervalsInDayFormatter.string(from: endTime)) between \(DateFormatter.dateOnlyDateFormatter.string(from: startDate)) and \(DateFormatter.dateOnlyDateFormatter.string(from: endDate))"
        case .REPEAT_WEEKLY:
            return "Every \(DateFormatter.intervalsInDayFormatter.string(from: startTime)) to \(DateFormatter.intervalsInDayFormatter.string(from: endTime)) between \(DateFormatter.dateOnlyDateFormatter.string(from: startDate)) and \(DateFormatter.dateOnlyDateFormatter.string(from: endDate))"
        default:
            return ""
        }
    }
    
    var json: [String: Any] {
        return [
            "id": id,
            "startDate": startDate.timeIntervalSince1970 * 1000,
            "endDate": endDate.timeIntervalSince1970 * 1000,
            "timezone": timezone,
            "overallPeriodOnly": overallPeriodOnly,
            "occurType": occurType.rawValue,
            "monthsInYear": monthsInYear,
            "weeksInMonth": weeksInMonth,
            "daysInWeek": daysInWeek,
            "intervalsInDay": "\(DateFormatter.intervalsInDayFormatter.string(from: startTime))-\(DateFormatter.intervalsInDayFormatter.string(from: endTime))"
        ]
    }
}
