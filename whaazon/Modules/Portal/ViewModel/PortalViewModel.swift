//
//  PortalViewModel.swift
//  whaazon
//
//  Created by Can Zhan on 7/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation

protocol PortalDelegate: class {
    func imagesDidUpdate()
}

class PortalViewModel {
    
    weak var delegate: PortalDelegate?
    
    var images = [UIImage]() {
        didSet {
            delegate?.imagesDidUpdate()
        }
    }
    
    /// Convert html content of `href` of WhaazonEvent into string content between `` and ``
    static func htmlToStringContent(_ htmlString: String) -> String {
        let htmlString = htmlString.replacingOccurrences(of: "\n", with: "")
        let pattern = #"<!-- text begins here -->.*<\/body>"#
        let expression = try? NSRegularExpression(pattern: pattern)
        if let range = expression?.firstMatch(in: htmlString, options: [], range: NSRange(location: 0, length: htmlString.count))?.range {
            let startIndex = htmlString.index(htmlString.startIndex, offsetBy: range.lowerBound)
            let endIndex = htmlString.index(htmlString.startIndex, offsetBy: range.upperBound)
            var trimmedString = String(htmlString[startIndex..<endIndex])
            trimmedString = trimmedString.replacingOccurrences(of: "<!-- text begins here -->", with: "")
            trimmedString = trimmedString.replacingOccurrences(of: #"</body>"#, with: "")
            return trimmedString.escaped
        }
        return ""
    }
}
