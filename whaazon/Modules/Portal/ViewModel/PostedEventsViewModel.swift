//
//  PostedEventsViewModel.swift
//  whaazon
//
//  Created by Can Zhan on 7/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation

class PostedEventsViewModel {
    
    // MARK: - Vars
    
    var events = [WhaazonEvent]()
    
    // MARK: - Functions
    
    /// Cell view model for specific cell
    func cellViewModelForRowAt(_ indexPath: IndexPath) -> WhaazonTableViewCellViewModel? {
        guard indexPath.row < events.count else { return nil }
        return WhaazonTableViewCellViewModel(event: events[indexPath.row])
    }
    
    // MARK: - Service Calls
    
    /// Fetch posted events. Both success and failure handlers are run in main queue.
    func fetchPostedEvents(success: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        PortalManager.listEvents(
            success: { (events) in
                self.events = events.sorted(by: { $0.id > $1.id })
                success()
            },
            failure: { (error) in
                if let error = error {
                    failure(error)
                }
            })
    }
    
    func deleteEventAt(_ indexPath: IndexPath, success: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        let event = events[indexPath.row]
        PortalManager.deleteEvent(
            id: event.id,
            success: {
                self.events.remove(at: indexPath.row)
                success()
            },
            failure: { error in
                failure(error)
            })
    }
    
}
