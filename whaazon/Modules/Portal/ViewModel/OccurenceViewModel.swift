//
//  OccurenceViewModel.swift
//  whaazon
//
//  Created by Can Zhan on 17/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Foundation

class OccurenceViewModel {
    
    struct CellIdentifier {
        static let startDate = "startDateCell"
        static let endDate = "endDateCell"
        static let startTime = "startTimeCell"
        static let endTime = "endTimeCell"
        static let daysInWeek = "daysInWeekCell"
        static let confirmation = "confirmationCell"
    }
    
    var occurence: OccurenceDTO
    
    init(occurence: OccurenceDTO = OccurenceDTO()) {
        self.occurence = occurence
    }
    
    // MARK: - Table view
    
    var identifiers: [String] {
        switch occurence.occurType {
        case .SINGLE:
            return [CellIdentifier.startDate, CellIdentifier.startTime, CellIdentifier.endTime, CellIdentifier.confirmation]
        case .REPEAT_DAILY:
            return [CellIdentifier.startDate, CellIdentifier.endDate, CellIdentifier.startTime, CellIdentifier.endTime, CellIdentifier.confirmation]
        case .REPEAT_WEEKLY:
            return [CellIdentifier.daysInWeek, CellIdentifier.startDate, CellIdentifier.endDate, CellIdentifier.startTime, CellIdentifier.endTime, CellIdentifier.confirmation]
        default:
            return []
        }
    }
    
    func updateOccurence(in tableView: UITableView) {
        switch occurence.occurType {
        case .SINGLE:
            occurence.startDate = (tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OccurenceStartDateTableViewCell).picker.date
            occurence.startTime = (tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! OccurenceStartTimeTableViewCell).picker.date
            occurence.endTime = (tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! OccurenceEndTimeTableViewCell).picker.date
        case .REPEAT_DAILY:
            occurence.startDate = (tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OccurenceStartDateTableViewCell).picker.date
            occurence.endDate = (tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! OccurenceEndDateTableViewCell).picker.date
            occurence.startTime = (tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! OccurenceStartTimeTableViewCell).picker.date
            occurence.endTime = (tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! OccurenceEndTimeTableViewCell).picker.date
        case .REPEAT_WEEKLY:
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? OccurenceDaysInWeekTableViewCell {
                let days: [Bool] = cell.days.map{ $0.isTicked }
                var pickedDays: [String] = []
                for (index, day) in days.enumerated() {
                    if day {
                        pickedDays.append(String(index))
                    }
                }
                occurence.daysInWeek = pickedDays.joined(separator: ",")
            }
            occurence.startDate = (tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! OccurenceStartDateTableViewCell).picker.date
            occurence.endDate = (tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! OccurenceEndDateTableViewCell).picker.date
            occurence.startTime = (tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! OccurenceStartTimeTableViewCell).picker.date
            occurence.endTime = (tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! OccurenceEndTimeTableViewCell).picker.date
        default:
            break
        }
    }
    
}
