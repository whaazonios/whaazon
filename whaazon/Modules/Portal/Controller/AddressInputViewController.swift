//
//  AddressInputViewController.swift
//  whaazon
//
//  Created by Can on 10/3/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit
import MapKit
//import GooglePlacesAutocomplete

protocol AddressPickDelegate {
    func picked(address: Address?)
}

class AddressInputViewController: UIViewController {

    // MARK: Vars
    var address: Address?
    var delegate: AddressPickDelegate?
    fileprivate var searchCompleter = MKLocalSearchCompleter()
    fileprivate var searchResults = [MKLocalSearchCompletion]()
    fileprivate var annotation = MKPointAnnotation()
    private let locationManager = CLLocationManager()
    
    // MARK: IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if address == nil {
            address = Address()
        }
        searchBar.text = address?.description
        searchCompleter.delegate = self
        tableView.isHidden = true
        setupMapView()
    }

    @IBAction func tappedCenterButton() {
        if let currentLocation = locationManager.location {
            centerMap(to: currentLocation.coordinate)
        }
    }
    
    @IBAction func tappedDoneButton() {
        delegate?.picked(address: address)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func longPressedMapView(_ sender: UIGestureRecognizer) {
        pinTouchPoint(sender)
    }
    
    @IBAction func tappedMapView(_ sender: UIGestureRecognizer) {
        if tableView.isHidden {
            pinTouchPoint(sender)
        } else {
            tableView.isHidden = true
            searchBar.endEditing(false)
        }
    }
    
    private func pinTouchPoint(_ recognizer: UIGestureRecognizer) {
        if recognizer.state == .began {
            let touchPoint = recognizer.location(in: mapView)
            let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: self.mapView)
            annotation.coordinate = touchCoordinate
            updateAddressWithLocation(CLLocation(latitude: touchCoordinate.latitude, longitude: touchCoordinate.longitude))
        }
    }
    
    private func updateAddressWithLocation(_ location: CLLocation) {
        // Add below code to get address for touch coordinates.
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { [weak self] (placemarks, error) -> Void in
            guard let strongSelf = self else { return }
            if let placeMark = placemarks?.first {
                strongSelf.address?.building = placeMark.name ?? ""
                strongSelf.address?.street = placeMark.thoroughfare ?? ""
                strongSelf.address?.suburb = placeMark.locality ?? ""
                strongSelf.address?.state = placeMark.administrativeArea ?? ""
                strongSelf.address?.postcode = placeMark.postalCode ?? ""
                strongSelf.address?.country = placeMark.country ?? ""
                strongSelf.address?.coordinate = placeMark.location?.coordinate
                strongSelf.searchBar.text = strongSelf.address?.description
            }
        })
    }
    
    private func setupMapView() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // View Area
        var region = MKCoordinateRegion()
        if let latestLocation = address?.coordinate {
            region.center = latestLocation
            annotation.coordinate = latestLocation
        } else if let currentLocation = locationManager.location {
            region.center = currentLocation.coordinate
            annotation.coordinate = currentLocation.coordinate
        }
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
        
        // View Area
//        if let coordinate = address?.coordinate {
//            centerMap(to: coordinate)
//            annotation.coordinate = coordinate
//        } else if let currentLocation = locationManager.location {
//            centerMap(to: currentLocation.coordinate)
//            annotation.coordinate = currentLocation.coordinate
//        }
        mapView.addAnnotation(annotation)
    }
    
    fileprivate func centerMap(to coordinate: CLLocationCoordinate2D) {
        var region = MKCoordinateRegion()
        region.center = coordinate
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
    }

}

// MARK: Map

extension AddressInputViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else { return nil }
        let identifier = "Pin"
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//        annotationView.canShowCallout = true
        annotationView.animatesDrop = true
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? MKUserLocation, let location = annotation.location {
            updateAddressWithLocation(location)
//            mapView.removeAnnotations(mapView.annotations.filter({ !($0 is MKUserLocation) }))
        }
    }
    
}

// MARK: Searchbar and table

extension AddressInputViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tableView.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        tableView.isHidden = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
    }
}

extension AddressInputViewController: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        tableView.reloadData()
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        DLog("failed to get autocompletion")
    }
    
}

extension AddressInputViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }

}

extension AddressInputViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let completion = searchResults[indexPath.row]
//        address?.title = completion.title
//        address?.subTitle = completion.subtitle
        searchBar.text = address?.description
        searchBar.endEditing(false)
        
        let searchRequest = MKLocalSearchRequest(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { [weak self] (response, error) in
            guard let strongSelf = self else { return }
            if let coordinate = response?.mapItems[0].placemark.coordinate {
                DLog(String(describing: coordinate))
                strongSelf.address?.coordinate = coordinate
                // show annotation and center map
                strongSelf.centerMap(to: coordinate)
                strongSelf.annotation.coordinate = coordinate
            }
        }
        tableView.isHidden = true
    }
}
