//
//  DateTimePickViewController.swift
//  whaazon
//
//  Created by Can Zhan on 19/5/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit
import SwiftDate

protocol DateTimePickDelegate {
    func picked(date: Date?, of type: DateTimeType)
}

enum DateTimeType {
    case eventStartTime, eventEndTime, occurrenceStartTime, occurrenceEndTime
}

/// Generic DateTime picker view controller which provides three options: date only, time only and date&time
class DateTimePickViewController: UIViewController {

    /// Date only
    @IBOutlet weak var datePicker: UIDatePicker?
    /// Time only
    @IBOutlet weak var timePicker: UIDatePicker?
    /// Date and Time
    @IBOutlet weak var dateTimePicker: UIDatePicker?
    
    var date: Date? {
        didSet {
            if let date = date {
                datePicker?.date = date
                timePicker?.date = date
                dateTimePicker?.date = date
            }
        }
    }
    
    var delegate: DateTimePickDelegate?
    var datePickedHandler: ((Date?) -> Void)?
    var type: DateTimeType = .eventStartTime
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let date = date {
            datePicker?.date = date
            timePicker?.date = date
            dateTimePicker?.date = date
        }
    }

    @IBAction func tappedDoneButton() {
        var pickedDate: Date?
        pickedDate = dateTimePicker?.date
        delegate?.picked(date: pickedDate, of: type)
        datePickedHandler?(pickedDate)
        dismiss(animated: true, completion: nil)
//        let pickedDate = Date(year: datePicker.date.year, month: datePicker.date.month, day: datePicker.date.day, hour: timePicker.date.hour, minute: timePicker.date.minute, second: timePicker.date.second)
//        var c: [Calendar.Component : Int] = [.year: datePicker.date.year,
//                                             .month: datePicker.date.month,
//                                             .day: datePicker.date.day,
//                                             .hour: timePicker.date.hour,
//                                             .minute: timePicker.date.minute,
//                                             .second: timePicker.date.second]
//        let pickedDate = DateInRegion(components: &c, region: Region.local)?.absoluteDate
//        delegate?.picked(date: pickedDate, of: type)
//        navigationController?.popViewController(animated: true)
    }
    
}
