//
//  RepeatFrequencyViewController.swift
//  whaazon
//
//  Created by Can Zhan on 1/6/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit

class RepeatFrequencyViewController: UIViewController {
    
    fileprivate let frequencies = ["Forever", "Until a date", "For a number of events"]

    @IBOutlet weak var frequencyPickerView: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var eventsNumberContainerView: UIView!
    @IBOutlet weak var eventsNumberTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension RepeatFrequencyViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return frequencies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 1:
            datePicker.isHidden = false
            eventsNumberContainerView.isHidden = true
        case 2:
            datePicker.isHidden = true
            eventsNumberContainerView.isHidden = false
        default:
            datePicker.isHidden = true
            eventsNumberContainerView.isHidden = true
        }
    }
}
