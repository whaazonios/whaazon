//
//  OccurenceViewController.swift
//  whaazon
//
//  Created by Can Zhan on 29/5/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit

protocol OccurenceDelegate: class {
    func occurenceDidUpdated(occurence: OccurenceDTO)
}

class OccurenceViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var occurenceTypeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Vars
    weak var delegate: OccurenceDelegate?
    var viewModel: OccurenceViewModel!
    
    // MARK: - Init
    static func instance(occurence: OccurenceDTO, delegate: OccurenceDelegate?) -> OccurenceViewController? {
        if let viewController = UIStoryboard(name: "Portal", bundle: nil).instantiateViewController(withIdentifier: "OccurenceViewController") as? OccurenceViewController {
            viewController.viewModel = OccurenceViewModel(occurence: occurence)
            viewController.delegate = delegate
            return viewController
        }
        return nil
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        refreshUIData()
    }
    
    private func setupUI() {
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }
    
    private func refreshUIData() {
        occurenceTypeLabel.text = viewModel.occurence.occurType.descrpition
    }
    
    // MARK: - IBActions
    @IBAction func tappedOccurenceTypeLabel(_ sender: UITapGestureRecognizer) {
        let actionSheet = UIAlertController(title: "Choose the type", message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Single", style: .default) { [weak self] (action) in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.occurence.occurType = .SINGLE
            strongSelf.refreshUIData()
            strongSelf.tableView.reloadData()
        }
        let action2 = UIAlertAction(title: "Repeat daily", style: .default) { [weak self] (action) in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.occurence.occurType = .REPEAT_DAILY
            strongSelf.refreshUIData()
            strongSelf.tableView.reloadData()
        }
        let action3 = UIAlertAction(title: "Repeat weekly", style: .default) { [weak self] (action) in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.occurence.occurType = .REPEAT_WEEKLY
            strongSelf.refreshUIData()
            strongSelf.tableView.reloadData()
        }
        actionSheet.addAction(action1)
        actionSheet.addAction(action2)
        actionSheet.addAction(action3)
        present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func tappedCancelButton() {
        dismiss(nil)
    }
    
    @IBAction func tappedSaveButton() {
        viewModel.updateOccurence(in: tableView)
        delegate?.occurenceDidUpdated(occurence: viewModel.occurence)
        dismiss(nil)
    }
}

extension OccurenceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.identifiers.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = viewModel.identifiers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let cell = cell as? OccurenceStartDateTableViewCell {
            cell.picker.date = viewModel.occurence.startDate
        } else if let cell = cell as? OccurenceEndDateTableViewCell {
            cell.picker.date = viewModel.occurence.endDate
        } else if let cell = cell as? OccurenceStartTimeTableViewCell {
            cell.picker.date = viewModel.occurence.startTime
        } else if let cell = cell as? OccurenceEndTimeTableViewCell {
            cell.picker.date = viewModel.occurence.endTime
        } else if let cell = cell as? OccurenceDaysInWeekTableViewCell {
            let pickedDays = viewModel.occurence.daysInWeek.split(separator: ",").compactMap{ Int(String($0)) }
            for day in pickedDays {
                cell.days[day].isTicked = true
            }
        }
        return cell
    }
}

extension OccurenceViewController: UITableViewDelegate {
}

/*
import UIKit
import M13Checkbox
import RxSwift
import RxCocoa

protocol OccurrencePickDelegate {
    func picked(occurrence: OccurenceDTO?)
}

class OccurrenceViewController: UITableViewController {

    var occurrence: OccurenceDTO?
    var delegate: OccurrencePickDelegate?
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var typePicker: UIPickerView!
//    @IBOutlet weak var weekdaysCheckboxCell: UITableViewCell!
//    @IBOutlet weak var monthlyOptionsCell: UITableViewCell!
    @IBOutlet weak var typePickerContainerView: UIView!
    @IBOutlet weak var repeatInputContainerView: UIView!
    @IBOutlet weak var weekDaysCheckboxContainerView: UIView!
    @IBOutlet weak var monthlyOptionsContainerView: UIView!
    @IBOutlet weak var repeatFrequencyContainerView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideContainerViewsIfNeeded(with: typePicker.selectedRow(inComponent: 0))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repeatMonthlySwitch.rx.isOn
            .subscribe(onNext: { [weak self] (isOn) in
            self?.repeatMonthlyLabel.text = isOn ? "on the same day each month" : "on every last Wednesday"
        }).disposed(by: disposeBag)
        typePicker.rx.itemSelected
            .throttle(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (rowIndex, componentIndex) in
                guard let strongSelf = self else { return }
                strongSelf.hideContainerViewsIfNeeded(with: rowIndex)
//            strongSelf.tableView.beginUpdates()
//            strongSelf.tableView.deleteRows(at: strongSelf.rowIndexesShouldBeDeleted, with: .automatic)
//            strongSelf.tableView.insertRows(at: strongSelf.rowIndexesShouldBeInserted, with: .automatic)
//            strongSelf.tableView.endUpdates()
        }).disposed(by: disposeBag)
    }
    
    // MARK: General
    @IBAction func tappedOKButton() {
        delegate?.picked(occurrence: occurrence)
        navigationController?.popViewController(animated: true)
    }
    
//    private var rowIndexesShouldBeDeleted: [IndexPath] {
//        var indexPaths = [IndexPath]()
//        if let indexPath = tableView.indexPath(for: weekdaysCheckboxCell) {
//            indexPaths.append(indexPath)
//        }
//        if let indexPath = tableView.indexPath(for: monthlyOptionsCell) {
//            indexPaths.append(indexPath)
//        }
//        return indexPaths
//    }
//    
//    private var rowIndexesShouldBeInserted: [IndexPath] {
//        var indexPaths = [IndexPath]()
//        switch typePicker.selectedRow(inComponent: 0) {
//        case 2:
//            if let indexPath = super.tableView.indexPath(for: weekdaysCheckboxCell) {
//                indexPaths.append(indexPath)
//            }
//        case 3:
//            if let indexPath = super.tableView.indexPath(for: monthlyOptionsCell) {
//                indexPaths.append(indexPath)
//            }
//        default:
//            break
//        }
//        return indexPaths
//    }
    
    // MARK: REPEAT_WEEKLY
    @IBOutlet weak var checkboxContainerView: UIView!
//    @IBOutlet weak var untilDatePicker: UIDatePicker!
    
    fileprivate let occurrences = [OccurType.SINGLE,
                              OccurType.REPEAT_HOURLY,
                              OccurType.REPEAT_DAILY,
                              OccurType.REPEAT_WEEKLY,
                              OccurType.REPEAT_MONTHLY,
                              OccurType.REPEAT_YEARLY]
    
    fileprivate var selectedDays: [DayOfWeek]? { return checkboxes(of: checkboxContainerView).flatMap({ $0.value as? Int }).flatMap({ DayOfWeek(rawValue: $0) }) }
    
    private func checkboxes(of view: UIView) -> [M13Checkbox] {
        var foundCheckboxex = [M13Checkbox]()
        if let checkbox = view as? M13Checkbox {
            foundCheckboxex.append(checkbox)
        } else {
            for subview in view.subviews {
                foundCheckboxex += checkboxes(of: subview)
            }
        }
        return foundCheckboxex
    }
    
    private func hideContainerViewsIfNeeded(with selectedRow: Int) {
        repeatInputContainerView.isHidden = selectedRow == 0
        weekDaysCheckboxContainerView.isHidden = selectedRow != 2
        monthlyOptionsContainerView.isHidden = selectedRow != 3
        repeatFrequencyContainerView.isHidden = selectedRow == 0
    }
    
    // MARK: REPEAK_MONTHLY
    @IBOutlet weak var repeatMonthlySwitch: UISwitch!
    @IBOutlet weak var repeatMonthlyLabel: UILabel!
    
    @IBAction func changedMonthlySwitch(_ monthlySwitch: UISwitch) {
    }
    
}

extension OccurrenceViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return occurrences.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return occurrences[row].descrpition
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        occurrence?.occurType = String(describing: occurrences[row])
    }
}
*/
