//
//  PortalViewController.swift
//  whaazon
//
//  Created by Can on 22/2/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit
import MobileCoreServices
import RxSwift
import CoreLocation
import BSImagePicker
import Photos
import Kingfisher

class PortalViewController: UITableViewController {
    
    enum PortalViewType {
        case add, edit, clone
    }
    
    struct SegueIdentifier {
        static let login = "login"
        static let pickTime = "pickTime"
        static let pickAddress = "pickAddress"
        static let pickOccurence = "pickOccurence"
        static let inputHtml = "inputHtml"
    }
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var eventNameTextField: UITextField!
    @IBOutlet weak var eventDescriptionTextView: UITextView!// summary
    @IBOutlet weak var eventLocationLabel: UILabel!
    @IBOutlet weak var eventOccurrenceLabel: UILabel?
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventImageButton: UIButton!
    @IBOutlet weak var descriptionWebView: UIWebView?
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var eventDescriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var coffeeTickBoxView: TickBoxView!
    @IBOutlet weak var mealTickBoxView: TickBoxView!
    @IBOutlet weak var cakeTickBoxView: TickBoxView!
    @IBOutlet weak var toggleDetailsTextViewButton: UIButton!
    @IBOutlet weak var eventDetailsTextView: UITextView!    // description field in multipart
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Vars
    
    var viewType = PortalViewType.add
    var event = WhaazonEvent()
    
    private let disposeBag = DisposeBag()
    private var type: DateTimeType = .eventStartTime
    private let locationManager = CLLocationManager()
    private let loadingView = LoadingView()
    private let viewModel = PortalViewModel()
    
    /// Local Image file URL, which is the one to be saved/viewed
    private var imageURL: URL { return URL(fileURLWithPath: getDocumentsDirectory()).appendingPathComponent("tmp.png") }
    
    /// Local Image file URL, which is the one to be edited/viewed
    private var htmlURL: URL { return URL(fileURLWithPath: getDocumentsDirectory()).appendingPathComponent("tmp.html") }
    
    private var pickedTags: String? {
        var tags = [String]()
        if coffeeTickBoxView.isTicked {
            tags.append("Coffee")
        }
        if mealTickBoxView.isTicked {
            tags.append("Meal")
        }
        if cakeTickBoxView.isTicked {
            tags.append("Cake")
        }
        let jointString = tags.joined(separator: ",")
        return !jointString.isEmpty ? jointString : nil
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        collectionView.delegate = self
        collectionView.dropDelegate = self
//        checkLogin()
        eventNameTextField.applyBorder()
        eventDescriptionTextView.applyBorder()
        eventDetailsTextView.applyBorder()
        eventImageButton.applyBorder()
        addressContainerView.applyBorder()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        let tap = UITapGestureRecognizer(target: self, action: #selector(PortalViewController.tappedAnywhere))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
//        bind()
        postButton.setTitle(viewType == .edit ? "Update" : "Post", for: .normal)
        refreshUIData()
        // clear cached events
        CacheManager.shared.reset()
        viewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkLogin()
    }
    /*
    private func bind() {
        Observable.just(event).subscribe(onNext: { [weak self] (event) in
            guard let strongSelf = self else { return }
            strongSelf.eventLocationLabel.text = event.address.description
//            strongSelf.eventOccurrenceLabel?.text = event.occurrence.occurType
        }).disposed(by: disposeBag)
    }
    */
    /// Update UI with event data
    private func refreshUIData() {
        setupLocationManager()
        eventNameTextField.text = event.name
        eventDescriptionTextView.text = event.summary
//        if let url = event.imageURL {
//            eventImageView.kf.setImage(with: url)
//        } else {
//            eventImageView.image = UIImage(named: "icon-camera")
//        }
        collectionView.isHidden = event.imageURLs.isEmpty && viewModel.images.isEmpty
        if event.occurrence == nil {
            event.occurrence = event.occurrences?.first
        }
        eventOccurrenceLabel?.text = event.occurrence?.wordsDescription ?? "Pick an occurence"
        eventLocationLabel.text = event.address.description
        coffeeTickBoxView.isTicked = event.tags?.lowercased().contains("coffee") ?? false
        mealTickBoxView.isTicked = event.tags?.lowercased().contains("meal") ?? false
        cakeTickBoxView.isTicked = event.tags?.lowercased().contains("cake") ?? false
        // In the events returned from server, the previously input details are stored in a separate file linked by `href`
        // to be able to edit it, load the file and fill it into the textview
        if let urlString = event.html {
            PortalManager.loadHtml(
                urlString: urlString,
                success: { [weak self] (htmlContent, imageURLs) in
                    guard let strongSelf = self else { return }
                    // now the htmlContent is already escaped plain text from the server
                    strongSelf.event.html = htmlContent //PortalViewModel.htmlToStringContent(htmlContent)
                    strongSelf.event.imageURLs = imageURLs
                    strongSelf.eventDetailsTextView.text = strongSelf.event.html
                    strongSelf.imagesDidUpdate()
                },
                failure: { (_) in
                })
        }
    }
    
    private func setupLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            // only need to preset location when Posting a new event
            if viewType == .add {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pickTime,
            let dateTimePickViewController = segue.destination as? DateTimePickViewController {
            dateTimePickViewController.delegate = self
            switch type {
            case .eventStartTime: dateTimePickViewController.date = event.startTime
            case .eventEndTime: dateTimePickViewController.date = event.endTime
            default: break
            }
            dateTimePickViewController.type = type
        } else if segue.identifier == SegueIdentifier.pickAddress,
            let addressInputViewController = segue.destination as? AddressInputViewController {
            addressInputViewController.delegate = self
            addressInputViewController.address = event.address
        } else if segue.identifier == SegueIdentifier.login,
            let loginViewController = (segue.destination as? UINavigationController)?.topViewController as? LoginViewController {
            loginViewController.dismissHandler = { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.presentingViewController?.dismiss(animated: true, completion: nil)
                strongSelf.onLeftMenu()
            }
            loginViewController.signedInHandler = { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.checkLogin()
            }
        }
        /*
        else if segue.identifier == SegueIdentifier.pickOccurence,
            let occurrenceViewController = segue.destination as? OccurrenceViewController {
            occurrenceViewController.delegate = self
            occurrenceViewController.occurrence = event.occurrence
        }
        else if segue.identifier == SegueIdentifier.inputHtml,
            let richTextEditorViewController = segue.destination as? RichTextEditorViewController {
            richTextEditorViewController.delegate = self
            richTextEditorViewController.html = event.html
        }
        */
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedLogoutButton(_ sender: Any) {
        AppUserInfo.clearData()
        performSegue(withIdentifier: SegueIdentifier.login, sender: self)
    }
    
    @IBAction func onLeftMenu() {
        switch viewType {
        case .add: dismiss(animated: true, completion: nil)
        case .edit, .clone: navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func tappedPickingStartTimeButton(_ sender: Any) {
        tappedAnywhere()
        type = .eventStartTime
        performSegue(withIdentifier: SegueIdentifier.pickTime, sender: sender)
    }
    
    @IBAction func tappedPickingEndTimeButton(_ sender: Any) {
        tappedAnywhere()
        type = .eventEndTime
        performSegue(withIdentifier: SegueIdentifier.pickTime, sender: sender)
    }
    
    @IBAction func tappedToggleEventDetailsTextViewButton(_ button: UIButton) {
        eventDetailsTextView.isHidden = !eventDetailsTextView.isHidden
        let image = eventDetailsTextView.isHidden ? UIImage(named: "button_arrow_down") : UIImage(named: "button_arrow_up")
        button.setImage(image, for: .normal)
//        tableView.beginUpdates()
//        tableView.endUpdates()
        tableView.reloadData()
    }
    
    @IBAction func tappedModifyOccurenceButton() {
        tappedAnywhere()
        if let viewController = OccurenceViewController.instance(occurence: event.occurrence ?? event.occurrences?.first ?? OccurenceDTO(), delegate: self) {
            present(viewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func tappedPostButton(_ sender: Any) {
        func popIfNeeded() {
            if viewType == .edit {
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            } else if viewType == .add {
                event = WhaazonEvent()
                refreshUIData()
            }
        }
        
        func validateEvent() -> Bool {
            if let name = event.name, name.isEmpty {
                presentAlertViewController("Please enter an event name.") { [weak self] in self?.eventNameTextField.becomeFirstResponder() }
                return false
            }
            if event.imageURL == nil, event.images.isEmpty, event.imageURLs.isEmpty {
                presentAlertViewController("Please pick an photo for the event.")
                return false
            }
            if event.occurrence == nil {
                presentAlertViewController("Please choose the event occurrence for the event.")
                return false
            }
            if event.address.coordinate == nil {
                presentAlertViewController("Please set the address for the event.")
                return false
            }
            if event.tags == nil {
                presentAlertViewController("Please choose at least one tag for the event.")
                return false
            }
            return true
        }
        
        tappedAnywhere()
        DLog("event to post = \(event)")
        event.name = eventNameTextField.text
        event.summary = eventDescriptionTextView.text
        event.html = eventDetailsTextView.text.escaped
//        if FileManager.default.fileExists(atPath: imageURL.path) {
//            event.imageURL = imageURL
//        }
        event.images = viewModel.images
        event.htmlURL = htmlURL
        event.tags = pickedTags
        var isRetried = false
        if viewType == .clone {
            // clear id when clone an event to avoid updating the existing one
            event.id = 0
        }
        
        guard validateEvent() else { return }
        
        loadingView.show()
        PortalManager.post(
            event: event,
            success: { [weak self] (result) in
                DLog("posted")
                guard let strongSelf = self else { return }
                strongSelf.loadingView.hide()
                strongSelf.presentAlertViewController("Successfully \(strongSelf.viewType == .edit ? "Updated" : "Posted")", showCancel: false, handler: { popIfNeeded() })
                try? FileManager.default.removeItem(at: strongSelf.imageURL)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.eventUpdated), object: nil, userInfo: nil)
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                DLog("failed, get a new whaazon token and retry")
//                guard let code = error?.code else {
//                    strongSelf.presentAlertViewController("Unknown error when \(strongSelf.viewType == .edit ? "update" : "post") event, please try again later", showCancel: false, handler: { popIfNeeded() })
//                    return
//                }
                if let code = error?.code,
                    code == 401 {
                    if !isRetried {
                        isRetried = true
                        AccountManager.getWhaazonToken({
                            PortalManager.post(
                                event: strongSelf.event,
                                success: { (result) in
                                    strongSelf.loadingView.hide()
                                    DLog("posted after get a new whaazon token and retry")
                                    strongSelf.presentAlertViewController("Successfully \(strongSelf.viewType == .edit ? "Updated" : "Posted")", showCancel: false, handler: { popIfNeeded() })
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.eventUpdated), object: nil, userInfo: nil)
                            },
                                failure: { (error) in
                                    strongSelf.loadingView.hide()
                                    DLog("failed again after get a new whaazon token and retry")
                                    strongSelf.presentAlertViewController("You're not authorized to \(strongSelf.viewType == .edit ? "update" : "post") event, please login and retry.", showCancel: false, handler: {
                                        popIfNeeded()
                                        strongSelf.tappedLogoutButton(strongSelf)
                                    })
                            })
                        }, failure: { (error) in
                            strongSelf.loadingView.hide()
                            DLog(error.debugDescription)
                            strongSelf.presentAlertViewController("You're not authorized to \(strongSelf.viewType == .edit ? "update" : "post") event, please login and retry.", showCancel: false, handler: {
                                popIfNeeded()
                                strongSelf.tappedLogoutButton(strongSelf)
                            })
                        })
                    }
                } else {
                    strongSelf.loadingView.hide()
                    strongSelf.presentAlertViewController("Failed to post event. \nSummary : \(error?.userInfo["summary"] ?? "")\nDetails : \(error?.userInfo["details"] ?? "")", showCancel: false, handler: { popIfNeeded() })
                }
        })
    }
    
    // MARK: - Photo selection
    private var previouslySelectedAssets = [PHAsset]()
    
    @IBAction func tappedPickPhotoButton() {
        tappedAnywhere()
//        showVideoPhotoOptions(button, isVideo: false)
        let vc = BSImagePickerViewController()
        vc.takePhotos = true
        vc.defaultSelections = PHAsset.fetchAssets(withLocalIdentifiers: previouslySelectedAssets.map({ $0.localIdentifier }), options: nil)
        
        bs_presentImagePickerController(vc, animated: true,
            select: { (asset: PHAsset) -> Void in
              // User selected an asset.
              // Do something with it, start upload perhaps?
            }, deselect: { (asset: PHAsset) -> Void in
              // User deselected an assets.
              // Do something, cancel upload?
            }, cancel: { (assets: [PHAsset]) -> Void in
              // User cancelled. And this where the assets currently selected.
            }, finish: { [weak self] (assets: [PHAsset]) -> Void in
                guard let `self` = self else { return }
                // User finished with these assets
                self.previouslySelectedAssets = assets
                DispatchQueue.global().async {
                    let option = PHImageRequestOptions()
                    option.isSynchronous = true
                    var images = [UIImage]()
                    for asset in assets {
                        PHImageManager.default().requestImage(
                            for: asset,
                            targetSize: CGSize(width: 320, height: asset.pixelHeight / asset.pixelWidth * 320),
                            contentMode: .default,
                            options: option,
                            resultHandler: { (image, _) in
                                if let image = image {
                                    images.append(image)
                                }
                        })
                    }
                    DispatchQueue.main.async { [weak self] in
                        self?.viewModel.images = images
                    }
                }
        }, completion: nil)
    }
    
    @IBAction func tappedAddressButton(_ button: UIButton) {
        tappedAnywhere()
        performSegue(withIdentifier: SegueIdentifier.pickAddress, sender: self)
    }
    
    @IBAction func tappedShowMoreButton(_ button: UIButton) {
    }
    
    @objc private func tappedAnywhere() {
        view.endEditing(true)
    }
    
    // MARK: - Login
    
    /// If not logged in, navigated to login view
    fileprivate func checkLogin() {
        let loggedIn = AppUserInfo.hasAccount
        if !loggedIn {
            performSegue(withIdentifier: SegueIdentifier.login, sender: self)
        } else {
            DLog("User logged in with account = \(AppUserInfo.account)")
            welcomeLabel.text = "Welcome come back \(AppUserInfo.account.name ?? "")"
        }
    }

}

// MARK: - Title - UITextFieldDelegate
extension PortalViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        eventDescriptionTextView.becomeFirstResponder()
        return true
    }
}

// MARK: - Content - UITextViewDelegate
extension PortalViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "[Enter a short summary]" {
            textView.text = nil
        }
        return true
    }
}

// MARK: - DateTime picker
extension PortalViewController: DateTimePickDelegate {
    func picked(date: Date?, of type: DateTimeType) {
//        guard let date = date else { return }
//        switch type {
//        case .eventStartTime:
//            event.startTime = date
//            eventStartTimeLabel.text = event.startTime?.displayString
//        case .eventEndTime:
//            event.endTime = date
//            eventEndTimeLabel.text = event.endTime?.displayString
//        case .occurrenceStartTime: event.occurrence?.startDate = date
//        case .occurrenceEndTime: event.occurrence?.endDate = date
//        }
    }
}

// MARK: - Address
extension PortalViewController: AddressPickDelegate {
    func picked(address: Address?) {
        guard let address = address else { return }
        event.address = address
        eventLocationLabel.text = event.address.description
    }
}

extension PortalViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else { return }
        locationManager.stopUpdatingLocation()
        updateAddressWithLocation(location)
    }
    
    private func updateAddressWithLocation(_ location: CLLocation) {
        // Add below code to get address for touch coordinates.
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { [weak self] (placemarks, error) -> Void in
            guard let strongSelf = self else { return }
            if let placeMark = placemarks?.first {
                let address = Address(
                    coordinate: placeMark.location?.coordinate,
                    building: placeMark.name ?? "",
                    street: placeMark.thoroughfare ?? "",
                    suburb: placeMark.locality ?? "",
                    state: placeMark.administrativeArea ?? "",
                    postcode: placeMark.postalCode ?? "",
                    country: placeMark.country ?? "")
                strongSelf.picked(address: address)
            }
        })
    }
}

// MARK: - Occurrence
extension PortalViewController: OccurenceDelegate {
    func occurenceDidUpdated(occurence: OccurenceDTO) {
        event.occurrence = occurence
        eventOccurrenceLabel?.text = event.occurrence?.wordsDescription ?? "Pick an occurrence"
    }
}

/*
// MARK: - HTML
extension PortalViewController: RichTextEditorDelegate {
    func input(html: String) {
        event.html = html
        do {
            try FileManager.default.removeItem(at: htmlURL)
        } catch let error as NSError {
            print("Failed to delete HTML file: \(error)")
        }
        do {
            try event.html?.write(to: htmlURL, atomically: true, encoding: .utf8)
            descriptionWebView.loadHTMLString(html, baseURL: nil)
        } catch let error as NSError {
            print("Failed to write HTML file: \(error)")
        }
    }
}
*/

// MARK: - Photo
/*
extension PortalViewController {
    
    /// Show camera options
    fileprivate func showVideoPhotoOptions(_ button: UIButton, isVideo: Bool) {
        let alertController = UIAlertController(title: "Please Select", message: nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        alertController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            self.tappedAddVideoPhotoButton(button, isVideo: isVideo)
        }
        alertController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose from Albumn", style: .default) { action -> Void in
            self.tappedSelectVideoPhotoButton(button, isVideo: isVideo)
        }
        alertController.addAction(choosePictureAction)
        alertController.popoverPresentationController?.sourceView = button // required for iPad
        present(alertController, animated: true, completion: nil)
    }
    
    /// Take video with camera
    func tappedAddVideoPhotoButton(_ sender: UIButton, isVideo: Bool) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            if isVideo {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
//                imagePicker.videoMaximumDuration = Constant.videoLengthLimitation
            } else {
                imagePicker.mediaTypes = [kUTTypeImage as String]
            }
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select video from album
    func tappedSelectVideoPhotoButton(_ sender: UIButton, isVideo: Bool) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            if isVideo {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
            } else {
                imagePicker.mediaTypes = [kUTTypeImage as String]
            }
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    // MARK: Photo
    
    /// Take photo with camera
    @IBAction func tappedAddPhotoButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Select photo from album
    @IBAction func tappedSelectPhotoButton(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overFullScreen // avoid size of current view being changed
            present(imagePicker, animated: true, completion: nil)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension PortalViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let mediaType = info[UIImagePickerControllerMediaType] as? String, mediaType == (kUTTypeMovie as String) {
            let urlOfVideo = info[UIImagePickerControllerMediaURL] as? URL
            if let url = urlOfVideo {
                video(url, didFinishSavingWithError: nil, contextInfo: "" as AnyObject)
            }
        } else if let mediaType = info[UIImagePickerControllerMediaType] as? String, mediaType == (kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let resizedImageData = image.resizeWith(width: 320)?.mediumQualityJPEGNSData
            let saved = ((try? resizedImageData?.write(to: imageURL, options: [.atomic])) != nil)
            if !saved {
                presentAlertViewController("Failed to save image, please try again.")
            } else {
                eventImageView.image = image
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if let _ = error {
            presentAlertViewController("Failed to save image, please try again.")
        } else {
        }
    }
    
    fileprivate func video(_ videoPath: URL, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
            presentAlertViewController("Failed to save video, please try again.")
        } else {
        }
    }
}
*/

// MARK: - PortalDelegate
extension PortalViewController: PortalDelegate {
    func imagesDidUpdate() {
        collectionView.reloadData()
        collectionView.isHidden = event.imageURLs.isEmpty && viewModel.images.isEmpty
    }
}

// MARK: - UICollectionViewDataSource
extension PortalViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return event.imageURLs.count + viewModel.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        if let cell = cell as? ImageCell {
            if event.imageURLs.count > indexPath.row {
                cell.imageView.kf.setImage(with: URL(string: event.imageURLs[indexPath.row]))
            } else {
                cell.imageView.image = viewModel.images[indexPath.row - event.imageURLs.count]
            }
            
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PortalViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 160)
    }
}

// MARK: - UICollectionViewDelegate
extension PortalViewController: UICollectionViewDelegate, UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let updateAction = UIAlertAction(title: "Update", style: .default) { [weak self] (action) in
            self?.tappedPickPhotoButton()
        }
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [weak self] (action) in
            guard let `self` = self else { return }
            if `self`.event.imageURLs.count > indexPath.row {
                self.event.imageURLs.remove(at: indexPath.row)
            } else {
                self.viewModel.images.remove(at: indexPath.row - self.event.imageURLs.count)
            }
            self.imagesDidUpdate()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(updateAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let temp = viewModel.images.remove(at: sourceIndexPath.item)
        viewModel.images.insert(temp, at: destinationIndexPath.item)
    }

}
