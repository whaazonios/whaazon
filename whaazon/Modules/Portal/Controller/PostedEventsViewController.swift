//
//  PostedEventsViewController.swift
//  whaazon
//
//  Created by Can Zhan on 7/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import UIKit

class PostedEventsViewController: UITableViewController {
    
    struct Identifier {
        static let eventCell = "eventCell"
        static let segue = "eventDetails"
    }
    
    // MARK: - Vars
    private let viewModel = PostedEventsViewModel()
    private let loadingView = LoadingView()
    private var viewType: PortalViewController.PortalViewType = .add
    private var editingEvent: WhaazonEvent?

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingView.show()
        viewModel.fetchPostedEvents(
            success: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.tableView.reloadData()
                strongSelf.loadingView.hide()
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.loadingView.hide()
                strongSelf.presentAlertViewController(error?.localizedDescription)
        })
    }
    
    private func setupUI() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(PostedEventsViewController.pullToRefrsh(_:)), for: .valueChanged)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PortalViewController {
            vc.viewType = viewType
            if let editingEvent = editingEvent {
                vc.event.id = editingEvent.id
                vc.event.name = editingEvent.name
                vc.event.summary = editingEvent.summary
                vc.event.startTime = editingEvent.startTime
                vc.event.endTime = editingEvent.endTime
                vc.event.address = editingEvent.address
                vc.event.occurrence = editingEvent.occurrence
                vc.event.occurrences = editingEvent.occurrences
                vc.event.imageURL = editingEvent.imageURL
                vc.event.tags = editingEvent.tags
                vc.event.html = editingEvent.html
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedLogoutButton(_ sender: Any) {
        AppUserInfo.clearData()
        if let portalViewController = tabBarController?.viewControllers?.compactMap({ ($0 as? UINavigationController)?.viewControllers.first as? PortalViewController }).first {
            portalViewController.tappedLogoutButton(sender)
        }
    }
    
    @IBAction func tappedLeftMenu() {
//        guard let slideMenuController = slideMenuController() else { return }
//        slideMenuController.isLeftOpen() ? slideMenuController.closeLeft() : slideMenuController.openLeft()
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.events.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.eventCell, for: indexPath)
        if let cell = cell as? WhaazonTableViewCell {
            cell.viewModel = viewModel.cellViewModelForRowAt(indexPath)
        }
        return cell
    }
    
    // MARK: - Delete event
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.deleteEventAt(indexPath)
        }
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            self.editEventAt(indexPath)
        }
        let clone = UITableViewRowAction(style: .normal, title: "Clone") { (action, indexPath) in
            self.cloneEventAt(indexPath)
        }
        return [delete, edit, clone]
    }
    
    private func deleteEventAt(_ indexPath: IndexPath) {
        presentAlertViewController("Delete the event?", showCancel: true) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.deleteEventAt(indexPath,
                                               success: { strongSelf.tableView.deleteRows(at: [indexPath], with: .automatic) },
                                               failure: { error in strongSelf.presentAlertViewController(error?.localizedDescription) })
        }
    }
    
    private func editEventAt(_ indexPath: IndexPath) {
        viewType = .edit
        editingEvent = viewModel.events.remove(at: indexPath.row)
        performSegue(withIdentifier: Identifier.segue, sender: self)
    }
    
    private func cloneEventAt(_ indexPath: IndexPath) {
        viewType = .clone
        editingEvent = viewModel.events.remove(at: indexPath.row)
        performSegue(withIdentifier: Identifier.segue, sender: self)
    }
    
    // MARK: - Pull to Refresh
    
    @objc private func pullToRefrsh(_ refreshControl: UIRefreshControl) {
        viewModel.fetchPostedEvents(
            success: { [weak self] in
                refreshControl.endRefreshing()
                guard let strongSelf = self else { return }
                strongSelf.tableView.reloadData()
            },
            failure: { [weak self] (error) in
                refreshControl.endRefreshing()
                guard let strongSelf = self else { return }
                strongSelf.presentAlertViewController(error?.localizedDescription)
            })
    }
}
