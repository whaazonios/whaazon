//
//  RichTextEditorViewController.swift
//  whaazon
//
//  Created by Can Zhan on 2/6/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import UIKit

protocol RichTextEditorDelegate {
    func input(html: String)
}

class RichTextEditorViewController: ZSSRichTextEditor {
    
    var delegate: RichTextEditorDelegate?
    var html: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeholder = "This is a placeholder that will show when there is no content(html)"
        setHTML(html)               
        shouldShowKeyboard = false
        alwaysShowToolbar = true
    }

    @IBAction func exportHTML() {
        delegate?.input(html: getHTML())
        navigationController?.popViewController(animated: true)
    }
}
