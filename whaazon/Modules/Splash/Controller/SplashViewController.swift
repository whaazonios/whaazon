//
//  SplashViewController.swift
//  whaazon
//
//  Created by Can on 6/09/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    
    fileprivate var timer: Timer?
    
    struct SegueIdentifier {
        static let main = "main"
    }
    
    struct UserDefaultsKey {
        static let acceptedTermsAndConditions = "acceptedTermsAndConditions"
    }
    
    @IBOutlet weak var termsAndConditionsView: UIView!
    
    /// Terms and Conditions have ever been accepted
    var acceptedTermsAndConditions: Bool {
        return userDefaults.bool(forKey: UserDefaultsKey.acceptedTermsAndConditions)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        termsAndConditionsView.isHidden = acceptedTermsAndConditions
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if acceptedTermsAndConditions {
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SplashViewController.showMain), userInfo: nil, repeats: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        super.viewDidDisappear(animated)
        timer?.invalidate()
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func tappedAcceptButton() {
        userDefaults.set(true, forKey: UserDefaultsKey.acceptedTermsAndConditions)
        showMain()
    }

    @objc func showMain() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.performSegue(withIdentifier: SegueIdentifier.main, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let
            navigationController = segue.destination as? UINavigationController,
            let disclaimerViewController = navigationController.topViewController as? DisclaimerViewController
        {
            disclaimerViewController.fromSplash = true
        }
    }
    
    @IBAction func unwindToSplashWithSegue(_ segue: UIStoryboardSegue) {}
    
}
