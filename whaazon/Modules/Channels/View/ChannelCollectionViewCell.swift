//
//  ChannelCollectionViewCell.swift
//  whaazon
//
//  Created by Can on 22/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class ChannelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var channelImageView: UIImageView!
    @IBOutlet weak var domainNameLabel: UILabel!
    @IBOutlet weak var tickImageView: UIImageView!
    
    var isChosen: Bool = false {
        didSet {
            tickImageView.isHidden = !isChosen
        }
    }
    
    func toggleSelection() {
        isChosen = !isChosen
    }
}
