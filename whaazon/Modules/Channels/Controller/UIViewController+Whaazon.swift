//
//  UIViewController+Whaazon.swift
//  whaazon
//
//  Created by Can Zhan on 14/6/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController {
    
    func hasWechatInstalled() -> Bool {
        return false
    }
    
    /// Launch Wechat
    func launchWechat() {
        if let url = URL(string: "weixin://") {
            UIApplication.shared.openURL(url)
        }
    }
    
    func presentAlertViewController(_ errorString: String?) {
        presentAlertViewController(errorString, showCancel: false, handler: nil)
    }
    
    /// Show Alert View
    func presentAlertViewController(_ message: String?, showCancel: Bool = false, handler: (() -> Void)?) {
        let alertViewController = UIAlertController(title: "", message: message ?? "", preferredStyle: .alert)
        
        let alertOKButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { _ in
            handler?()
        }
        alertViewController.addAction(alertOKButton)
        
        if showCancel {
            alertViewController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        }
        
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
        }
        
    }
    
    /// Dismiss View Controller
    @IBAction func dismiss(_ sender: AnyObject?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Used to get file path for Video/Audio/Photo
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first
        return documentsDirectory!
    }
    
}
