//
//  ChooseChannelViewController.swift
//  whaazon
//
//  Created by Can on 22/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol ChannelDelegate {
    func channelsUpdated()
}

class ChooseChannelViewController: UIViewController {
    
    fileprivate struct CellIdentifier {
        static let channel = "channel"
    }
    
    var delegate: ChannelDelegate?

    @IBAction func tappedConfirmButton() {
        dismiss(true)
    }
    
    @IBAction func tappedCancelButton() {
        dismiss()
    }
    
    private func dismiss(_ hasUpdate: Bool = false) {
        if Preferences.selectedDomains.isEmpty {
            let alertViewController = UIAlertController(title: "No selection yet", message: "Please select at least 1 channel to explore events around you.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertViewController.addAction(okAction)
            present(alertViewController, animated: true, completion: nil)
        } else {
            if hasUpdate {
                delegate?.channelsUpdated()
                if delegate == nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.eventUpdated), object: nil, userInfo: nil)
                }
            }
            dismiss(animated: true, completion: nil)
        }
    }
}

extension ChooseChannelViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Domain.allValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.channel, for: indexPath) as! ChannelCollectionViewCell
        cell.channelImageView.image = Domain.allValues[indexPath.item].values.logo
        cell.domainNameLabel.text = Domain.allValues[indexPath.item].rawValue
        cell.isChosen = UserDefaults.standard.domains.contains(Domain.allValues[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let domain = Domain.allValues[indexPath.item]
        Preferences.toggle(domain: domain)
        let cell = collectionView.cellForItem(at: indexPath) as! ChannelCollectionViewCell
        cell.toggleSelection()
    }

}
