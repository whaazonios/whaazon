//
//  EventManager.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 14/12/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct EventManager {
    
    static func loadEventsFromLocalFile(eventDelegate: EventDelegate) {
        if let jsonFilePathString = Bundle.main.path(forResource: "CitiesOfOZEvents", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: jsonFilePathString))
                let json = try JSON(data: data)
                // construct json
                var events = [Event]()
                for (_, subjson):(String, JSON) in json {
                    let event = CityOfOZEvent(json: subjson)
                    events.append(event)
                }
                print("update view with \(events.count) events")
                eventDelegate.updateViewWithEvents(events, filterEvents: true)
            } catch {
                eventDelegate.updateViewWithError("failed to load data from \(jsonFilePathString)")
            }
        }
    }
    
    static func loadEventsFromLocalFile(completion: @escaping ([Event]) -> Void) {
        if let jsonFilePathString = Bundle.main.path(forResource: "CitiesOfOZEvents", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: jsonFilePathString))
                let json = try JSON(data: data)
                // construct json
                var events = [Event]()
                for (_, subjson):(String, JSON) in json {
                    let event = CityOfOZEvent(json: subjson)
                    events.append(event)
                }
                print("update view with \(events.count) events")
                completion(events)
            } catch {
                DLog("failed to load data from \(jsonFilePathString)")
            }
        }
    }
}
