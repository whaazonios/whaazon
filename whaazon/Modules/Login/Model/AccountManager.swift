//
//  AccountManager.swift
//  timemanager
//
//  Created by Can on 2/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AccountManager {
    
    fileprivate struct EndPoint {
        static let getAccountFamilies = "api/account-families"
        static let getAccountFamilyWithId = "api/account-families/%@"
        static let getAccount = "api/account"
        static let getTeachersProfile = "api/account-families/getAccountFamilyInfoDTO"
        static let getManagedStudents = "api/student-infos/byTeacherId"
        static let getExpiryDate = "api/account-families/getExpiryDate"
        static let getWhaazonSessionToken = "MapForEventsServer1/auth/fb/getSessCookie"
    }
    
    /// 根据token获取后台当前登录用户的Account对象，返回值带有student，guardian，或teacher对象，并包括AccountFamilyId在其中
    static func fetchAccount(_ success: @escaping (() -> Void), failure: @escaping ((NSError?) -> Void)) {
        Alamofire.request(
            Api.baseURL + EndPoint.getAccount,
            method: .get,
            headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let data):
                    DLog("received account = \(data)")
                    let json = JSON(data)
                    if let account = Account(json: json) {
                        AppUserInfo.account = account
                    }
                    success()
                case .failure(let error):
                    ErrorManager.handle(alamoFire: error, failure: failure)
                }
        }
    }
    
    //MARK: Get Whaazon access token
    
    private static var manager: Alamofire.SessionManager = {
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "m4e.chen.id.au:8443": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    static func getWhaazonToken(_ success: @escaping (() -> Void), failure: @escaping ((NSError?) -> Void)) {
        // Handle Authentication challenge
        let delegate: Alamofire.SessionDelegate = AccountManager.manager.delegate
        delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = AccountManager.manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            return (disposition, credential)
        }
        
        //Web service Request
        let parameters: Parameters = [
            "firstname": AppUserInfo.account.firstname ?? "tester",
            "familyname": AppUserInfo.account.familyname ?? "tester",
            "email": AppUserInfo.account.email ?? "test@test.com",
            "channel": AppUserInfo.account.channel ?? "Coffee Chase",
            "url": AppUserInfo.account.url ?? Api.baseURL + "uploadEvent/true"
        ]
        
        AccountManager.manager.request(
            Api.baseURL + EndPoint.getWhaazonSessionToken,
            method: .post,
            parameters: parameters,
            headers :headers)
            .responseString(completionHandler: { (response) in
                debugPrint(response)
                
            })
            .responseJSON { response in
                debugPrint(response)
                switch response.result {
                case .success(let data):
                    DLog("received account = \(data)")
                    AppUserInfo.whaazonAccessToken = JSON(data)["details"].stringValue
                    DispatchQueue.main.async {
                        success()
                    }
                case .failure(let error):
                    DLog("AccountManager.Manager.request error = \(error.localizedDescription)")
                    debugPrint("\n\n*********************** \(#file).\(#function) \n\nresponse = \n\n\(error.localizedDescription)\n\n")
                    ErrorManager.handle(alamoFire: error, failure: failure)
                    DispatchQueue.main.async {
                        failure(nil)
                    }
                }
            }
        }
    
}
