//
//  AppUserInfo.swift
//  beta
//
//  Created by Can on 11/04/2016.
//  Copyright © 2016 Optus. All rights reserved.
//

import Foundation

class AppUserInfo {
    fileprivate static let keychain = KeychainHandler()
    fileprivate static let userDefaults = UserDefaults.standard
    
    class var account: Account {
        get {
            // TODO: block app if Account is not available
            let decoded = userDefaults.object(forKey: UserDefaultsKey.account) as! Data
            let decodedAccount = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! Account
            return decodedAccount
        }
        set {
            userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: newValue), forKey: UserDefaultsKey.account)
            userDefaults.synchronize()
        }
    }
    
    class var hasAccount: Bool {
        var has = false
        if let decoded = userDefaults.object(forKey: UserDefaultsKey.account) as? Data, NSKeyedUnarchiver.unarchiveObject(with: decoded) is Account {
            has = true
        }
        return has
    }
    
    class var employeeFirstName: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.firstName)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.firstName)
        }
    }
    
    class var employeeID: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.employeeId)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.employeeId)
        }
    }
    
    class var accessToken: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.accessToken)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.accessToken)
        }
    }
    
    class var whaazonAccessToken: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.whaazonAccessToken)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.whaazonAccessToken)
        }
    }
    
    class var refreshToken: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.refreshToken)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.refreshToken)
        }
    }
    
    class var employeeEmailAddress: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.emailAddress)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.emailAddress)
        }
    }
    
    class var username: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.username)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.username)
        }
    }
    
    class var validPassword: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.validPassword)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.validPassword)
        }
    }
    
    class var mobileNumber: String {
        get {
            return keychain.searchKeychainByIdentifier(KeychainIdentifier.mobileNumber)
        }
        
        set {
            keychain.storeKeychainValue(newValue, identifier: KeychainIdentifier.mobileNumber)
        }
    }
    
    class func clearData() {
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.accessToken)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.whaazonAccessToken)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.accessTokenWType)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.emailAddress)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.employeeId)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.firstName)
//        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.validPassword)
        keychain.deleteKeychainValueByIdentifier(KeychainIdentifier.mobileNumber)
        userDefaults.removeObject(forKey: UserDefaultsKey.account)
    }
    
}
