//
//  KeychainHandler.swift
//  wingman
//
//  Created by Chau Hua on 2/11/2015.
//  Copyright © 2015 Optus. All rights reserved.
//

import Foundation
import Security

struct KeychainIdentifier {
    static let employeeId = "employee_id"
    static let firstName = "first_name"
    static let accessToken = "access_token"
    static let whaazonAccessToken = "whaazon_access_token"
    static let refreshToken = "refresh_token"
    static let emailAddress = "email_address"
    static let username = "username"
    static let validPassword = "valid_password"
    static let accessTokenWType = "access_token_with_type"
    static let mobileNumber = "mobile_number"
}

// TODO: Create class to access Keychain data items and expose them as properties
class KeychainHandler: NSObject {
    
    let appName = "whaazon"
    var success = false
    
    func storeKeychainValue(_ value: String, identifier: String) -> Bool {
        if value.isEmpty {
            success = deleteKeychainValueByIdentifier(identifier)
        } else {
            var dictionary = setupSearchDirectoryForIdentifier(identifier)
            let valueData = value.data(using: String.Encoding.utf8)!
            dictionary[kSecValueData as AnyHashable] = valueData
            dictionary[kSecAttrAccessible as AnyHashable] = kSecAttrAccessibleAlways
            let status = SecItemAdd(dictionary as CFDictionary, nil)
            switch status {
            case errSecSuccess:
                success = true
            case errSecDuplicateItem:
                success = updateKeychainValue(value, identifer: identifier)
            default:
                success = false
            }
        }
        return success
    }
    
    func updateKeychainValue(_ value: String, identifer: String) -> Bool {
        let searchDictionary = setupSearchDirectoryForIdentifier(identifer)
        let valueData = value.data(using: String.Encoding.utf8)!
        let updateDictionary: [AnyHashable: Any] = [kSecValueData as AnyHashable: valueData]
        let status = SecItemUpdate(searchDictionary as CFDictionary, updateDictionary as CFDictionary)
        if (status == errSecSuccess) {
            success = true
        } else {
            success = false
        }
        return success
    }
    
    func setupSearchDirectoryForIdentifier(_ identifer: String) -> [AnyHashable: Any] {
        let encodedIdentifier = identifer.data(using: String.Encoding.utf8)!
        let searchDictionary: [AnyHashable: Any] = [kSecClass as AnyHashable: kSecClassGenericPassword,
            kSecAttrService as AnyHashable: appName,
            kSecAttrGeneric as AnyHashable: encodedIdentifier,
            kSecAttrAccount as AnyHashable: encodedIdentifier]
        return searchDictionary
    }
    
    func deleteKeychainValueByIdentifier(_ identifier: String) -> Bool {
        let searchDictionary = setupSearchDirectoryForIdentifier(identifier)
        let status: OSStatus = SecItemDelete(searchDictionary as CFDictionary)
        if (status == errSecSuccess) {
            success = true
        } else {
            success = false
        }
        return success
    }
    
    func searchKeychainByIdentifier(_ identifier: String) -> String {
        let kSecMatchLimitKey = kSecMatchLimit
        let kSecReturnDataKey = kSecReturnData
        var searchDictionary = setupSearchDirectoryForIdentifier(identifier)
        searchDictionary[kSecMatchLimitKey as AnyHashable] = kSecMatchLimitOne
        searchDictionary[kSecReturnDataKey as AnyHashable] = kCFBooleanTrue
        var passcode = ""
        var dataTypeRef: AnyObject? = nil
        let status = SecItemCopyMatching(searchDictionary as CFDictionary, &dataTypeRef)
        if status == errSecSuccess {
            passcode = NSString(data: dataTypeRef as! Data, encoding: String.Encoding.utf8.rawValue) as! String
        }
        return passcode
    }
    
    func removeAllObjects() {
        let searchDictionary: [AnyHashable: Any] = [kSecClass as AnyHashable: kSecClassGenericPassword,
            kSecAttrService as AnyHashable: appName]
        SecItemDelete(searchDictionary as CFDictionary)
    }
}
