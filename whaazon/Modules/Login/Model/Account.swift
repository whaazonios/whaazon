//
//  Account.swift
//  timemanager
//
//  Created by Can on 30/07/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

enum Role: String {
    case user = "ROLE_USER"
    case admin = "ROLE_ADMIN"
}

/// Currently Logged in User Account
class Account: NSObject, NSCoding {
    
    /// User ID
    var id: String
    /// Role of current logged in user
    var role: Role
    var firstname: String?
    var familyname: String?
    var email: String?
    var channel: String?
    var url: String?
    
    var isUser: Bool { return role == .user }
    var isAdmin: Bool { return role == .admin }
    var name: String { return "\(firstname ?? "") \(familyname ?? "")" }
    
    init(id: String, role: Role) {
        self.id = id
        self.role = role
    }
    
    init?(json: JSON) {
        guard let id = json["login"].string else { return nil }
        guard let role = Role(rawValue: json["authorities"][0].stringValue) else { return nil }
        self.id = id
        self.role = role
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let role = Role(rawValue: aDecoder.decodeObject(forKey: "role") as! String)!
        self.init(id: id, role: role)
        let firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        let familyname = aDecoder.decodeObject(forKey: "familyname") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        self.firstname = firstname
        self.familyname = familyname
        self.email = email
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(role.rawValue, forKey: "role")
        aCoder.encode(firstname, forKey: "firstname")
        aCoder.encode(familyname, forKey: "familyname")
        aCoder.encode(email, forKey: "email")
    }
    
}
