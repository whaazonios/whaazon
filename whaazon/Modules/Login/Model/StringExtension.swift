//
//  StringExtension.swift
//  wingman
//
//  Created by Chau Hua on 12/11/2015.
//  Copyright © 2015 Optus. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
