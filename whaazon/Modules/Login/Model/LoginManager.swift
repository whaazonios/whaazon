//
//  LoginManager.swift
//  beta
//
//  Created by Can on 11/04/2016.
//  Copyright © 2016 Optus. All rights reserved.
//

import Foundation
import SwiftyJSON

let LoginManagerDidAuthenticateUserNotification = "com.waigi.cdm.LoginManagerDidAuthenticateUserNotification"
let LoginManagerDidRequireAuthenticationNotification = "com.waigi.cdm.LoginManagerDidRequireAuthenticationNotification"

let AccountNotAvailableErrorCode = 9999

class LoginManager {
    struct ServerError {
        static let InvalidGrant = "invalid_grant"
    }
    
    static let sharedManager = {
        return LoginManager()
    }()
    
    fileprivate init() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginManager.authTokenInvalidated(_:)), name: NSNotification.Name(rawValue: ServiceManagerDidInalidateAuthTokenNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func login(_ username: String, password: String, success: @escaping (() -> Void), failure: @escaping ((NSError?) -> Void)) {
        let bodyString = "username=\(username)&password=\(password)&grant_type=password"
        let loginRequest = ServiceManager.generateRequest("oauth/token", requestType: "POST", requestContentType: .Form, requestAcceptType: .JSON, authorisationType: .basic, requestHttpBody: bodyString.data(using: String.Encoding.utf8))
        
        ServiceManager.httpRequest(loginRequest as URLRequest) { (data, error) in
            var successful = false
            var returnedError: NSError?
            
            if data != nil {
                do {
                    if let loginResponse = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String: AnyObject] {
                        DLog("loginResponse = \(loginResponse)")
                        if let
                            access_token = loginResponse["access_token"] as? String,
                            let refresh_token = loginResponse["refresh_token"] as? String
                        {
                            AppUserInfo.accessToken = access_token
                            AppUserInfo.refreshToken = refresh_token
                            AppUserInfo.username = username
                            AppUserInfo.validPassword = password
                            successful = true
                        }
                    } else {
                        returnedError = ServiceManager.handleErrorResponse(false, data: nil, errorCode: 0)
                    }
                } catch let jsonError as NSError {
                    returnedError = jsonError
                }
            } else {
                returnedError = ServiceManager.handleErrorResponse(false, data: nil, errorCode: 0)
            }
            
            if successful {
                success()
            } else {
                failure(returnedError)
            }
        }
    }
        
    @objc fileprivate func authTokenInvalidated(_ notification: Notification) {
        logout()
    }
    
    func logout() {
        AppUserInfo.clearData()
        NotificationCenter.default.post(name: Notification.Name(rawValue: LoginManagerDidRequireAuthenticationNotification), object: nil)
    }
}
