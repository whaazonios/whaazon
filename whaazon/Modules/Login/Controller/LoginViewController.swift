//
//  LoginViewController.swift
//  timemanager
//
//  Created by Can on 2/08/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
//import FacebookCore
//import FacebookLogin
//import GoogleSignIn
import Firebase
import FirebaseUI
//import FirebaseAuthUI
//import FirebaseGoogleAuthUI
//import FirebaseFacebookAuthUI
//import FirebasePhoneAuthUI

class LoginViewController: UITableViewController {

    struct SegueIdentifier {
        static let unwindToHome = "unwindToHome"
    }
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private let loadingView = LoadingView()
    private let authUI = FUIAuth.defaultAuthUI()
    
    var dismissHandler: (() -> Void)?
    var signedInHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configAuthUI()
        showLogin()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showLogin)))
    }
    
    private func configAuthUI() {
        authUI?.delegate = self
        let providers: [FUIAuthProvider] = [
            FUIGoogleAuth(),
            FUIFacebookAuth(),
            FUIPhoneAuth(authUI: FUIAuth.defaultAuthUI()!),
        ]
        self.authUI?.providers = providers
    }

    @IBAction func tappedLoginButton() {
//        guard let username = usernameTextField.text,
//            let password = passwordTextField.text
//            else { return }
//        if username.isEmpty || password.isEmpty {
//            presentAlertViewController("Provide username and password to login.")
//            return
//        }
//        LoginManager.sharedManager.login(username, password: password,
//        success: {
//            DLog("Login success，token ＝ \(AppUserInfo.accessToken)")
//            AccountManager.fetchAccount(
//                {
//                    if AppUserInfo.hasAccount {
//                        self.navigationController?.dismiss(self)
//                    } else {
//                        self.presentAlertViewController("Can't find your account, please contact customer service.")
//                    }
//                }, failure: { (error) in
//                    self.presentAlertViewController("Failed to get your account infomation, please try again.")
//            })
//        },
//        failure: { error in
//            if let error = error {
//                self.presentAlertViewController(error.description)
//            } else {
//                self.presentAlertViewController("Failed to login, please try again.")
//            }
//        })
    }
    
    @IBAction func onLeftMenu() {
        dismissHandler?()
    }
    
    @objc @IBAction func tappedLoginWithFacebookButton() {
        /*
        let loginManager = FacebookLogin.LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile, ReadPermission.email], viewController: self) { [weak self] (loginResult: LoginResult) in
            switch loginResult {
            case .success(let grantedPermissions, let declinedPermissions, let token):
                // logged in successfully, make another request to fetch user info
                let connection = GraphRequestConnection()
                let request = GraphRequest(graphPath: "/me", parameters: ["fields": "id, first_name, last_name, email"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: .defaultVersion)
                connection.add(request) { httpResponse, result in
                    switch result {
                    case .success(let response):
                        guard let id = response.dictionaryValue?["id"] as? String else { return }
                        let firstname = response.dictionaryValue?["first_name"] as? String
                        let familyname = response.dictionaryValue?["last_name"] as? String
                        let email = response.dictionaryValue?["email"] as? String
                        let account = Account(id: id, role: .user)
                        account.firstname = firstname
                        account.familyname = familyname
                        account.email = email
                        // save user details
                        AppUserInfo.account = account
                        // request Access Token to interact with portal services
                        self?.requestAccessToken()
                    case .failed(let error):
                        print("Graph Request Failed: \(error)")
                    }
                }
                connection.start()
            case .cancelled:
                print("cancelled")
            case .failed(let error):
                print(error)
            }
        }
 */
    }
    
    @objc @IBAction func tappedLoginWithGoogleButton() {
//        GIDSignIn.sharedInstance()?.signIn()
    }
    
    private func requestAccessToken() {
        loadingView.show()
        AccountManager.getWhaazonToken({ [weak self] in
            self?.loadingView.hide()
            print("AccountManager.getWhaazonToken succeeded.")
            // dismiss login page if succeeded
            self?.navigationController?.dismiss(animated: false, completion: {
                self?.signedInHandler?()
            })
        }, failure: { [weak self] (error) in
            self?.loadingView.hide()
            DLog(error.debugDescription)
            print("AccountManager.getWhaazonToken failed.")
            self?.presentAlertViewController("Failed to login, please try again.", showCancel: true, handler: { [weak self] in
                self?.showLogin()
            })
        })
    }
    
    @objc private func showLogin() {
        if let authViewController = authUI?.authViewController() {
            present(authViewController, animated: true, completion: nil)
        }
    }

}
/*
extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            user.authentication.accessToken
            // ...
        }
    }
}
*/

extension LoginViewController: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        guard let user = user else { return }
        let account = Account(id: user.providerID, role: .user)
        account.firstname = user.displayName
        account.familyname = nil
        account.email = user.email
        // save user details
        AppUserInfo.account = account
        // request Access Token to interact with portal services
        requestAccessToken()
    }
}
