//
//  EventPreviewView.swift
//  Whaazon
//
//  Created by Can Zhan on 2/8/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

class EventPreviewView: UIView {
    
    var tappedHandler: (() -> Void)?
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let summaryLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = ContentMode.scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        // round corner & border
        applyBorder(0.0)
        clipsToBounds = true
        
        // Stack
        let stackView = UIStackView(arrangedSubviews: [titleLabel, timeLabel, imageView, summaryLabel])
        stackView.axis = .vertical
        stackView.spacing = 8.0
        stackView.distribution = .fill
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 0.0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0.0).isActive = true
        trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 0.0).isActive = true
        bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 0.0).isActive = true
        
//        stackView.leadingAnchor.constraint(equalTo: timeLabel.leadingAnchor).isActive = true
//        timeLabel.trailingAnchor.constraint(greaterThanOrEqualTo: stackView.trailingAnchor).isActive = true
        
        imageView.widthAnchor.constraint(equalToConstant: 200.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 150.0).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedView)))
    }
    
    @objc private func tappedView() {
        tappedHandler?()
    }
}
