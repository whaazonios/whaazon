//
//  DateRange.swift
//  whaazon
//
//  Created by Can Zhan on 6/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Foundation
import SwiftDate

/// A Date Range contains a start and an end date which are the boundaries of the range.
struct DateRange {
    
    let startDate: Date
    let endDate: Date
    
    init(startDate: Date, endDate: Date) {
        self.startDate = startDate
        self.endDate = endDate
    }
    
    /// Produce a DateRange with the same day of theDay, and same time of given intervalsInDay
    /// The input intervals are in current Region, return value will be converted to UTC time
    /// e.g. theDay: 2019-3-7, intervalsInDay: "12:30:0-14:30:0", DateRange.startDate will be "2019-3-7 1:30:00", DateRange.endDate will be "2019-3-7 2:30:00"
    init?(theDay: Date, intervalsInDay: String) {
        let cal = Calendar.current
        let components = cal.dateComponents([.year, .month, .day], from: theDay)
        
        let times = intervalsInDay.split(separator: "-")
        guard times.count == 2 else { return nil }
        let startTimeComponents = times[0].split(separator: ":")
        let endTimeComponents = times[1].split(separator: ":")
        guard startTimeComponents.count == 3, endTimeComponents.count == 3 else { return nil }
        guard let year = components.year,
            let month = components.month,
            let day = components.day,
            let startTimeHour = Int(startTimeComponents[0]),
            let startTimeMinute = Int(startTimeComponents[1]),
            let endTimeHour = Int(endTimeComponents[0]),
            let endTimeMinute = Int(endTimeComponents[1])
            else { return nil }
        
        startDate = DateInRegion(year: year, month: month, day: day, hour: startTimeHour, minute: startTimeMinute, region: Region.current).date
        endDate = DateInRegion(year: year, month: month, day: day, hour: endTimeHour, minute: endTimeMinute, region: Region.current).date
    }
    
    /// Check if there is an intersection between given range and current range
    func hasIntersectionWith(_ dateRange: DateRange) -> Bool {
        return !(self.endDate < dateRange.startDate || self.startDate > dateRange.endDate)
    }
    
    /// Find out intersection between two DateRanges, if there is no intersection, return nil
    static func intersectionBetween(_ dateRange1: DateRange, _ dateRange2: DateRange) -> DateRange? {
        guard dateRange1.hasIntersectionWith(dateRange2) else { return nil }
        let dates = [dateRange1.startDate, dateRange1.endDate, dateRange2.startDate, dateRange2.endDate].sorted()
        // intersection is always between two middle dates
        return DateRange(startDate: dates[1], endDate: dates[2])
    }
    
}

func < (lhs: DateRange, rhs: DateRange) -> Bool {
    if lhs.startDate < rhs.startDate {
        return true
    } else if lhs.startDate > rhs.startDate {
        return false
    } else {
        return lhs.endDate < rhs.endDate
    }
}

func == (lhs: DateRange, rhs: DateRange) -> Bool {
    return lhs.startDate == rhs.startDate && lhs.endDate == rhs.endDate
}

extension DateRange: Equatable, Comparable {
}
