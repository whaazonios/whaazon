//
//  SimpleWebViewController.swift
//  VirginMobile
//
//  Created by Can on 25/05/2016.
//  Copyright © 2016 VirginMobile. All rights reserved.
//

import UIKit

@IBDesignable class SimpleWebViewController: UIViewController {
    
    // TODO: change url as computed variable and move the class to Control module.
    var url: URL?
    var fileName: String?
    
    @IBInspectable var urlString: String = "" {
        didSet {
            url = URL(string: urlString)
        }
    }
    
    @IBInspectable var screenTitle: String = ""

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = screenTitle
        webView.delegate = self
        if let filePath = Bundle.main.path(forResource: fileName, ofType: nil) {
            let request = URLRequest(url: URL(fileURLWithPath: filePath))
            webView.loadRequest(request)
        } else if let url = url {
            webView.loadRequest(URLRequest(url: url))
        }
    }
    
    @IBAction func onLeftMenu() {
        guard let slideMenuController = self.slideMenuController() else { return }
        slideMenuController.isLeftOpen() ? slideMenuController.closeLeft() : slideMenuController.openLeft()
    }
    
}

extension SimpleWebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.stopAnimating()
    }
    
}
