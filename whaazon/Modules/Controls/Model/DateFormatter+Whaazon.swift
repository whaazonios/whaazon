//
//  DateFormatter+Whaazon.swift
//  whaazon
//
//  Created by Can Zhan on 28/5/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /// Is current System time format setting 12 or 24 hours
    static var is12HourFormat: Bool {
        let formatString = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
        let hasAMPM = formatString.contains("a")
        return hasAMPM
    }
    
    static var displayFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE d MMM yyyy h:mm aa"
        return formatter
    }
    
    /// "15:0:0"
    static var intervalsInDayFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }
    
    /// Formatter with the pattern of "dd/MMM/yyyy". e.g. "15/Feb/2018"
    static let dateOnlyDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "EEE d MMM yyyy h:mm aa". e.g. Mon 17 Oct 2016 11:00 am - 12:00 pm
    static let eventSubTitleDateFormatter1: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DISPLAY_FORMAT
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "h:mm aa". e.g. "12:00 pm"
    static let eventSubTitleDateFormatter2: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DISPLAY_FORMAT2
        return dateFormatter
    }()
    
}
