//
//  Date+Whaazon.swift
//  whaazon
//
//  Created by Can Zhan on 28/5/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation

extension Date {
    var displayString: String {
        return DateFormatter.displayFormatter.string(from: self)
    }
}
