//
//  AppData.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 12/12/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation

struct AppData {
    
    /// Hold events in memory to speed up events filtering while sliding the time slider
    static var events = [Event]()
    /// Filters and options reading from FilterAttribute.json
    static var domainFilters = [DomainFilter]()
}
