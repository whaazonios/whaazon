//
//  UserDefaults+Whaazon.swift
//  whaazon
//
//  Created by Can on 22/12/16.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation

struct UserDefaultsKey {
    static let domains = "domains"
    static let filteringDomain = "filteringDomain"
    static let account = "account"
    static let timeSliderStartTime = "timeSliderStartTime"
    static let timeSliderEndTime = "timeSliderEndTime"
}

extension UserDefaults {
    
    /// User selected domains
    var domains: [Domain] {
        get {
            return (object(forKey: UserDefaultsKey.domains) as? [String])?.flatMap({ Domain(rawValue: $0) }) ?? [Domain]()
        }
        set {
            set(newValue.map({ $0.rawValue }), forKey: UserDefaultsKey.domains)
        }
    }
    
    /// The Domain currently shows filters
    var filteringDomain: Domain? {
        get {
            if let specificDomain = specificDomain {
                // single domain case
                return specificDomain
            } else {
                // multiple domains case
                return string(forKey: UserDefaultsKey.filteringDomain).flatMap{ Domain(rawValue: $0) } ?? domains.first
            }
        }
        set {
            set(newValue?.rawValue, forKey: UserDefaultsKey.filteringDomain)
        }
    }
    
    /// Next available user selected domain
    var nextFilteringDomain: Domain? {
        var nextFilteringDomain: Domain? = nil
        let domains = self.domains
        if !domains.isEmpty {
            if let filteringDomain = self.filteringDomain,
                let currentIndex = domains.index(of: filteringDomain)
            {
                nextFilteringDomain = domains[(currentIndex + 1) % domains.count]
            } else {
                nextFilteringDomain = domains.first
            }
        }
        return nextFilteringDomain
    }
    
    /// Previous available user selected domain
    var previousFilteringDomain: Domain? {
        var previousFilteringDomain: Domain? = nil
        let domains = self.domains
        if !domains.isEmpty {
            if let filteringDomain = self.filteringDomain,
                let currentIndex = domains.index(of: filteringDomain)
            {
                previousFilteringDomain = domains[(currentIndex - 1) >= 0 ? currentIndex - 1 : domains.count - 1 ]
            } else {
                previousFilteringDomain = domains.first
            }
        }
        return previousFilteringDomain
    }
    
    /// last begin position of time slider
    var timeSliderStartTime: Date {
        get {
            return object(forKey: UserDefaultsKey.timeSliderStartTime) as? Date ?? Date().dateByAdding(-1, .day).date
        }
        set {
            set(newValue, forKey: UserDefaultsKey.timeSliderStartTime)
        }
    }
    
    /// last end position of time slider
    var timeSliderEndTime: Date {
        get {
            return object(forKey: UserDefaultsKey.timeSliderEndTime) as? Date ?? Date().dateByAdding(1, .day).date
        }
        set {
            set(newValue, forKey: UserDefaultsKey.timeSliderEndTime)
        }
    }
    
}
