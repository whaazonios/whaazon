//
//  ErrorManager.swift
//  timemanager
//
//  Created by Can on 16/1/17.
//  Copyright © 2017 Waigi. All rights reserved.
//

import Foundation
import Alamofire

struct ErrorManager {
    
    static func handle(alamoFire error: Error, failure: @escaping ((NSError?) -> Void)) {
        guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
        if errorCode == 401 {
            failure(errorWithMessage("Authentication failed, please login again.", code: errorCode))
        } else {
            failure(errorWithMessage(error.localizedDescription))
        }
    }
    
    static func errorWithMessage(_ message: String) -> NSError {
        return errorWithMessage(message, code: nil)
    }
    
    static func errorWithMessage(_ message: String, code: Int?) -> NSError {
        return NSError(domain: "whaazon.com.au", code: code ?? 999, userInfo: [NSLocalizedFailureReasonErrorKey: message])
    }
    
}
