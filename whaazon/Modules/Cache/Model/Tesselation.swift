//
//  Tesselation.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 22/3/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation

class Tesselation {
    
    var spaceTiles = [RectTile]()
    var solidTiles = [RectTile]()
    
    init() {
        let initialSpace = RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: Int.max)
        spaceTiles.append(initialSpace)
    }
    
    init(spaceTiles: [RectTile], solidTiles: [RectTile]) {
        self.spaceTiles = spaceTiles
        self.solidTiles = solidTiles
    }
    
    func addTile(value: Any?, left: Int, bottom: Int, right: Int, top:Int) {
        // Iterate through all the space tiles. For those space tiles that overlap with this
        // solid tile, split the space tile by the overlap area.
        let solidTile = RectTile.createSolidTile(left: left, bottom: bottom, right: right, top: top, data: value)
        
        var tiles = [RectTile]()
        
        for spaceTile in spaceTiles {
            if let overlap = spaceTile.calcOverlap(t: solidTile) {
                let newSpaceTiles = spaceTile.splitSpaceTileAndReturnNewSpaces(overlap: overlap)
                tiles += newSpaceTiles
            } else {
                tiles.append(spaceTile)
            }
        }
        
        spaceTiles = tiles
        solidTiles.append(solidTile)
        
        spaceTiles = mergeTilesVertically(spaceTiles)
        solidTiles = mergeTilesVertically(solidTiles)
    }
    
    /**
     * Given a list of tiles, merge all the ones that share a common horizontal edge above and below each other.
     * @param list of tiles
     * @return a new list being the result of the merging
     */
    func mergeTilesVertically(_ tiles: [RectTile]) -> [RectTile] {
        // Form a list of list, where the first list is keyed by the start and end coordinates in the x direction
        // and the second list is sorted in the y direction
        var hlist = [String: [Int: RectTile]]()
        
        for tile in tiles {
            let key = "\(tile.left):\(tile.right)"
            if var map = hlist[key] {
                map[tile.bottom] = tile
                hlist[key] = map
            } else {
                hlist[key] = [tile.bottom: tile]
            }
        }
        
        // Each entry in hlist is a list of rectangular tiles that have start and end
        // on the same x-corrdinates. Now iterate through each of the lists and merge
        // the adjacent tiles vertically if both tiles overlap on a common horizontal edge.
        for (key, var map) in hlist {
            if map.count < 2 {
                continue
            }
            var lastTile: RectTile?
            var lastTileBottom: Int?
            let sortedTiles = map.values.sorted{ $0.top > $1.top }
            // sort all vertically aligned tiles in top-down order, and merge the neighbours
            for t in sortedTiles {
                if lastTile == nil {
                    lastTile = t
                    lastTileBottom = t.bottom
                    continue
                }
                if t.top == lastTile!.bottom {
                    // this tile and the last tile are adjacent and overlap on
                    // the same horizontal edge, therefore they should be merged
                    // and the current tile removed.
                    lastTile!.bottom = t.bottom
                    map[t.bottom] = nil
                    map[lastTileBottom!] = lastTile
                } else {
                    lastTile = t
                }
            }
            hlist[key] = map
        }
        
        // Merging is finished. Now iterate through the hlist to extract and return the result
        var result = [RectTile]()
        for map in hlist.values {
            result.append(contentsOf: map.values)
        }
        return result
    }
    
    /**
     * Return a list of tiles in this tesselation that overlap with the
     * given geometry defined by the given coordinates which may denote a
     * point, a line, or a rectangular area.
     *
     * @param type type of tiles to return, solid or space tiles
     * @param left
     * @param bottom
     * @param right
     * @param top
     * @return
     */
    private func findOverlapTiles(_ type: TileType, _ left: Int, _ bottom: Int, _ right: Int, _ top: Int) -> [RectTile] {
        switch type {
        case .space:
            return spaceTiles.filter { return $0.left < right && $0.right > left && $0.bottom < top && $0.top > bottom }
        case .solid:
            return solidTiles.filter { return $0.left < right && $0.right > left && $0.bottom < top && $0.top > bottom }
        }
    }
    
    /**
     * Find the minimum bounding rectangular area that can contain a given list of solid tiles
     * @param solidTiles a list of solid tiles
     * @return
     */
    private func findMinBound(_ solidTiles: [RectTile]) -> RectTile {
        var bbox = RectTile.createSpaceTile(left: Int.max, bottom: Int.max, right: Int.min, top: Int.min)
        for t in solidTiles {
            bbox.left = min(bbox.left, t.left)
            bbox.bottom = min(bbox.bottom, t.bottom)
            bbox.right = max(bbox.right, t.right)
            bbox.top = max(bbox.top, t.top)
        }
        return bbox
    }
    
    private func _findSuperBoundingRect(_ left: Int, _ bottom: Int, _ right: Int, _ top: Int) -> RectTile {
        let tlist1 = findOverlapTiles(.solid, left, bottom, right, top)
        let bb = findMinBound(tlist1)
        // check to see if this bounding box also contains other rects outside the
        // given area. If so, find a bigger bounding box to include the additional rects.
        // Repeat doing this until the final bounding area can contain all the rectangles
        let tlist2 = findOverlapTiles(.solid, bb.left, bb.bottom, bb.right, bb.top)
        if tlist1.count == tlist2.count {
            return bb
        } else {
            return findSuperBoundingRectForRegion(bb.left, bb.bottom, bb.right, bb.top)
        }
    }
    
    /**
     * Given a dot, a line, or a rectangular area represented by the four coordinates, find
     * the minimum bounding rectangular area that will enclose
     * all the rectangular solid tiles including all those overlapping with the given area.
     *
     * @param left
     * @param bottom
     * @param right
     * @param top
     * @return a space tile whose area represents the minimum bounding rect area satifying the
     * condition.
     */
    func findSuperBoundingRectForRegion(_ left: Int, _ bottom: Int, _ right: Int, _ top: Int) -> RectTile {
        let tscopy = copy() as! Tesselation
        // Add the search region as a solid tile
        tscopy.addTile(value: "search region", left: left, bottom: bottom, right: right, top: top)
        return tscopy._findSuperBoundingRect(left, bottom, right, top)
    }
    
    /**
     * Given a dot, a line, or a rectangular area represented by the four coordinates, find
     * the minimum bounding rectangular area that will enclose
     * all the rectangular solid tiles including all those overlapping with the given area.
     * This method accepts two collection objects at the end of the argument list
     * which will return the space tiles clipped by the search region, and any solid tiles
     * that are overlapped by the search region.
     *
     * @param left
     * @param bottom
     * @param right
     * @param top
     * @return a space tile whose area represents the minimum bounding rect area satifying the
     * condition.
     */
    func findSuperBoundingRectForRegion(_ left: Int, _ bottom: Int, _ right: Int, _ top: Int, _ spTiles: inout [RectTile], _ soTiles: inout [RectTile]) -> RectTile {
        let tscopy = copy() as! Tesselation
        // Add the search region as a solid tile
        tscopy.addTile(value: "search region", left: left, bottom: bottom, right: right, top: top)
        let bb = tscopy._findSuperBoundingRect(left, bottom, right, top)
        var tiles = findOverlapTiles(.space, bb.left, bb.bottom, bb.right, bb.top)
        tiles = TileUtils.clip(tiles, bb.left, bb.bottom, bb.right, bb.top)
        spTiles += tiles
        tiles = findOverlapTiles(.solid, bb.left, bb.bottom, bb.right, bb.top)
        soTiles += tiles
        return bb
    }
    
    /**
     * transform the given bounding box which is a bounding rect to a solid tile
     * with the value as given. The given bounding rect must completely contain
     * all the solid rectangular tiles within its boundary.
     *
     * @param bb the super bounding box which completely contains a set of solid rectangular
     * tiles
     * @param data the data which the new solid tile will associate with after the conversion
     */
    func convertBoundingRectToSolidTile(_ bb: RectTile, _ data: Any) throws {
        let boundedRects = findOverlapTiles(.solid, bb.left, bb.bottom, bb.right, bb.top)
    
        // First of all check that all solid tiles are within the bounding rect, otherwise
        // throw an exception.
        for t in boundedRects {
            if t.left < bb.left || t.bottom < bb.bottom || t.right > bb.right ||  t.top > bb.top {
                let message = "Convert failed: Found solid tile [\(t.left), \(t.bottom), \(t.right), \(t.top)] extended outside bound <\(bb.left), \(bb.bottom), \(bb.right), \(bb.top)>"
                throw NSError(domain: "Tesselation", code: 1001, userInfo: ["message": message])
            }
        }
        
        // Create a new empty tesselation
        let ts = Tesselation()
        
        // Now add all the solid tiles in the old tesselation into the new except those to be converted
        // to the new tile contained by the bounding rect.
        // After that simply add a new tile with the geometry of the bounding rect and associate it with
        // the given value.
        for t in solidTiles {
            if !boundedRects.contains(t) {
                ts.addTile(value: t.data, left: t.left, bottom: t.bottom, right: t.right, top: t.top)
            }
        }
        
        // Add the new solid tile with the size of the bounding rect
        ts.addTile(value: data, left: bb.left, bottom: bb.bottom, right: bb.right, top: bb.top)
        
        // Replace the current tesselation's content tiles with the one in ts
        spaceTiles = ts.spaceTiles
        solidTiles = ts.solidTiles
    }
}

extension Tesselation: Equatable, CustomStringConvertible, NSCopying {
    
    static func ==(lhs: Tesselation, rhs: Tesselation) -> Bool {
        guard lhs.spaceTiles.count == rhs.spaceTiles.count,
            lhs.solidTiles.count == rhs.solidTiles.count
            else { return false }
        for s in lhs.spaceTiles {
            if !rhs.spaceTiles.contains(s) {
                return false
            }
        }
        for s in lhs.solidTiles {
            if !rhs.solidTiles.contains(s) {
                return false
            }
        }
        return true
    }
    
    // MARK: - CustomStringConvertible
    
    var description: String {
        var result = "Solid tiles:\n"
        solidTiles.forEach { result += "\($0)\n" }
        result += "Space tiles:\n"
        spaceTiles.forEach { result += "\($0)\n"}
        return result
    }
    
    // MARK: - NSCopying
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        return copy
    }
    
}





