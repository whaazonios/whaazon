//
//  TileUtils.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 10/5/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation

struct TileUtils {
    
    static func clip(_ tilesToClip: [RectTile], _ left: Int, _ bottom: Int, _ right: Int, _ top: Int) -> [RectTile] {
        var result = [RectTile]()
        for t in tilesToClip {
            let newt = RectTile(left: max(left, t.left), bottom: max(bottom, t.bottom), right: min(right, t.right), top: min(top, t.top), data: t.data, type: t.type)
            result.append(newt)
        }
        return result
    }
    
    static func tilesToString(_ list: [RectTile]) -> String {
        var string = ""
        for (i, tile) in list.enumerated() {
            if i != 0 {
                string += ":"
            }
            string += "\(tile.bottom),\(tile.left),\(tile.top),\(tile.right)"
        }
        return string
    }
    
    static func dump(_ tiles: [RectTile]) {
        tiles.forEach{ print($0) }
    }
}
