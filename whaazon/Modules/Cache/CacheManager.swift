//
//  CacheManager.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 7/6/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation
import UIKit

class CacheManager {
    
    static let shared = {
        return CacheManager()
    }()
    
    private init() {}
    
    let tesselation = Tesselation()
    private(set) var events = [Event]()
    
    /// Clear all fetched events
    func reset() {
        tesselation.solidTiles = []
        tesselation.spaceTiles = [RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: Int.max)]
        events = []
    }
    
    /// completion is executed in main queue.
    func syncLocalWithRemote(minLatE6: Int, maxLatE6: Int, minLngE6: Int, maxLngE6: Int, beginTime: Int, endTime: Int, completion: @escaping () -> Void) {
        // Check to see if the current tile plane already contains the search region
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        let bb = tesselation.findSuperBoundingRectForRegion(minLngE6, minLatE6, maxLngE6, maxLatE6, &spaceTiles, &solidTiles)

        // Clear the cache then add all the events from the overlapped areas which have already been downloaded.
        events = [Event]()
        solidTiles.forEach {
            if let data = $0.data as? [Event] {
                events.append(contentsOf: data)
            }
        }
        
        printThread(message: "\n********** \n bottom = \(minLatE6), top = \(maxLatE6), left = \(minLngE6), right = \(maxLngE6) \n********** \n")
        // If the space tiles collection is empty, no download is necessary and the cache can be reused without clearing
        if spaceTiles.isEmpty,
            bb.left != Int.min, bb.bottom != Int.min, bb.right != Int.max, bb.top != Int.max
        {
            printThread(message: "\n********** \n no need to reload \n********** \n")
            
            // load data if needed
            // refresh mapView
            completion()
            // clear all tmp data
            events = [Event]()
            
        } else {
            printThread(message: "\n********** \n reload \n********** \n")
            
            // Generate a list of the rectangular areas to download from the space tile list
            let rectList = TileUtils.tilesToString(spaceTiles)
            // Make the service call to grab data
            fetchDataIfNeeded(area: rectList, beginTime: beginTime, endTime: endTime) { [weak self] in
                guard let strongSelf = self else { return }
                if !strongSelf.events.isEmpty {
                    try? strongSelf.tesselation.convertBoundingRectToSolidTile(bb, strongSelf.events)
                }
                completion()
                // clear all tmp data
                strongSelf.events = [Event]()
            }
        }
    }
    
    /// Fetch data of selected domain, completion is executed in main thread
    private func fetchDataIfNeeded(area: String, beginTime: Int, endTime: Int, completion: @escaping () -> Void) {
        // fetch via service calls in concurrent queues and wait at the end
        DispatchQueue.global(qos: .background).async {
            let dispatchGroup = DispatchGroup()
            Preferences.selectedDomains.forEach({ (domain) in
                dispatchGroup.enter()
                printThread(message: "*************** dispatchGroup.enter() : \(domain)")
                let searchCriteria = self.prepareSearchCriteria(domain: domain.rawValue, area: area, beginTime: beginTime, endTime: endTime)
                switch domain {
                case .RealEstate:
                    PropertyDomainAPIManager.fetchPropertyEvents(searchCriteria) { events in
                        dispatchGroup.leave()
                        printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                        DispatchQueue.main.async { [weak self] in
                            printThread(message: "*************** before appending RealEstate self?.events.count = \(self?.events.count)")
                            self?.events.append(contentsOf: events)
                            printThread(message: "*************** after appending RealEstate self?.events.count = \(self?.events.count)")
                        }
                    }
                case .CityOfOZ:
                        CityOfOZDomainClient.fetchCityOfSydneyEventsMatchingCriteria(searchCriteria) { events in
                            dispatchGroup.leave()
                            printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                            DispatchQueue.main.async { [weak self] in
                                printThread(message: "*************** before appending CityOfOZ self?.events.count = \(self?.events.count)")
                                self?.events.append(contentsOf: events)
                                printThread(message: "*************** after appending CityOfOZ self?.events.count = \(self?.events.count)")
                            }
                        }
                case .Garden, .Whaazon:
                    dispatchGroup.leave()
                    printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                    break
                case .foodAndDrinkSpecials:
                    FoodSpecialsDomainClient.fetchFoodSpecialsEventsMatchingCriteria(searchCriteria) { events in
                        dispatchGroup.leave()
                        printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                        DispatchQueue.main.async { [weak self] in
                            printThread(message: "*************** before foodAndDrinkSpecials appending self?.events.count = \(self?.events.count)")
                            self?.events.append(contentsOf: events)
                            printThread(message: "*************** after foodAndDrinkSpecials appending self?.events.count = \(self?.events.count)")
                        }
                    }
                case .coffeeChase:
                    CoffeeChaseDomainClient.fetchCoffeeChaseEventsMatchingCriteria(searchCriteria) { events in
                        dispatchGroup.leave()
                        printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                        DispatchQueue.main.async { [weak self] in
                            printThread(message: "*************** before CoffeeChaseSpecials appending self?.events.count = \(self?.events.count)")
                            self?.events.append(contentsOf: events)
                            printThread(message: "*************** after CoffeeChaseSpecials appending self?.events.count = \(self?.events.count)")
                        }
                    }
                default:
                    dispatchGroup.leave()
                    printThread(message: "*************** dispatchGroup.leave() : \(domain)")
                    break
                }
            })
            dispatchGroup.wait()
            printThread(message: "*************** dispatchGroup.wait()")
            DispatchQueue.main.async {
                printThread(message: "*************** dispatchGroup done and run completion")
                completion()
            }
        }
    }
    
    /// Generate search criteria for service call
    private func prepareSearchCriteria(domain: String, area: String, beginTime: Int, endTime: Int) -> SearchCriteria {
        var searchCriteria = SearchCriteria()
        searchCriteria.area = area
        searchCriteria.domain = domain
        searchCriteria.timeWin = "\(beginTime),\(endTime)"
        searchCriteria.category = "all"
        searchCriteria.publishBaseTime = "\(Int(Date().timeIntervalSince1970 - 365 * 24 * 3600) * 1000)"
        searchCriteria.idList = "0"
        return searchCriteria
    }
    
}
