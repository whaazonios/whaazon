//
//  RoundCornerView.swift
//  whaazon
//
//  Created by Can Zhan on 9/8/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import UIKit

/// A view with rounded corner and
@IBDesignable class RoundCornerView: UIView {

    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor = .lightGray
    @IBInspectable var cornerRadius: CGFloat = 5.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        applyBorder(borderWidth, borderColor, cornerRadius)
    }

}
