//
//  TimeScaleAndLabelGenerator.swift
//  Slider
//
//  Created by Zhan, C. (Can) on 15/12/17.
//  Copyright © 2017 Zhan, C. (Can). All rights reserved.
//

import Foundation

enum ScaleStyle {
    case none, small, medium, large
}

enum LabelStyle {
    case none, major, minor, superr
}

struct Seconds {
    static let minute: TimeInterval = 60
    static let hour: TimeInterval = 3600 // 60 * minute
    static let day: TimeInterval = 86400 // 24 * hour
    static let month: TimeInterval = 2592000 // 30 * day
    static let year: TimeInterval = 31104000 // 12 * month
}

struct ScaleLabelFormatRule {
    let calendarUnit: Calendar.Component
    let multiple: Int
    let scaleStyle: ScaleStyle
    let labelStyle: LabelStyle
}

struct TimeScaleAndLabelGenerator {
    static let DEF_SCALE_SPACING: Double = 40
    
    static let defScaleGenRule: [TimeScaleGenerationRule] = [
        // Minute Multiples
        TimeScaleGenerationRule(interval: Seconds.minute, multiple: 1, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 15, scaleStyle: .large, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 5, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: -1, scaleStyle: .small, labelStyle: .none)]),    // -1 will match all
        TimeScaleGenerationRule(interval: 2 * Seconds.minute, multiple: 2, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 10, scaleStyle: .large, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 2, scaleStyle: .medium, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 5 * Seconds.minute, multiple: 5, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 15, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 5, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 10 * Seconds.minute, multiple: 10, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 30, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 10, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 15 * Seconds.minute, multiple: 15, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 30, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 15, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 30 * Seconds.minute, multiple: 30, calendarUnit: .minute, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .minute, multiple: 0, scaleStyle: .medium, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .minute, multiple: 30, scaleStyle: .small, labelStyle: .none)]),
        
        // Hour Multiples
        TimeScaleGenerationRule(interval: Seconds.hour, multiple: 1, calendarUnit: .hour, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "HH:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .hour, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 12, scaleStyle: .large, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 3, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: -1, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 2 * Seconds.hour, multiple: 1, calendarUnit: .hour, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .hour, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 10, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 2, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 3 * Seconds.hour, multiple: 1, calendarUnit: .hour, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "H:mm", labelSuperFormat: "d MMM yy", superEncloseUnit: .day,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .hour, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 6, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 3, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 6 * Seconds.hour, multiple: 1, calendarUnit: .hour, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "H:mm", labelMajorFormat: "d MMM", labelSuperFormat: "MMM yy", superEncloseUnit: .month,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .hour, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 12, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 6, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 12 * Seconds.hour, multiple: 1, calendarUnit: .hour, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "d", labelMajorFormat: "d MMM", labelSuperFormat: "MMM yy", superEncloseUnit: .month,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .hour, multiple: 0, scaleStyle: .medium, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .hour, multiple: 12, scaleStyle: .small, labelStyle: .none)]),
        
        // Day Multiples
        TimeScaleGenerationRule(interval: Seconds.day, multiple: 1, calendarUnit: .day, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "EEE dd", labelMajorFormat: "EEE dd", labelSuperFormat: "d MMM yy", superEncloseUnit: .month,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .day, multiple: 1, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 5, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: -1, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 2 * Seconds.day, multiple: 1, calendarUnit: .day, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "EEE dd", labelMajorFormat: "EEE dd", labelSuperFormat: "d MMM yy", superEncloseUnit: .month,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .day, multiple: 1, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 10, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 2, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 5 * Seconds.day, multiple: 1, calendarUnit: .day, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "EEE dd", labelMajorFormat: "EEE dd", labelSuperFormat: "d MMM yy", superEncloseUnit: .month,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .day, multiple: 1, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 15, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 5, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 10 * Seconds.day, multiple: 1, calendarUnit: .day, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "EEE dd", labelMajorFormat: "MMM", labelSuperFormat: "d MMM yy", superEncloseUnit: .year,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .day, multiple: 1, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 10, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 15 * Seconds.day, multiple: 1, calendarUnit: .day, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "EEE dd", labelMajorFormat: "MMM", labelSuperFormat: "MMM yy", superEncloseUnit: .year,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .day, multiple: 1, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .day, multiple: 15, scaleStyle: .small, labelStyle: .none)]),
        
        // Month Multiples
        TimeScaleGenerationRule(interval: Seconds.month, multiple: 1, calendarUnit: .month, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "MMM", labelMajorFormat: "MMM", labelSuperFormat: "d MMM yy", superEncloseUnit: .year,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .month, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 3, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 6, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 9, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 12, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: -1, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 3 * Seconds.month, multiple: 3, calendarUnit: .month, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "MMM", labelMajorFormat: "MMM", labelSuperFormat: "yyyy", superEncloseUnit: .year,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .month, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 6, scaleStyle: .medium, labelStyle: .minor),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: -1, scaleStyle: .small, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 6 * Seconds.month, multiple: 6, calendarUnit: .month, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "yyyy", labelMajorFormat: "yyyy", labelSuperFormat: "yyyy", superEncloseUnit: .year,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .month, multiple: 0, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .month, multiple: 6, scaleStyle: .medium, labelStyle: .none)]),
        
        // Year
        TimeScaleGenerationRule(interval: Seconds.year, multiple: 1, calendarUnit: .year, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "yyyy", labelMajorFormat: "yyyy", labelSuperFormat: "yyyy", superEncloseUnit: .second,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .year, multiple: 5, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .year, multiple: 1, scaleStyle: .medium, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 5 * Seconds.year, multiple: 1, calendarUnit: .year, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "yyyy", labelMajorFormat: "yyyy", labelSuperFormat: "yyyy", superEncloseUnit: .second,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .year, multiple: 20, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .year, multiple: 5, scaleStyle: .medium, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 10 * Seconds.year, multiple: 1, calendarUnit: .year, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "yyyy", labelMajorFormat: "yyyy", labelSuperFormat: "yyyy", superEncloseUnit: .second,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .year, multiple: 50, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .year, multiple: 10, scaleStyle: .medium, labelStyle: .none)]),
        TimeScaleGenerationRule(interval: 20 * Seconds.year, multiple: 20, calendarUnit: .year, minWidth: DEF_SCALE_SPACING, labelMinorFormat: "yyyy", labelMajorFormat: "yyyy", labelSuperFormat: "yyyy", superEncloseUnit: .second,
                                formatRule: [ScaleLabelFormatRule(calendarUnit: .year, multiple: 100, scaleStyle: .large, labelStyle: .major),
                                             ScaleLabelFormatRule(calendarUnit: .year, multiple: 20, scaleStyle: .medium, labelStyle: .none)])
    ]
    
    /// Generate a list of time scales and labels associated with the time scale for a given time range described in milliseconds. This method is called to display the time scale in the time range slider.
    /// Generate all scales on the slider view
    static func generateTimeScale(beginX: Double, endX: Double, actualBeginTimeInterval: TimeInterval, actualEndTimeInterval: TimeInterval, timeZone: TimeZone) -> [TimeScale] {
        var scales = [TimeScale]()
        
        // Obtain the time interval for the minimum spacing
        let width = endX - beginX
        let intervalPerSegment = DEF_SCALE_SPACING * (actualEndTimeInterval - actualBeginTimeInterval) / width
        
        // Find the rule where for this interval size. The size closest one
        var rulee: TimeScaleGenerationRule?
        for r in defScaleGenRule {
            rulee = r
            if intervalPerSegment < r.interval { break }
        }
        // If the interval is too big and cannot find any matches, then just return null
        guard let rule = rulee else { return scales }
        
        // Otherwise, find the beginning time of the enclosing interval.
        // An enclosing interval for minute is hour, and enclosing interval for an hour is day ...etc.
        let cal = Calendar.current
        let actualBeginTime = Date(timeIntervalSince1970: actualBeginTimeInterval)
        let year = cal.component(.year, from: actualBeginTime)
        let month = cal.component(.month, from: actualBeginTime)
        let day = cal.component(.day, from: actualBeginTime)
        let hour = cal.component(.hour, from: actualBeginTime)
        
        // Now adjust the calendar to the beginning of the enclosing time interval, then iterate through each eclosing interval
        if rule.interval < Seconds.hour {
            // The rounded begin time to closest hour, normally ahead of the actual begin time. e.g. beginMillis: 2017-12-22 9:30:30 => roundedBeginTime: 2017-12-22 9:00:00
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: year, month: month, day: day, hour: hour))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.hour
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                   beginX: beginX,
                                                   endX: endX,
                                                   overallRangeStart: actualBeginTimeInterval,
                                                   overallRangeEnd: actualEndTimeInterval,
                                                   segBegin: beginTimeInterval,
                                                   segEnd: endTimeInterval,
                                                   timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        } else if rule.interval < 12 * Seconds.hour {
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: year, month: month, day: day))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.day
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                  beginX: beginX,
                                                  endX: endX,
                                                  overallRangeStart: actualBeginTimeInterval,
                                                  overallRangeEnd: actualEndTimeInterval,
                                                  segBegin: beginTimeInterval,
                                                  segEnd: endTimeInterval,
                                                  timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        } else if rule.interval == 12 * Seconds.hour {
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: year, month: 0, day: 1))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.month
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                  beginX: beginX,
                                                  endX: endX,
                                                  overallRangeStart: actualBeginTimeInterval,
                                                  overallRangeEnd: actualEndTimeInterval,
                                                  segBegin: beginTimeInterval,
                                                  segEnd: endTimeInterval,
                                                  timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        } else if rule.interval < Seconds.month {
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: year, month: month, day: 1))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.month
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                  beginX: beginX,
                                                  endX: endX,
                                                  overallRangeStart: actualBeginTimeInterval,
                                                  overallRangeEnd: actualEndTimeInterval,
                                                  segBegin: beginTimeInterval,
                                                  segEnd: endTimeInterval,
                                                  timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        } else if rule.interval < Seconds.year {
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: year, month: 0, day: 1))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.year
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                  beginX: beginX,
                                                  endX: endX,
                                                  overallRangeStart: actualBeginTimeInterval,
                                                  overallRangeEnd: actualEndTimeInterval,
                                                  segBegin: beginTimeInterval,
                                                  segEnd: endTimeInterval,
                                                  timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        } else if rule.interval < Seconds.year {
            let roundedBeginTimeInterval = cal.date(from: DateComponents(year: (year / 10) * 10, month: 0, day: 1))?.timeIntervalSince1970 ?? 0
            var beginTimeInterval = roundedBeginTimeInterval
            while beginTimeInterval < actualEndTimeInterval {
                let endTimeInterval = beginTimeInterval + Seconds.year
                let list = rule.generateTimeScale(with: [TimeScale](),
                                                  beginX: beginX,
                                                  endX: endX,
                                                  overallRangeStart: actualBeginTimeInterval,
                                                  overallRangeEnd: actualEndTimeInterval,
                                                  segBegin: beginTimeInterval,
                                                  segEnd: endTimeInterval,
                                                  timeZone: timeZone)
                scales.append(contentsOf: list)
                beginTimeInterval = endTimeInterval
            }
        }
        
        return scales
    }
    
}

/// Scale marking on the time line
struct TimeScale {
    let position: Double
    let scaleStyle: ScaleStyle
    let labelStyle: LabelStyle
    let label: String
}

struct TimeScaleGenerationRule {
    let interval: TimeInterval       // Approximate interval in seconds representing the size of the scale interval
    let multiple: Int       // The size of each scale interval = multiple x calendarUnit, this is the actual
    let calendarUnit: Calendar.Component    // interval according to the calendar after correction such as lunar year and daylight saving
    let minWidth: Double       // Minimum geometrical width between time scales in screen coordinate
    let labelMinorFormat: String    // Format of date component to use for minor label
    let labelMajorFormat: String    // Format of date component to use for major label
    let labelSuperFormat: String    // Format of date component to use if the time falls on the next higher time boundary
    let superEncloseUnit: Calendar.Component       // The interval boundary to distinguish the use of major or super label
    let formatRule: [ScaleLabelFormatRule]  // Label and Scale format depending on the unit and multiple

    /// Generate scales within one segment (between two scales)
    func generateTimeScale(with list:[TimeScale],
                           beginX: Double,
                           endX: Double,
                           overallRangeStart: TimeInterval,
                           overallRangeEnd: TimeInterval,
                           segBegin: TimeInterval,
                           segEnd: TimeInterval,
                           timeZone: TimeZone
                           ) -> [TimeScale] {
        var scales = [TimeScale]()
        
        var current = TimeInterval(segBegin)    // rounded start time interval
        var currentDate = Date(timeIntervalSince1970: current)  // rounded start time
        let cal = Calendar.current
        let width = endX - beginX
        
        while current < segEnd {
            let position = (current - overallRangeStart) * (width / (overallRangeEnd - overallRangeStart))
            let year = cal.component(.year, from: currentDate)
            let month = cal.component(.month, from: currentDate)
            let day = cal.component(.day, from: currentDate)
            let hour = cal.component(.hour, from: currentDate)
            let min = cal.component(.minute, from: currentDate)
            
            switch calendarUnit {
            case .minute:
                for r in formatRule {
                    if ((min == 0 && r.multiple == 0) || (r.multiple != 0 && min % r.multiple == 0) || r.multiple == -1) {
                        if let ts = createTimeScale(current: current, position: position, r: r, timeZone: timeZone) {
                            scales.append(ts)
                        }
                        break
                    }
                }
                
            case .hour:
                for r in formatRule {
                    if min == 0 &&
                        ((hour == 0 && r.multiple == 0) || (r.multiple != 0 && hour % r.multiple == 0) || r.multiple == -1) {
                        if let ts = createTimeScale(current: current, position: position, r: r, timeZone: timeZone) {
                            scales.append(ts)
                        }
                        break
                    }
                }
                
            case .day:
                for r in formatRule {
                    if min == 0 && hour == 0 &&
                        ((day == 1 && r.multiple == 1) || (r.multiple != 1 && day % r.multiple == 0) || r.multiple == -1) {
                        if let ts = createTimeScale(current: current, position: position, r: r, timeZone: timeZone) {
                            scales.append(ts)
                        }
                        break
                    }
                }
                
            case .month:
                for r in formatRule {
                    if min == 0 && hour == 0 && day == 1 &&
                        ((month == 0 && r.multiple == 0) || (r.multiple != 0 && month % r.multiple == 0) || r.multiple == -1) {
                        if let ts = createTimeScale(current: current, position: position, r: r, timeZone: timeZone) {
                            scales.append(ts)
                        }
                        break
                    }
                }
                
            case .year:
                for r in formatRule {
                    if min == 0 && hour == 0 && day == 1 && month == 0 &&
                        ((year == 0 && r.multiple == 0) || (r.multiple != 0 && year % r.multiple == 0) || r.multiple == -1) {
                        if let ts = createTimeScale(current: current, position: position, r: r, timeZone: timeZone) {
                            scales.append(ts)
                        }
                        break
                    }
                }
                
            default: break
            }
            
            currentDate = Date(timeIntervalSince1970: current)
            current = cal.date(byAdding: calendarUnit, value: multiple, to: currentDate)?.timeIntervalSince1970 ?? 0
        }
        
        return scales
    }
    
    private func createTimeScale(current: TimeInterval, position: Double, r: ScaleLabelFormatRule, timeZone: TimeZone) -> TimeScale? {
        var ts: TimeScale?
        var labelStyle = r.labelStyle
        let scaleStyle = r.scaleStyle
        let sdf = DateFormatter()
        
        if labelStyle == .minor {
            sdf.dateFormat = labelMinorFormat
        } else if labelStyle == .major {
            if isOnBoundaryOfEnclosingInterval(current: current, timeZone: timeZone) {
                // If this major label is also on enclosing interval bounday
                // change it to become a super label
                sdf.dateFormat = labelSuperFormat
                labelStyle = .superr
            } else {
                sdf.dateFormat = labelMajorFormat
            }
        }
        let label = sdf.string(from: Date(timeIntervalSince1970: current))
        ts = TimeScale(position: position, scaleStyle: scaleStyle, labelStyle: labelStyle, label: label)
//        print("******* createTimeScale: current = \(Date(timeIntervalSince1970: current)), position = \(position), label = \(label)")
        return ts
    }
    
    private func isOnBoundaryOfEnclosingInterval(current: TimeInterval, timeZone: TimeZone) -> Bool {
        let date = Date(timeIntervalSince1970: current)
        let cal = Calendar.current
        let month = cal.component(.month, from: date)
        let day = cal.component(.day, from: date)
        let hour = cal.component(.hour, from: date)
        let minute = cal.component(.minute, from: date)
        
        switch superEncloseUnit {
        case .hour:
            if minute == 0 { return true }
        case .day:
            if hour == 0 && minute == 0 { return true }
        case .month:
            if day == 1 && hour == 0 && minute == 0 { return true }
        case .year:
            if month == 0 && day == 1 && hour == 0 && minute == 0 { return true }
        default: break
        }
        return false
    }
}


















































