//
//  AnnotationMarkerManager.swift
//  whaazon
//
//  Created by Can on 1/04/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DomainAnnotationMarker {
    
    typealias AnnotationMarker = (name: String, icon: String)
    
    let domain: String
    var markers = [AnnotationMarker]()
    
    init(json: JSON) {
        domain = json["domain"].stringValue
        for markerJson in json["markers"].arrayValue {
            markers.append(AnnotationMarker(name: markerJson["name"].stringValue, icon: markerJson["icon"].stringValue))
        }
    }
    
}

class AnnotationMarkerManager {
    
    static var annotationMarkers: [DomainAnnotationMarker]?
    /*
    static func annotationOfEvent(event: Event) -> UIImage {
        let defaultImage = event.domainType.values.icon
        guard let domainMarkers = AnnotationMarkerManager.currentDomainAnnotationMarkers() else {return defaultImage}
        
        for domainMarker in domainMarkers {
            for marker in domainMarker.markers {
                if event.category.lowercaseString.containsString(marker.name.lowercaseString) && FilterManager.isFilterOptionSelected(marker.name) {
                    return UIImage(named: marker.icon) ?? defaultImage
                }
            }
        }
        return defaultImage
    }
 */
    
    /**
     Get current selected Domain's markers. If selected domain is All, merge all filters together
     
     - returns: current selected domain's markers or nil if nothing match
     */
    static func currentDomainAnnotationMarkers() -> [DomainAnnotationMarker]? {
        if annotationMarkers == nil {
            annotationMarkers = AnnotationMarkerManager.fetchAnnotationMarkers()
        } else {
            var markers = [DomainAnnotationMarker]()
            if Preferences.isSelected(domain: .All) {
                markers = annotationMarkers!
            } else {
                for annotationMarker in annotationMarkers! {
                    if let domain = Domain(rawValue: annotationMarker.domain),
                        Preferences.isSelected(domain: domain)
                    {
                        markers.append(annotationMarker)
                        break
                    }
                }
            }
            
            return markers
        }
        return nil
    }
    
    /**
     Load all domains' markers
     
     - returns: AnnotaionMarkers
     */
    fileprivate static func fetchAnnotationMarkers() -> [DomainAnnotationMarker]? {
        let filePath = Bundle.main.path(forResource: "AnnotationMarker",ofType:"json")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath!), options: NSData.ReadingOptions.uncached)
            let json = try JSON(data: data)
            var domainAnnotationMarkers = [DomainAnnotationMarker]()
            for (_, subJson: domainJson) in json {
                let domainAnnotationMarker = DomainAnnotationMarker(json: domainJson)
                print(domainAnnotationMarker)
                domainAnnotationMarkers.append(domainAnnotationMarker)
            }
            return domainAnnotationMarkers
        } catch {
            print("Failed to read AnnotationMarker")
        }
        return nil
    }
    
}

