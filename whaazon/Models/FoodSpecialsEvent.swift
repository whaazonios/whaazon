//
//  FoodSpecialsEvent.swift
//  whaazon
//
//  Created by Zhan, C. (Can) on 16/4/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class FoodSpecialsEvent: Event {
    
    //TODO: implement a generic method in Event class
    override var icon: UIImage {
        var image: UIImage?
        if let attribute = FilterManager.filterAttributes(of: .foodAndDrinkSpecials) {
            if let options = attribute.selectedAttributeOptions?.values {
                for option in options.reversed() {
                    //TODO: icon urls are string of local file name at the moment, use online urls later
                    let iconName = option.iconSelected.absoluteString
                    
                    switch attribute.attributeName {
                    case "Type":
                        if tags.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    default:
                        break
                    }
                    if image != nil {
                        break
                    }
                }
            }
        }
        return image ?? domainType.values.icon
    }
    
    override init(json: JSON) {
        super.init(json: json)
    }
    
    override func evaluateIn(_ attributeName: String, value: Any, caseSensitive: Bool) -> Bool {
        if attributeName == "Type" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return tags.contains(stringValue)
                } else {
                    return tags.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        return false
    }
    
}
