//
//  PropertyEvent.swift
//  whaazon
//
//  Created by Can on 25/11/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class PropertyEvent: Event {
    let propertyId: Int
    let priceMin: Double
    let priceMax: Double
    let propertyPublisher: String
    let propertyMarketType: String
    let propertyType: String
    
    //TODO: implement a generic method in Event class
    override var icon: UIImage {
        var image: UIImage?
        if let attribute = FilterManager.filterAttributes(of: .RealEstate) {
            if let options = attribute.selectedAttributeOptions?.values {
                for option in options.reversed() {
                    //TODO: icon urls are string of local file name at the moment, use online urls later
                    let iconName = option.iconPointer.absoluteString
                    
                    switch attribute.attributeName {
                    case "Market Type":
                        if option.value == propertyMarketType {
                            image = UIImage(named: iconName)
                        }
                    case "Property Type":
                        if propertyType.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    case "Publisher":
                        if propertyPublisher.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    default:
                        break
                    }
                    if image != nil {
                        break
                    }
                }
            }
        }
        return image ?? domainType.values.icon
    }
    
    override init(json: JSON) {
        propertyId = json["propertyId"].intValue
        priceMin = json["priceMin"].doubleValue
        priceMax = json["priceMax"].doubleValue
        propertyPublisher = json["publisher"].stringValue
        propertyMarketType = json["marketType"].stringValue
        propertyType = json["propertyType"].stringValue

        super.init(json: json)
    }
    
    override func evaluateEqual(_ attributeName: String, value: Any) -> Bool {
        if attributeName == "Market Type" {
            if let stringValue = value as? String {
                return stringValue == self.propertyMarketType
            }
        }
        if attributeName == "Property Type" {
            if let stringValue = value as? String {
                return stringValue == self.propertyType
            }
        }
        if attributeName == "Publisher" {
            if let stringValue = value as? String {
                return stringValue == self.propertyPublisher
            }
        }
        return false
    }
    
    override func evaluateIn(_ attributeName: String, value: Any, caseSensitive: Bool) -> Bool {
        if attributeName == "Market Type" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return self.propertyMarketType.contains(stringValue)
                } else {
                    return self.propertyMarketType.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        if attributeName == "Property Type" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return self.propertyType.contains(stringValue)
                } else {
                    return self.propertyType.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        if attributeName == "Publisher" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return self.propertyPublisher.contains(stringValue)
                } else {
                    return self.propertyPublisher.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        return false
    }
    
    override func evaluateRange(_ attributeName: String, lowerValue: Any?, upperValue: Any?) -> Bool? {
        if attributeName == "Rent Price" && propertyMarketType.lowercased() == "rent" {
            if let lowerValueDouble = lowerValue as? Double,
                let upperValueDouble = upperValue as? Double
            {
                return priceMin >= lowerValueDouble && priceMax <= upperValueDouble
            }
        }
        if attributeName == "Selling Price" && propertyMarketType.lowercased() == "buy" {
            if let lowerValueDouble = lowerValue as? Double,
                let upperValueDouble = upperValue as? Double
            {
                return priceMin >= lowerValueDouble && priceMax <= upperValueDouble
            }
        }
        return nil
    }
    
}
