//
//  Event.swift
//  whaazon
//
//  Created by Can on 19/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON
import SwiftDate
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class Event: NSObject, MKAnnotation {
    
    enum PayType: String {
        case Free = "FREE", Paid = "PAID", Unknown = "UNKNOWN"
    }
    
    // MKAnnotation properties
    var title: String?  // use eventTitle instead
    var subtitle: String?
    let coordinate: CLLocationCoordinate2D
    // Other properties
    let eventTitle: String?
    let id: Int
    let pid: String
    let summary: String?
    /// The event's original web page URL e.g. the city of Sydney URL
    let eventURL: URL?
    let imageURL: URL?
    let imageAlt: String
    let place: String
    let latitudeE6: Double
    let longitudeE6: Double
    let level: String
    let category: String
    let tags: String
    let publisher: String
    let domain: String
    let publishTime: String
    let suburb: String
    let state: String
    let country: String
    let postcode: Int
    var occurences: [Occurence]
//    var savedEvent = false  // indicate if this event is persisted as favouriate
    /// The event's Whaazon generated web page URL
    var detailPageURLString: String?
    var expired = false
    // payment
    let costInfo: String?
    let payType: PayType?
    let minAmountDollar: Int?
    let minAmountCent: Int?
    let maxAmountDollar: Int?
    let maxAmountCent: Int?
    
    static var startDateFormatter: DateFormatter? = nil, endDateFormatter: DateFormatter? = nil, dayMonthFormatter: DateFormatter? = nil

    /// Override this var to allow Set operation
    override var hash: Int {
        return pid.hashValue
    }
    
    /// Most recent occurence of this Event. assigned when do fine grain filtering of events
    var mostRecentOccurence: (startTime: Date?, endTime: Date?, timeRange: (lowerRange: Date, upperRange: Date)?) {
        didSet {
            subtitle = occurenceDateString
        }
    }
    
    /// The first occurence of current event in timer slider's date range
    var firstOccurence: DateRange? { didSet { subtitle = occurenceDateString } }

    var occurenceDateString: String {
        var formattedString = ""
        guard let firstOccurence = firstOccurence else { return formattedString }
        formattedString = "\(DateFormatter.eventSubTitleDateFormatter1.string(from: firstOccurence.startDate)) - \(DateFormatter.eventSubTitleDateFormatter2.string(from: firstOccurence.endDate))"
//        if let occurence = occurences.first, occurence.overallPeriodOnly {
//            let startTime = occurence.startDate
//            let endTime = occurence.endDate
//                if Event.dayMonthFormatter == nil {
//                    Event.dayMonthFormatter = DateFormatter()
//                    Event.dayMonthFormatter!.dateFormat = DAY_MONTH_DATE_FORMAT
//                }
//                formattedString = "⚠️ \(Event.dayMonthFormatter!.string(from: startTime)) - \(Event.dayMonthFormatter!.string(from: endTime))"
//
//        } else {
//            if let startTime = mostRecentOccurence.startTime, let endTime = mostRecentOccurence.endTime {
//                if Event.startDateFormatter == nil {
//                    Event.startDateFormatter = DateFormatter()
//                    Event.startDateFormatter!.dateFormat = DISPLAY_FORMAT
//                }
//                if Event.endDateFormatter == nil {
//                    Event.endDateFormatter = DateFormatter()
//                    Event.endDateFormatter!.dateFormat = DISPLAY_FORMAT2
//                }
//                formattedString = "\(Event.startDateFormatter!.string(from: startTime)) - \(Event.endDateFormatter!.string(from: endTime))"
//            }
//        }
        return formattedString
    }
    
    // if no user specified start/end date to calculate most recent occurence, use the start/end time of the first occurence
    var defaultOccurenceDateString: String {
        if let startTime = occurences.first?.startDate, let endTime = occurences.first?.endDate {
            if Event.startDateFormatter == nil {
                Event.startDateFormatter = DateFormatter()
                Event.startDateFormatter!.dateFormat = DISPLAY_FORMAT
            }
            if Event.endDateFormatter == nil {
                Event.endDateFormatter = DateFormatter()
                Event.endDateFormatter!.dateFormat = DISPLAY_FORMAT2
            }
            return "\(Event.startDateFormatter!.string(from: startTime)) - \(Event.endDateFormatter!.string(from: endTime))"
        }
        return ""
    }
    
    var domainType: Domain {
        return Domain(rawValue: domain)!
    }
    
    var detailPageURL: URL? {
        if let detailPageURLString = detailPageURLString?.replacingOccurrences(of: "{host}", with: serverDomain) {
            return URL(string: detailPageURLString.replacingOccurrences(of: " ", with: "%20"))
        }
        return eventURL
    }
    
    var icon: UIImage {
        return domainType.values.icon
    }
    
    var isOverallPeriodOnly: Bool {
        return occurences.first?.overallPeriodOnly ?? false
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        pid = json["pid"].stringValue
        eventURL = URL(string: json["href"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        detailPageURLString = json["detailedPageUrl"].string
        eventTitle = json["eventHeader"].stringValue
        summary = json["eventSummary"].stringValue
        imageURL = URL(string: json["imgSrc"].stringValue)
        imageAlt = json["imgAlt"].stringValue
        latitudeE6 = json["latitudeE6"].doubleValue
        longitudeE6 = json["longitudeE6"].doubleValue
        place = json["place"].stringValue
        level = json["level"].stringValue
        let categoryString = json["category"].stringValue
        if categoryString.isEmpty {
            // set to "other" as default value for events with no categories
            category = "other"
        } else {
            category = categoryString
        }
        tags = json["tags"].stringValue
        publisher = json["publisher"].stringValue
        domain = json["domain"].stringValue
        publishTime = json["publishTime"].stringValue
        suburb = json["suburb"].stringValue
        state = json["state"].stringValue
        country = json["country"].stringValue
        postcode = json["postcode"].intValue
        occurences = [Occurence]()
        for (_, subJson) in json["occurrenceDTOs"] {
            occurences.append(Occurence(json: subJson))
        }
        costInfo = json["costInfo"].string
        payType = PayType(rawValue: json["payType"].stringValue)
        minAmountDollar = json["minAmountDollar"].int
        minAmountCent = json["minAmountCent"].int
        maxAmountDollar = json["maxAmountDollar"].int
        maxAmountCent = json["maxAmountCent"].int
        coordinate = CLLocationCoordinate2D(latitude: latitudeE6 / 1000000, longitude: longitudeE6 / 1000000)
        super.init()
        
        subtitle = defaultOccurenceDateString
    }
    
    override init() {
        id = 0
        pid = ""
        eventURL = nil
        title = nil
        eventTitle = nil
        summary = nil
        imageURL = nil
        imageAlt = ""
        latitudeE6 = 0
        longitudeE6 = 0
        place = ""
        level = ""
        category = ""
        tags = ""
        publisher = ""
        domain = ""
        publishTime = ""
        suburb = ""
        state = ""
        country = ""
        postcode = 0
        occurences = [Occurence]()
        coordinate = CLLocationCoordinate2D.init()
        costInfo = nil
        payType = nil
        minAmountDollar = nil
        minAmountCent = nil
        maxAmountDollar = nil
        maxAmountCent = nil
        
        super.init()
    }
    
    // this initializer is used for restore Event from CoreData
    init(id: Int, pid: String, title: String, subtitle: String, summary: String, latitude: Double, longitude: Double, eventURLString: String, imageURLString: String, detailPageURLString: String, domain: String, costInfo: String?, payType: String?) {
        // values from CoreData
        self.id = id
        self.pid = pid
        self.eventURL = URL(string: eventURLString)
        self.eventTitle = title
        self.subtitle = subtitle
        self.summary = summary
        imageURL = URL(string: imageURLString)
        coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.detailPageURLString = detailPageURLString
        self.domain = domain
        self.costInfo = costInfo
        self.payType = (payType != nil) ? PayType(rawValue: payType!) : nil
        //TODO: below fields are not persisted yet, set to default values
        imageAlt = ""
        latitudeE6 = 0
        longitudeE6 = 0
        place = ""
        level = ""
        category = ""
        tags = ""
        publisher = ""
        publishTime = ""
        suburb = ""
        state = ""
        country = ""
        postcode = 0
        occurences = [Occurence]()
        minAmountDollar = nil
        minAmountCent = nil
        maxAmountDollar = nil
        maxAmountCent = nil
        // only retrieved Event has true value
//        savedEvent = true
        super.init()
    }
    
    /**
     Compare if given value matches property value in the instance. e.g. check if 'art' == 'art'
     
     - parameter attributeName: Attribute name on Filter panel
     - parameter value:        value to be compared
     - parameter evaluateType: compare type
     
     - returns: true if given value is satisfied with given property
     */
    func evaluateEqual(_ attributeName: String, value: Any) -> Bool {
        return false
    }
    
    /**
     Check if value of 'attributeName' is in the range of lowerValue and upperValue
    
     - parameter attributeName: attributeName description
     - parameter lowerValue:    lower bound value to compare
     - parameter upperValue:    upper bound value to compare
     
     - returns: true if given value is satisfied with given property, nil if the evaluation is not valid.
                e.g realestate domain didn't select Rent, so, when compare rent price with selling price, 
                    return nil instead of false to indicate that evaluation is not valid instead of failed.
     */
    func evaluateRange(_ attributeName: String, lowerValue: Any?, upperValue: Any?) -> Bool? {
        return nil
    }
    
    /**
     e.g. check if 'art' in 'art,education,movie' category
     */
    func evaluateIn(_ attributeName: String, value: Any, caseSensitive: Bool) -> Bool {
        return false
    }
    
    // for array.contains
    override func isEqual(_ object: Any?) -> Bool {
        if let comparingEvent = object as? Event {
//            guard let leftURLString = self.eventURL?.absoluteString, rightURLString = rhs.eventURL?.absoluteString else { return false }
//            return leftURLString == rightURLString
            return pid == comparingEvent.pid
        }
        return false
    }
    
    func mostRecentOccurenceFromNow() -> Date? {
        let dateRange = DateRange(startDate: Date(), endDate: 1.years.from(Date())!)
        let occurence = occurences.compactMap{ Occurence.firstOccurenceIn(dateRange, with: $0) }.sorted().first
        expired = occurence == nil
        return occurence?.startDate
    }
    
    private var lastSliderDateRange: DateRange?
    
    /// Calculate and set first valid occurence of the current Event with given time slider date range
    func calculateMostRecentValidOccurence(in sliderDateRange: DateRange) {
        // only need to recalculate when slider date range updated
        guard lastSliderDateRange != sliderDateRange else {
            printThread(message: "~~~~~~ Occurence calculation Ignored with sliderDateRange = \(sliderDateRange)")
            return
        }
        lastSliderDateRange = sliderDateRange
        printThread(message: "~~~~~~ Occurence calculation Procceeded with sliderDateRange = \(sliderDateRange)")
        
        firstOccurence = occurences.compactMap{ Occurence.firstOccurenceIn(sliderDateRange, with: $0) }.sorted().first
    }
    
}

func == (left: Event, right: Event) -> Bool {
    return left.pid == right.pid
}


enum DayOfWeek: Int {
    case sunday = 0, monday, tuesday, wednesday, thursday, friday, saturday
}

enum MonthInYear: Int {
    case january = 1, february, march, april, may, june, july, august, september, october, november, december
}

enum OccurType: String {
    case SINGLE, REPEAT_HOURLY, REPEAT_DAILY, REPEAT_WEEKLY, REPEAT_BIWEEKLY, REPEAT_BY_WEEK_MONTHLY, REPEAT_MONTHLY, REPEAT_YEARLY, UNKNOWN
    
    var descrpition: String {
        switch self {
        case .SINGLE: return "No Repeat"
        case .REPEAT_HOURLY: return "Repeat Hourly"
        case .REPEAT_DAILY: return "Repeat Daily"
        case .REPEAT_WEEKLY: return "Repeat Weekly"
        case .REPEAT_BIWEEKLY: return "Repeat Fortnightly"
        case .REPEAT_BY_WEEK_MONTHLY: return "Repeat by Week Monthly"
        case .REPEAT_MONTHLY: return "Repeat Monthly"
        case .REPEAT_YEARLY: return "Repeat Yearly"
        case .UNKNOWN: return "Unknow"
        }
    }
}

enum WeekInMonth: Int {
    case first_WEEK = 1, second_WEEK, third_WEEK, fourth_WEEK, fifth_WEEK, last_WEEK
}


struct Occurence {
    
    let id: Int
    var startDateTimeInterval: Double   // Local time
    var endDateTimeInterval: Double     // Local time
    var occurType: OccurType?
    var monthsInYear: [MonthInYear]
    var weeksInMonth: [WeekInMonth] // 1, 2, 3, 4, 5
    var daysInWeek: [DayOfWeek]     // (0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday)
    ///  "15:0:0-17:0:0"
    var intervalsInDay: [String]
    /// Events specifying only the overall period without the fine details of its occurrences. e.g. 3 September to 30 September
    let overallPeriodOnly: Bool
    
    let startDate: Date  // UTC time
    let endDate: Date    // UTC time
    let durationInSeconds: Int  // duration of one occurence
    
    /// In "2,3,4,5,6" format
    var daysInWeekString: String { return daysInWeek.map{"\($0)"}.joined(separator: ",") }
    var weeksInMonthString: String { return weeksInMonth.map{"\($0)"}.joined(separator: ",") }
    var monthsInYearString: String { return monthsInYear.map{"\($0)"}.joined(separator: ",") }
    
    /// Produce a default empty Occurence
    init() {
        id = 0
        startDateTimeInterval = Date().timeIntervalSince1970
        endDateTimeInterval = startDateTimeInterval
        occurType = .SINGLE
        monthsInYear = []
        weeksInMonth = []
        daysInWeek = []
        intervalsInDay = []
        overallPeriodOnly = false
        startDate = Date()
        endDate = startDate
        durationInSeconds = 0
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        
        startDateTimeInterval = json["startDate"].doubleValue
        endDateTimeInterval = json["endDate"].doubleValue
        occurType = OccurType(rawValue: json["occurType"].stringValue)
        monthsInYear = json["monthsInYear"].stringValue.components(separatedBy: ",").compactMap({Int($0)}).compactMap({MonthInYear(rawValue: $0)})
        weeksInMonth = json["weeksInMonth"].stringValue.components(separatedBy: ",").compactMap({Int($0)}).compactMap({WeekInMonth(rawValue: $0)})
        daysInWeek = json["daysInWeek"].stringValue.components(separatedBy: ",").compactMap({Int($0)}).compactMap({DayOfWeek(rawValue: $0)})
        intervalsInDay = json["intervalsInDay"].stringValue.components(separatedBy: ",").compactMap({$0})
        overallPeriodOnly = json["overallPeriodOnly"].boolValue
        
        // deduct time zone offset from receiving local time
//        let secondsFromGMT = -Double(Preferences.getMapTimezone()?.secondsFromGMT ?? 0)
        startDate = Date(timeIntervalSince1970: startDateTimeInterval / 1000)
        endDate = Date(timeIntervalSince1970: endDateTimeInterval / 1000)
//        startDate = NSDate(timeIntervalSince1970: startDateTimeInterval / 1000).dateByAddingTimeInterval(secondsFromGMT)
//        endDate = NSDate(timeIntervalSince1970: endDateTimeInterval / 1000).dateByAddingTimeInterval(secondsFromGMT)
    
        func secondsOfIntervalsInDay(_ intervalsInDay: String) -> Int {
            let times = intervalsInDay.components(separatedBy: "-")
            let startTimes = times[0].components(separatedBy: ":")
            let endTimes = times[1].components(separatedBy: ":")
            var hours: Int = 0
            if Int(endTimes[0])! >= Int(startTimes[0])! {
                hours = Int(endTimes[0])! - Int(startTimes[0])!
            } else {
                hours = 24 - Int(startTimes[0])! + Int(endTimes[0])!
            }
            let hoursInSeconds = hours * 3600
            let minutesInSeconds = (Int(endTimes[1])! - Int(startTimes[1])!) * 60
            let seconds = Int(endTimes[2])! - Int(startTimes[2])!
            return hoursInSeconds + minutesInSeconds + seconds
        }
        
        durationInSeconds = secondsOfIntervalsInDay(intervalsInDay.count > 0 ? intervalsInDay[0] : "0")
    }
    
    /**
     Calculate the next occurence of this event in specified range. Both inputs and outputs are LocalTime instead of UTC time
     
     - parameter lowerRangeLocalTime: lower bound of time range in local time
     - parameter upperRangeLocalTime: upper bound of time range in local time
     
     - returns: the occurence in local time or nil if not happening in the range
     */
    func mostRecentOccurenceInRange(_ lowerRange: Date, upperRange: Date) -> Date? {
        if endDate < lowerRange || startDate > upperRange {
            // If the start time and end time of an occurence do not overlap the time range on slider, it means this event is not going to be shown at all, so skip it to save huge amount of time of finding most recent occurence.
            return nil
        }
        
        var occurence: Date?
        
        guard occurType != nil else {
            return occurence
        }
        
        if occurType == .SINGLE {
            occurence = possibleOccurenceInTheDayInRange(self.startDate, lowerRange: lowerRange, upperRange: upperRange)
        }
        
        if occurType == .REPEAT_DAILY {
            // if testdingDate is less than lowerRange, it indicates out of range and can skip directly
            var testingDate = startDate < lowerRange ? lowerRange : startDate
            while occurence == nil && testingDate <= upperRange {
                occurence = possibleOccurenceInTheDayInRange(testingDate, lowerRange: lowerRange, upperRange: upperRange)
                
                let lastTestingDate = testingDate
                testingDate = 1.days.from(testingDate)!
                if let offset = dayLightSavingOffsetInSeconds(lastTestingDate, compareDate: testingDate) {
                    testingDate = offset.seconds.from(testingDate)!
                }
            }
        }
        
        if occurType == .REPEAT_BY_WEEK_MONTHLY {
            // if testdingDate is less than lowerRange, it indicates out of range and can skip directly
            var testingDate: Date = startDate < lowerRange ? lowerRange : startDate
            while occurence == nil && testingDate <= upperRange {
                // test if dayOfWeek and weekOfMonth conditions are satisfied, if yes, try to find possible occurence in the day
                let dateInLocal = DateInRegion(testingDate, region: Region.local)
                let day = dateInLocal.weekday
                let week = dateInLocal.weekOfMonth
                let dayOfWeek = DayOfWeek(rawValue: day - 1)        // convert from iOS weekDay (1-7) to server weekDay (0-6)
                let weekOfMonth = WeekInMonth(rawValue: week + 1)   // convert from iOS weekOfMonth (0...5) to server weekOfMonth (1-6)

                if dayOfWeek != nil && weekOfMonth != nil && self.daysInWeek.contains(dayOfWeek!) && self.weeksInMonth.contains(weekOfMonth!) {
                    occurence = possibleOccurenceInTheDayInRange(testingDate, lowerRange: lowerRange, upperRange: upperRange)
                }
                
                let lastTestingDate = testingDate
                testingDate = 1.days.from(testingDate)!
                if let offset = dayLightSavingOffsetInSeconds(lastTestingDate, compareDate: testingDate) {
                    testingDate = offset.seconds.from(testingDate)!
                }
            }
        }
        
        if occurType == .REPEAT_WEEKLY {
            // if testdingDate is less than lowerRange, it indicates out of range and can skip directly
            var testingDate = startDate < lowerRange ? lowerRange : startDate
            while occurence == nil && testingDate <= upperRange {
                // test if dayOfWeek condition is satisfied, if yes, try to find possible occurence in the day
                let dateInLocal = DateInRegion(testingDate, region: Region.local)
                let day = dateInLocal.weekday
                let dayOfWeek = DayOfWeek(rawValue: day - 1)        // convert from iOS weekDay (1-7) to server weekDay (0-6)
                
                if daysInWeek.contains(dayOfWeek!) {
                    occurence = possibleOccurenceInTheDayInRange(testingDate, lowerRange: lowerRange, upperRange: upperRange)
                }
                
                let lastTestingDate = testingDate
                testingDate = 1.days.from(testingDate)!
                if let offset = dayLightSavingOffsetInSeconds(lastTestingDate, compareDate: testingDate) {
                    testingDate = offset.seconds.from(testingDate)!
                }
            }
        }
        
        return occurence
    }
    
    func possibleOccurenceInTheDayInRange(_ startTime: Date?, lowerRange: Date?, upperRange: Date?) -> Date? {
        guard let startTime = startTime,
            let lowerRange = lowerRange,
            let upperRange = upperRange
            else {return nil}

        var occurence: Date? = nil
        
        // either start time or end time fall into the range, this occurence is considered as valid
        if lowerRange <= startTime && startTime <= upperRange {
            occurence = startTime
            return occurence
        }
        var endTime = self.durationInSeconds.seconds.from(startTime)!
        if lowerRange <= endTime && endTime <= upperRange {
            occurence = startTime
            return occurence
        }
        
        // try to find next occurence in the same day
        var nextOccurence = startTime
        for index in 1 ..< intervalsInDay.count {
            let seconds = secondsBetweenTwoIntervalsInDay(intervalsInDay[index - 1], interval2: intervalsInDay[index])
            nextOccurence = seconds.seconds.from(startTime)!
            if lowerRange <= nextOccurence && nextOccurence <= upperRange {
                occurence = nextOccurence
                break
            }
            endTime = self.durationInSeconds.seconds.from(nextOccurence)!
            if lowerRange <= endTime && endTime <= upperRange {
                occurence = nextOccurence
                break
            }
        }
        return occurence
    }
    
    func secondsBetweenTwoIntervalsInDay(_ interval1: String, interval2: String) -> Int {
        let times1 = interval1.components(separatedBy: "-")
        let startTimes1 = times1[0].components(separatedBy: ":")
        let secondsOfInterval1 = Int(startTimes1[0])! * 3600 + Int(startTimes1[1])! * 60 + Int(startTimes1[2])!
        let times2 = interval2.components(separatedBy: "-")
        let startTimes2 = times2[0].components(separatedBy: ":")
        let secondsOfInterval2 = Int(startTimes2[0])! * 3600 + Int(startTimes2[1])! * 60 + Int(startTimes2[2])!
        return secondsOfInterval2 - secondsOfInterval1
    }
    
    fileprivate func dayLightSavingOffsetInSeconds(_ baseDate: Date?, compareDate: Date?) -> Int? {
        guard let baseDate = baseDate, let compareDate = compareDate else { return nil }
        var seconds = 0.0
        if let timezone = Preferences.getMapTimezone(), timezone.isDaylightSavingTime(for: baseDate) || timezone.isDaylightSavingTime(for: compareDate) {
            seconds = Double(timezone.daylightSavingTimeOffset(for: baseDate)) - Double(timezone.daylightSavingTimeOffset(for: compareDate))
        }
        return seconds != 0 ? Int(seconds) : nil
    }
    
}

// MARK: - Rebuild Occurent and Event filtering
extension Occurence {
    /// Find the first valid DateRange within the given range with intervalsInDay. If no valid Range, nil is returned
    static func firstOccurenceIn(_ range: DateRange, with occurence: Occurence) -> DateRange? {
        
        /// Check if date is in the occurence type range
        func isOccurenceTypeSatisfied(date: Date, occurence: Occurence) -> Bool {
            guard let type = occurence.occurType else { return false }
            switch type {
            case .SINGLE, .REPEAT_DAILY:
                return true
            case .REPEAT_WEEKLY:
                return occurence.daysInWeek.contains(DayOfWeek(rawValue: date.weekday - 1)!) // convert from iOS weekDay (1-7) to server weekDay (0-6)
            case .REPEAT_BY_WEEK_MONTHLY:
                    if occurence.daysInWeek.contains(DayOfWeek(rawValue: date.weekday - 1)!),  // convert from iOS weekDay (1-7) to server weekDay (0-6)
                        occurence.weeksInMonth.contains(WeekInMonth(rawValue: date.weekOfMonth + 1)!) {    // convert from iOS weekOfMonth (0...5) to server weekOfMonth (1-6)
                        return true
                    }
            default: break
            }
            return false
        }
        
        guard let type = occurence.occurType else { return nil }
        let intervalsInDay = occurence.intervalsInDay
        
        var comparingDate = range.startDate
        comparingLoop: while comparingDate < range.endDate {
            for timeInterval in intervalsInDay {
                if isOccurenceTypeSatisfied(date: comparingDate, occurence: occurence),
                    let comparingRange = DateRange(theDay: comparingDate, intervalsInDay: timeInterval),
                    comparingRange.hasIntersectionWith(range) {
                    return comparingRange
                }
            }
            
            // increase date to the next testing day
            switch type {
            case .SINGLE:
                comparingDate = comparingDate.addingTimeInterval(TimeInterval.day)
            case .REPEAT_DAILY:
                comparingDate = comparingDate.addingTimeInterval(TimeInterval.day)
            case .REPEAT_WEEKLY:
                while comparingDate < range.endDate {
                    comparingDate = comparingDate.addingTimeInterval(TimeInterval.day)
                    if occurence.daysInWeek.contains(DayOfWeek(rawValue: comparingDate.weekday - 1)!) { // convert from iOS weekDay (1-7) to server weekDay (0-6)
                        break
                    }
                }
            case .REPEAT_BY_WEEK_MONTHLY:
                while comparingDate < range.endDate {
                    comparingDate = comparingDate.addingTimeInterval(TimeInterval.day)
                    if occurence.daysInWeek.contains(DayOfWeek(rawValue: comparingDate.weekday - 1)!),  // convert from iOS weekDay (1-7) to server weekDay (0-6)
                        occurence.weeksInMonth.contains(WeekInMonth(rawValue: comparingDate.weekOfMonth + 1)!) {    // convert from iOS weekOfMonth (0...5) to server weekOfMonth (1-6)
                        break
                    }
                }
            default: break comparingLoop
            }
            
        }
        return nil
    }
}

extension TimeInterval {
    static let day: Double = 86400
}
