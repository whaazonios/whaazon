//
//  CityOfSydneyEvent.swift
//  whaazon
//
//  Created by Can on 19/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class CityOfOZEvent: Event {
    
    //TODO: implement a generic method in Event class
    override var icon: UIImage {
        var image: UIImage?
        if let attribute = FilterManager.filterAttributes(of: .CityOfOZ) {
            if let options = attribute.selectedAttributeOptions?.values.sorted(by: { $0.order < $1.order }) {
                for option in options {
                    //TODO: icon urls are string of local file name at the moment, use online urls later
                    let iconName = option.iconPointer.absoluteString
                    
                    switch attribute.attributeName {
                    case "Pay Type":
                        if option.value == payType?.rawValue {
                            image = UIImage(named: iconName)
                        }
                    case "Category":
                        if category.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    case "Publisher":
                        if publisher.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    default:
                        break
                    }
                    if image != nil {
                        break
                    }
                }
            }
        }
        return image ?? domainType.values.icon
    }
    
    override init(json: JSON) {
        super.init(json: json)
    }
    
    override func evaluateEqual(_ attributeName: String, value: Any) -> Bool {
        if attributeName == "Pay Type" {
            if let stringValue = value as? String {
                return stringValue == payType?.rawValue
            }
        }
        return false
    }
    
    override func evaluateIn(_ attributeName: String, value: Any, caseSensitive: Bool) -> Bool {
        if attributeName == "Category" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return category.contains(stringValue)
                } else {
                    return category.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        if attributeName == "Publisher" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return publisher.contains(stringValue)
                } else {
                    return publisher.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        return false
    }
    
}
