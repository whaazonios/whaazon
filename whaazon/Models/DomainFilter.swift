//
//  FilterAttribute.swift
//  whaazon
//
//  Created by Can on 27/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum ComparatorType: String {
    case Equal = "=="
    case Range = "..."
    case In = "in"
}

let FilterCriteriaChangedNotification = "FilterCriteriaChangedNotification"

/**
 *  Model class to hold filter attributes of a domain
 */
class DomainFilter {
    
    let domain: String
    var normalAttributes = [FilterAttribute]()      // attributes which are always showing
    var dependentAttributes = [FilterAttribute]()   // attributes which are only showing when displayConditions are met
    
    /// all normal attributes and valid dependent attributes
    var attributes: [FilterAttribute] {
        var allAttributes = [FilterAttribute]()
        allAttributes += normalAttributes
        for attribute in dependentAttributes {
            if isValidDependentAttribute(attribute) {
                allAttributes.append(attribute)
            }
        }
        return allAttributes
    }
    
    init(json: JSON) {
        domain = json["domain"].stringValue
        for attributeJson in json["attributes"].arrayValue {
            let attribute = FilterAttribute(json: attributeJson)
            if attribute.isDependentAttribute {
                dependentAttributes.append(attribute)
            } else {
                normalAttributes.append(attribute)
            }
        }
        for attribute in normalAttributes {
            attribute.hasDependency = attributeHasDependency(attribute)
        }
    }
    
    /**
    *  Check if given attribute's display conditions are satisfied
    *
    *  @param FilterAttribute a dependent attribute to test
    *
    *  @return true if attribute's display conditions are all satisfied
    */
    fileprivate func isValidDependentAttribute(_ attribute: FilterAttribute) -> Bool {
        guard let conditions = attribute.displayConditions, conditions.count > 0 else {
            return false
        }
        var satisfied = true
        for condition in conditions {
            if let attribute = attributeWithValue(condition.attributeValue), satisfied {
                if (condition.operation == "==") && (attribute.selectedAttributeOptions?[condition.value] != nil) {
                    satisfied = true
                } else {
                    satisfied = false
                    break
                }
            } else {
                satisfied = false
                break
            }
        }
        return satisfied
    }
    
    /**
     Find an attribute with it's value
     
     - parameter attributeValue: attributeValue
     
     - returns: matching attribute or nil
     */
    fileprivate func attributeWithValue(_ attributeValue: String) -> FilterAttribute? {
        //TODO: use map to store attributes to ommit loop
        for attribute in normalAttributes {
            if attribute.attributeValue == attributeValue {
                return attribute
            }
        }
        return nil
    }
    
    /**
     Check if there is any attribute depends on the given attribute
     
     - parameter attribute: attribute to test
     
     - returns: true if there is at least one dependent attribute relies on it, otherwise false
     */
    fileprivate func attributeHasDependency(_ attribute: FilterAttribute) -> Bool {
        for dependentAttribute in dependentAttributes {
            if let conditions = dependentAttribute.displayConditions {
                for condition in conditions {
                    if condition.attributeValue == attribute.attributeValue {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    /**
     Filter events with user selected options
     
     - parameter events: events to be filtered
     
     - returns: filtered events
     */
    func filterEvents(_ events: [Event]) -> [Event] {
        var filteredEvents = [Event]()
        
        for event in events {
        
            var matchAllAttributes = false
            for attribute in attributes {
                var matchOneOption = false
                if attribute.comparatorType != nil && (attribute.comparatorType! == .Equal || attribute.comparatorType! == .In) {
                    
                    if let selectedAttributeOptions = attribute.selectedAttributeOptions {
                        for option in selectedAttributeOptions {
    //                        let attributeName = option.0
                            let compareValue = option.1.value

                            var evaluateResult = false
                            if let comparatorType = attribute.comparatorType {
                                switch comparatorType {
                                case .Equal :
                                    evaluateResult = event.evaluateEqual(attribute.attributeName, value: compareValue as AnyObject)
                                case .In :
                                    evaluateResult = event.evaluateIn(attribute.attributeName, value: compareValue, caseSensitive: false)
                                default : break
                                }
                            }
                            
                            if evaluateResult {
                                matchOneOption = true
                                break
                            }
                        }
                    }
                    
                } else if attribute.comparatorType != nil && attribute.comparatorType == .Range {
                    
                    var evaluateResult = false
                    if let comparatorType = attribute.comparatorType {
                        switch comparatorType {
                        case .Range :
                            if let result = event.evaluateRange(attribute.attributeName, lowerValue: attribute.minValue, upperValue: attribute.maxValue) {
                                evaluateResult = result
                            } else {
                                // if the evaluation was nil, it means the evaluation was not valid, but not failed, so break to the next loop instead of set to false
                                continue
                            }
                        default : break
                        }
                    }
                    
                    if evaluateResult {
                        matchOneOption = true
                        break
                    }
                    
                }
                
                matchAllAttributes = matchOneOption
                if !matchAllAttributes {
                    break
                }
            }
            
            // add the event into filtered list if it matches all filter criterias
            if matchAllAttributes {
                filteredEvents.append(event)
            }
        }
        
        return filteredEvents
    }
    
}

/**
 *  Model class to represent a filter attribute
 */
class FilterAttribute {
    
    typealias AttributeOption = (name: String, value: String, icon: URL, iconSelected: URL, iconPointer: URL, selectByDefault : Bool, order: Int)
    typealias DisplayCondition = (attributeValue: String, operation: String, value: String)
    
    // values from server
    let attributeName: String
    let attributeValue: String
    let attributeIcon: URL
    let attributeIconSelected: URL
    let operation: String
    var attributeOptions: [AttributeOption]?
    let minValue: Double?
    let maxValue: Double?
    let stepValue: Double?
    let unit: String?
    var selectedMinValue: Double?
    var selectedMaxValue: Double?
    var displayConditions: [DisplayCondition]?
    
    // user selection
    var selectedAttributeOptions: [String: AttributeOption]?
    
    var isDependentAttribute: Bool {
        return displayConditions?.count > 0
    }
    
    var comparatorType: ComparatorType? {
        return ComparatorType(rawValue: self.operation)
    }
    
    var isEqualComparatorAttribute: Bool {
        return comparatorType == ComparatorType.Equal
    }
    
    var isRangeComparatorAttribute: Bool {
        return comparatorType == ComparatorType.Range
    }
    
    var isInComparatorAttribute: Bool {
        return comparatorType == ComparatorType.In
    }

    
    // if there is any other attribute depend on this attribute
    var hasDependency = false
    
    init(json: JSON) {
        attributeName = json["attributeName"].stringValue
        attributeValue = json["attributeValue"].string ?? json["attributeName"].stringValue
        attributeIcon = URL(string: json["attributeIcon"].stringValue)!
        attributeIconSelected = URL(string: json["attributeIconSelected"].string ?? json["attributeIcon"].stringValue)!
        operation = json["operation"].stringValue
        if let optionsJson = json["attributeOptions"].array {
            attributeOptions = [AttributeOption]()
            selectedAttributeOptions = [String: AttributeOption]()
            for optionJson in optionsJson {
                let option: AttributeOption = (optionJson["name"].stringValue,
                    optionJson["value"].string ?? optionJson["name"].stringValue,
                    URL(string: optionJson["icon"].stringValue)!,
                    URL(string: optionJson["iconSelected"].string ?? optionJson["icon"].stringValue)!,
                    URL(string: optionJson["iconPointer"].string ?? optionJson["icon"].stringValue)!,
                    optionJson["selectByDefault"].boolValue,
                    optionJson["order"].int ?? 0)
                attributeOptions!.append(option)
                if option.selectByDefault {
                    selectedAttributeOptions!.updateValue(option, forKey: option.value)
                }
            }
        }
        minValue = json["minValue"].double
        maxValue = json["maxValue"].double
        stepValue = json["stepValue"].double
        unit = json["unit"].string
        selectedMinValue = json["selectedMinValueDefault"].double ?? minValue
        selectedMaxValue = json["selectedMaxValueDefault"].double ?? maxValue
        if let conditionsJson = json["displayConditions"].array {
            displayConditions = [DisplayCondition]()
            for conditionJson in conditionsJson {
                displayConditions!.append((attributeValue: conditionJson["attributeValue"].stringValue, operation: conditionJson["operation"].stringValue, value: conditionJson["value"].stringValue))
            }
        }
    }
    
    /**
     If given attribute is the same one
     
     - parameter attribute: attribute to compare
     
     - returns: true or false
     */
    func equals(_ attribute: FilterAttribute?) -> Bool {
        return self.attributeValue == attribute?.attributeValue
    }
    
    /**
     Check if given option is selected by user
     
     - parameter option: option
     
     - returns: true if the option is selected, otherwise false
     */
    func isSelectedOption(_ option: AttributeOption) -> Bool {
        if selectedAttributeOptions?[option.value] != nil {
            return true
        }
        return false
    }
    
    /**
     Mark the option as selected if it's not, otherwise, mark it as unchecked
     
     - parameter option: option to be marked
     */
    func toggleOptionSelection(_ option: AttributeOption) {
        if isSelectedOption(option) {
            selectedAttributeOptions?[option.value] = nil
            printThread(message: "set \(option.value) to nil \n \(selectedAttributeOptions)")
        } else {
            selectedAttributeOptions?[option.value] = option
            printThread(message: "set \(option.value) to \(option) \n \(selectedAttributeOptions)")
        }
//        print(selectedAttributeOptions)
        
        // notify to filter and refresh
        NotificationCenter.default.post(name: Notification.Name(rawValue: FilterCriteriaChangedNotification), object: nil)
    }

}

