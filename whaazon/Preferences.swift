//
//  Preferences.swift
//  whaazon
//
//  Created by Can on 21/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftDate

struct Preferences {
    
    static let defaultDomain = Domain.CityOfOZ
    static let defaultTimelineType = TimelineType.SevenDays
    static let defaultPresentType = PresentType.Next1Day
    static let defaults = UserDefaults.standard
    // keys
    static let selectedTimelineTypeKey = "selectedTimelineTypeKey"
    static let selectedPresentTypeKey = "selectedPresentTypeKey"
    static let mapCenterKey = "mapCenterKey"
    static let latitudeKey = "latitudeKey"
    static let longitudeKey = "longitudeKey"
    static let mapSpanKey = "mapSpanKey"
    static let latitudeDeltaKey = "latitudeDeltaKey"
    static let longitudeDeltaKey = "longitudeDeltaKey"
    static let timezoneKey = "timezoneKey"
    
    // vars
    fileprivate static var timezone: TimeZone?
    
    /// current Region
    static var region: Region {
        return Region(calendar: Calendar.current, zone: Preferences.getMapTimezone() ?? TimeZone.current, locale: Locale.current)
    }
    
    /// save user selected domain
    static func append(selected domain: Domain) {
        var currentDomains = defaults.domains
        if !currentDomains.contains(domain) {
            currentDomains.append(domain)
            defaults.domains = currentDomains
        }
    }
    
    /// get saved user selected domain, if nothing found, return default domain
    static var selectedDomains: [Domain] {
        var domains: [Domain]
        if let specificDomain = specificDomain {
            // single domain mode, such as for realestate
            domains = [specificDomain]
        } else {
            // multiple domain mode, such as for realestate, cities of oz
            domains = defaults.domains
//            if domains.isEmpty {
//                domains = defaultDomains
//                DispatchQueue.global().async { defaults.domains = domains }
//            }
        }
        return domains
    }
    
    /// If a domain has been selected
    static func isSelected(domain: Domain) -> Bool {
        return selectedDomains.contains(domain)
    }
    
    /// Select or deselect a domain
    static func toggle(domain: Domain) {
        var domains = selectedDomains
        if domains.contains(domain) {
            domains.remove(at: domains.index(of: domain)!)
            defaults.domains = domains
        } else {
            append(selected: domain)
        }
    }
    
    /// save user selected TimelineType
    static func setSelectedTimelineType(_ type: TimelineType) {
        defaults.set(type.rawValue, forKey: selectedTimelineTypeKey)
    }
    
    /// get saved user selected TimelineType, if nothing found, return default TimelineType
    static func getSelectedTimelineType() -> TimelineType {
        if let typeValue = defaults.string(forKey: selectedTimelineTypeKey) {
            return TimelineType(rawValue: typeValue) ?? defaultTimelineType
        }
        return defaultTimelineType
    }
    
    /// save user selected PresentType
    static func setSelectedPresentType(_ type: PresentType) {
        defaults.set(type.rawValue, forKey: selectedPresentTypeKey)
    }
    
    /// get saved user selected PresentType, if nothing found, return default PresentType
    static func getSelectedPresentType() -> PresentType {
        if let typeValue = defaults.string(forKey: selectedPresentTypeKey) {
            return PresentType(rawValue: typeValue) ?? defaultPresentType
        }
        return defaultPresentType
    }
    
    /// save latest location on map
    static func setLatestMapCenter(_ coordinate: CLLocationCoordinate2D) {
        let dict = [latitudeKey: Double(coordinate.latitude), longitudeKey: Double(coordinate.longitude)] as NSDictionary
        defaults.set(dict, forKey: mapCenterKey)
        defaults.synchronize()
    }
    
    /// get saved latest location on map
    static func getLatestMapCenter() -> CLLocationCoordinate2D? {
        if let dict = defaults.object(forKey: mapCenterKey) {
            let latitude = (dict as AnyObject).object(forKey: latitudeKey) as! Double
            let longitude = (dict as AnyObject).object(forKey: longitudeKey) as! Double
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return ozCities[0].location // default location
    }
    
    /// check if there is a saved location
    static func hasSavedMapCenter() -> Bool {
        return defaults.string(forKey: mapCenterKey) != nil
    }
    
    /// save latest or default region span
    static func setMapRegionSpan(_ span: MKCoordinateSpan) {
        let dict = [latitudeDeltaKey: Double(span.latitudeDelta), longitudeDeltaKey: Double(span.longitudeDelta)] as NSDictionary
        defaults.set(dict, forKey: mapSpanKey)
        defaults.synchronize()
    }
    
    /// get saved latest or default region span
    static func getMapRegionSpan() -> MKCoordinateSpan {
        if let dict = defaults.object(forKey: mapSpanKey) {
            let latitudeDelta = (dict as AnyObject).object(forKey: latitudeDeltaKey) as! Double
            let longitudeDelta = (dict as AnyObject).object(forKey: longitudeDeltaKey) as! Double
            return MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        }
        return MKCoordinateSpanMake(0.005, 0.005)
    }

    /// save latest timezone on map center
    static func setMapTimezone(_ timezone: TimeZone) {
        Preferences.timezone = timezone
        // change the system default time zone to be the one on map
        NSTimeZone.default = timezone
    }
    
    /// get latest timezone on map center, if nothing found, return nil
    static func getMapTimezone() -> TimeZone? {
//        return defaults.objectForKey(timezoneKey) as? NSTimeZone
        return Preferences.timezone
    }
    
}
