//
//  CoffeeChaseClient.swift
//  whaazon
//
//  Created by Can Zhan on 7/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Alamofire
import SwiftyJSON

class CoffeeChaseDomainClient {
    
    static let baseURL = "\(serverURL)/chargeable/"
    
    static var processing = false
    static var request: DataRequest?
    
    static func fetchCoffeeChaseEventsMatchingCriteria(_ criteria: SearchCriteria, eventDelegate: EventDelegate) {
        let url = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if processing {
            //            request!.cancel()
            NSLog("drop off fetchCoffeeChaseEventsMatchingCriteria processing request")
            return
        }
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(eventDelegate: eventDelegate)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
        request = Alamofire.request(url, method: .get)
        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        request?.validate().responseJSON { response in
            processing = false
            switch response.result {
            case .success(let value):
                //                if let data = response.result.value {
                printThread(message: "received CoffeeChaseEvent events")
                let json = JSON(value)
                var events = [Event]()
                for (_, subjson):(String, JSON) in json {
                    let event = CoffeeChaseEvent(json: subjson)
                    events.append(event)
                }
                NSLog("update view with \(events.count) events")
                eventDelegate.updateViewWithEvents(events, filterEvents: true)
                AppData.events = events
            //                }
            case .failure(let error):
                //                guard error.code != NSURLErrorCancelled else {return}
                printThread(message: error.localizedDescription)
                eventDelegate.updateViewWithError("Failed get data, please retry later.")
                guard let errorCode = (error as? AFError)?.responseCode, errorCode != NSURLErrorCancelled else { return }
            }
        }
    }
    
    /// Used by CacheManager
    static func fetchCoffeeChaseEventsMatchingCriteria(_ criteria: SearchCriteria, completion: @escaping ([Event]) -> Void) {
        Client.fetchEventsMatchingCriteria(domain: .coffeeChase, baseURL: baseURL, criteria: criteria, completion: completion)
        /*
        guard let urlString = (baseURL + criteria.criteriaURL).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url = URL(string: urlString)
            else { return }
        
        if mockMode {
            printThread(message: "******** loading event from local file")
            EventManager.loadEventsFromLocalFile(completion: completion)
            return
        } else {
            printThread(message: "******** loading event from network")
        }
        
        processing = true
        
        printThread(message: "Thread = \(Thread.current.isMainThread), Accessing URL = \(url)")
        
        let request = URLRequest(url: url)
        task?.cancel()
        task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            var events = [Event]()
            if let data = data, let json = try? JSON(data: data) {
                for (_, subjson):(String, JSON) in json {
                    let event = CoffeeChaseEvent(json: subjson)
                    events.append(event)
                }
                AppData.events = events
                printThread(message: "~~~~~~~~~~~~~~~~~~~ update view with \(events.count) CityOfSydney events at \(Date().description) ~~~~~~~~~~~~~~~~~~~")
                completion(events)
            } else if let error = error {
                printThread(message: "~~~~~~~~~~~~~~~~~~~ \(error) ~~~~~~~~~~~~~~~~~~~")
            }
        }
        task?.resume()
 */
    }
}
