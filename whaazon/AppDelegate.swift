//
//  AppDelegate.swift
//  whaazon
//
//  Created by Can on 25/11/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit
import CoreData
//import Fabric
//import Crashlytics
import SlideMenuControllerSwift
//import FacebookCore
import Stripe
import Firebase
//import GoogleSignIn
import FirebaseUI
//import FirebaseAuthUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // launch Crashlytics
        if !mockMode {
            FirebaseApp.configure()
//            setupCrashlytics()
        }
        // launch GA
//        setupGoogleAnalytics()
        // set slide menus
        setupSlideMenu()
        // setup Facebook
        if !mockMode {
//            SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        }
        // setup Google Login
        if !mockMode {
//            GIDSignIn.sharedInstance().clientID = "457937739285-2u99bp99fjepdmqd08pjsd0iutq6ngk7.apps.googleusercontent.com"
        }
        // setup Stripe
        if !mockMode {
            STPPaymentConfiguration.shared().publishableKey = "pk_test_pYtmFcQjjtFolE06FplWdGhT00jtvO0cA3"
        }
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        AppEventsLogger.activate(application)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        /*
        let appId = SDKSettings.appId
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" { // facebook
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
 */
        let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        return false
    }

    func applicationWillTerminate(_ application: UIApplication) {
        saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.waigi.whaazon" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "whaazon", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // MARK: - Setup
    
//    func setupCrashlytics() {
//        Fabric.with([Crashlytics.self])
//        Fabric.with([Answers.self])
//    }
    
    /*
    func setupGoogleAnalytics() {
        // Configure tracker from GoogleService-Info.plist.
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        GAI.sharedInstance().trackUncaughtExceptions = true  // report uncaught exceptions
//        gai.logger.logLevel = GAILogLevel.Verbose  // remove before app release
    }*/
    
    func setupSlideMenu() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let leftMenuNavigationController = storyboard.instantiateViewController(withIdentifier: StoryboardId.LeftMenuNavigationController) as! UINavigationController
        let homeNavigationController = storyboard.instantiateViewController(withIdentifier: StoryboardId.HomeNavigationController) as! UINavigationController
        let slideMenuController = SlideMenuController(mainViewController: homeNavigationController, leftMenuViewController: leftMenuNavigationController)
        if let iconImage = UIImage(named: "AppIcon") {
            slideMenuController.addLeftBarButtonWithImage(iconImage)
        }
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }

}

