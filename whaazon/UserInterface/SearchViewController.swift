//
//  SearchViewController.swift
//  whaazon
//
//  Created by Can on 29/03/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

protocol SearchDelegate {
    func updateViewWithEvents(_ events: [Event])
    func updateViewWithError(_ errorMessage: String)
}


class SearchViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, SearchDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var delegate: EventDelegate?
    static var events = [Event]() // remember searched events
    var selectedEvent: Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Search"
        
        self.navigationController?.navigationBar.isHidden = false
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.hideKeyboard))
//        tableView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trackView("SearchViewController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if SearchViewController.events.count == 0 {
            self.searchBar.becomeFirstResponder()
        }
    }
    
    @IBAction func onClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onShowOnMap(_ sender: UIButton) {
        let eventId = sender.tag
        // find clicked event
        var clickedEvent: Event? = nil
        for event in SearchViewController.events {
            if event.id == eventId {
                clickedEvent = event
                break
            }
        }
        // show on Map
        if clickedEvent != nil {
            delegate?.refreshMapWithEvent(clickedEvent!)
        }
        hideKeyboard()
        onClose()
    }
    
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SearchViewController.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "eventCell"
        let cell: PropertyEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! PropertyEventTableViewCell
        
        let event = SearchViewController.events[indexPath.row]
        
        if let url = event.imageURL {
            cell.propertyImageView.kf.setImage(with: url, placeholder: UIImage(named: "property_list_placeholder"))
        }
        cell.titleLabel.text = event.eventTitle
        if event.subtitle == nil {
            event.subtitle = event.defaultOccurenceDateString
        }
        cell.subtitleLabel.text = event.subtitle
        cell.summaryLabel.text = event.summary
        cell.showOnMapButton.tag = event.id
        cell.expiredStatus(event.expired)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showEventDetail(SearchViewController.events[indexPath.row])
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EventDetailViewController {
            vc.event = selectedEvent
        }
    }
    
    func showEventDetail(_ event: Event) {
        selectedEvent = event
        self.performSegue(withIdentifier: SegueSearch2EventDetail, sender: self)
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let keywords: String = searchBar.text {
            if !keywords.isEmpty {
                self.indicator.startAnimating()
                // search
                CityOfOZDomainClient.searchEvents(keywords, searchDelegate: self)
            }
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: - SearchDelegate
    
    func updateViewWithEvents(_ events: [Event]) {
        self.indicator.stopAnimating()
        let calculatedEvents: [Event] = events.map({
            $0.mostRecentOccurenceFromNow()
            return $0
        })
        SearchViewController.events = calculatedEvents.sorted(by: { (event1, event2) -> Bool in
            return event1.occurences[0].startDateTimeInterval < event2.occurences[0].startDateTimeInterval
        })
        self.tableView.reloadData()
    }
    
    func updateViewWithError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideKeyboard()
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
}
