//
//  DisclaimerViewController.swift
//  whaazon
//
//  Created by Can on 24/03/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class DisclaimerViewController: BaseViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var hamburgeButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    var fromSplash = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Terms & Conditions"
        hamburgeButton.isHidden = fromSplash
        closeButton.isHidden = !fromSplash
        if let url = URL(string: "http://www.whaazon.com.au/terms/") {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    @IBAction func onLeftMenu() {
        guard let slideMenuController = self.slideMenuController() else { return }
        slideMenuController.isLeftOpen() ? slideMenuController.closeLeft() : slideMenuController.openLeft()
    }
    
}
