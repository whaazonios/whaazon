//
//  BaseViewController.swift
//  whaazon
//
//  Created by Can on 19/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    lazy var tracker = {
//        GAI.sharedInstance().defaultTracker!
    }()
    
    let userDefaults = UserDefaults.standard

    // MARK: - GoogleAnalytics
    
    func trackView(_ name: String) {
//        tracker.set(kGAIScreenName, value: name)
//        tracker.send((GAIDictionaryBuilder.createScreenView().build() as NSDictionary) as! [AnyHashable: Any])
    }
    
    func trackButtonAction(_ label: String) {
//        let builder = GAIDictionaryBuilder.createEvent(withCategory: "ui_action", action: "button_press", label: label, value: 1)!
//        tracker.send((builder.build() as NSDictionary) as! [AnyHashable: Any])
    }
    
    func showVersionUpdateIfNeeded() {
        let viewModel = VersionViewModel()
        viewModel.fetchVersion { [weak self] in
            guard let strongSelf = self,
                let version = viewModel.version,
                Device.appVersion < version.appVersion,
                let viewController = UIStoryboard(name: "Version", bundle: nil).instantiateInitialViewController() as? VersionViewController
                else { return }
            viewController.viewModel = viewModel
            strongSelf.present(viewController, animated: true, completion: nil)
        }
    }
    
}

struct Device {
    static var uniqueIdentifier: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    static var osVersion: String {
        return UIDevice.current.systemVersion
    }
    
    static var appVersion: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
}
