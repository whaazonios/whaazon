//
//  PropertyEventTableViewCell.swift
//  whaazon
//
//  Created by Can on 1/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit

class PropertyEventTableViewCell: UITableViewCell {

    @IBOutlet weak var propertyImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var showOnMapButton: UIButton!
    @IBOutlet weak var expiredLabel: UILabel!
    
    func expiredStatus(_ expired: Bool) {
        showOnMapButton.isHidden = expired
        expiredLabel.isHidden = !expired
    }

}
