//
//  AboutViewController.swift
//  whaazon
//
//  Created by Can on 8/04/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "About"
        
        let request = URLRequest(url: URL(string: "http://www.whaazon.com.au/")!)
        webView.loadRequest(request)
    }
    
    @IBAction func onLeftMenu() {
        guard let slideMenuController = self.slideMenuController() else { return }
        slideMenuController.isLeftOpen() ? slideMenuController.closeLeft() : slideMenuController.openLeft()
    }

}
