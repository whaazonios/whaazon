//
//  JumpLocationViewController.swift
//  whaazon
//
//  Created by Can on 4/03/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import UIKit
import MapKit

// OZ Cities
typealias CityLocation = (cityName: String, location: CLLocationCoordinate2D)
let ozCities: [CityLocation] = [
    ("Sydney", CLLocationCoordinate2D(latitude: -33.867477, longitude: 151.206978)),
    ("Melbourne", CLLocationCoordinate2D(latitude: -37.814215, longitude: 144.963231)),
    ("Brisbane", CLLocationCoordinate2D(latitude: -27.470933, longitude: 153.023502)),
    ("Perth", CLLocationCoordinate2D(latitude: -31.953596, longitude: 115.857012)),
    ("Adelaide", CLLocationCoordinate2D(latitude: -34.928589, longitude: 138.599943)),
    ("Canberra", CLLocationCoordinate2D(latitude: -35.282, longitude: 149.128684)),
    ("Hobart", CLLocationCoordinate2D(latitude: -42.881903, longitude: 147.323815)),
    ("Darwin", CLLocationCoordinate2D(latitude: -12.4634, longitude: 130.8456))
]

class JumpLocationViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: LocationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        trackButtonAction("jump location")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ozCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "cityCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)!
        
        cell.textLabel?.text = ozCities[indexPath.row].cityName
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard delegate != nil else {return}
        delegate!.centerMapViewToLocation(ozCities[indexPath.row].location)
        onCancel()
    }
    
}
