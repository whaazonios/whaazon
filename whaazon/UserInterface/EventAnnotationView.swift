//
//  EventAnnotationView.swift
//  whaazon
//
//  Created by Can on 11/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit
import MapKit

class EventAnnotationView: MKPinAnnotationView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
