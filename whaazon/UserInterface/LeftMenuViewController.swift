//
//  LeftMenuViewController.swift
//  whaazon
//
//  Created by Can on 3/04/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class LeftMenuViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var homeScreen: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // show the navigation bar over the side menu view
        if let navigationBar = self.navigationController?.navigationBar {
            view.bringSubview(toFront: navigationBar)
        }
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trackView("LeftMenuViewController")
    }

}


// MARK: - UITableViewDataSource & UITableViewDelegate

extension LeftMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 3
        case 1: return 3
        default:return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Personal"
        case 1: return "About"
        default:return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "menuTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)!
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Home"
            case 1:
                cell.textLabel?.text = "Publish"
            case 2:
                cell.textLabel?.text = "Channel"
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "About the APP"
            case 1:
                cell.textLabel?.text = "Terms and Conditions"
            case 2:
                cell.textLabel?.text = "Feedback"
            default:
                break
            }
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if homeScreen == nil, (slideMenuController()?.mainViewController as? UINavigationController)?.topViewController is HomeViewController {
            homeScreen = slideMenuController()?.mainViewController
        }
        
        //Present new view controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                destViewController = homeScreen ?? mainStoryboard.instantiateViewController(withIdentifier: StoryboardId.HomeNavigationController)
                homeScreen = destViewController
                self.slideMenuController()?.changeMainViewController(destViewController, close: true)
            case 1:
                let portal = UIStoryboard(name: "Portal", bundle: nil).instantiateViewController(withIdentifier: StoryboardId.PortalTabBarController)
                portal.modalPresentationStyle = .fullScreen
                guard let menu = slideMenuController() else { return }
                menu.mainViewController?.present(portal, animated: true, completion: nil)
                menu.closeLeft()
            case 2:
                let chooseChannelViewController = UIStoryboard(name: "Channels", bundle: nil).instantiateViewController(withIdentifier: "ChooseChannelViewController")
                guard let menu = slideMenuController() else { return }
                menu.mainViewController?.present(chooseChannelViewController, animated: true, completion: nil)
                menu.closeLeft()
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryboardId.AboutNavigationController)
                self.slideMenuController()?.changeMainViewController(destViewController, close: true)
            case 1:
                destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryboardId.DisclaimerNavigationController)
                self.slideMenuController()?.changeMainViewController(destViewController, close: true)
            case 2:
                destViewController = mainStoryboard.instantiateViewController(withIdentifier: StoryboardId.FeedbackNavigationController)
                self.slideMenuController()?.changeMainViewController(destViewController, close: true)
            default:
                break
            }
        default:
            break
        }
        
        
    }
    
}
