//
//  SettingViewController.swift
//  whaazon
//
//  Created by Can on 7/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Setting"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBActions
    
    @IBAction func dismissView() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource & UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
//        case 1: return 2
        default:return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Personal"
//        case 1: return "About"
        default:return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "settingsTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)!
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Favourite Events"
            default:
                break
            }
//        case 1:
//            switch indexPath.row {
//            case 0:
//                cell.textLabel?.text = "About the APP"
//            case 1:
//                cell.textLabel?.text = "Terms and Conditions"
//            default:
//                break
//            }
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                self.performSegue(withIdentifier: "setting2Favourite", sender: self)
            default:
                break
            }
//        case 1:
//            switch indexPath.row {
//            case 0:
//                self.performSegueWithIdentifier("setting2About", sender: self)
//            case 1:
//                self.performSegueWithIdentifier("setting2Disclaimer", sender: self)
//            default:
//                break
//            }
        default:
            break
        }
    }
}
