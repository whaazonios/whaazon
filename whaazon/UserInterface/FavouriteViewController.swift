//
//  FavouriteViewController.swift
//  whaazon
//
//  Created by Can on 23/03/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit
import CoreData

class FavouriteViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var favouriteEvents = [NSManagedObject]()
    var events = [Event]()
    var selectedEvent: Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Favourites"
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchFavouriteEvents()
        trackView("FavouriteViewController")
    }
    
    // MARK: - IBActions
    
    @IBAction func dismissView() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "propertyEventCell"
        let cell: PropertyEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! PropertyEventTableViewCell
        
        let event = events[indexPath.row]
        
        if let url = event.imageURL {
            cell.propertyImageView.kf.setImage(with: url, placeholder: UIImage(named: "property_list_placeholder"))
        }
        cell.titleLabel.text = event.eventTitle
        cell.subtitleLabel.text = event.subtitle
        cell.summaryLabel.text = event.summary
        cell.showOnMapButton.tag = event.id
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showEventDetail(events[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteSavedEvent(favouriteEvents[indexPath.row])
//            favouriteEvents.removeAtIndex(indexPath.row)
//            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EventDetailViewController {
            vc.event = selectedEvent
        }
    }
    
    func showEventDetail(_ event: Event) {
        selectedEvent = event
        self.performSegue(withIdentifier: SegueFavourite2EventDetail, sender: self)
    }
    
    @IBAction func onShowOnMap(_ sender: UIButton) {
        let eventId = sender.tag
        // find clicked event
        var clickedEvent: Event? = nil
        for event in self.events {
            if event.id == eventId {
                clickedEvent = event
                break
            }
        }
        // show on Map
        if clickedEvent != nil {
            // notify to filter and refresh
            NotificationCenter.default.post(name: Notification.Name(rawValue: PreselectAnnotationNotification), object: nil, userInfo: ["savedEvent": clickedEvent!])
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - CoreData
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    func fetchFavouriteEvents() {
        favouriteEvents.removeAll()
        events.removeAll()
        
        // fetch data
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
        do {
            var results = try managedContext.fetch(fetchRequest)
            results = results.reversed()
            favouriteEvents = results as! [NSManagedObject]
            for favouriteEvent in favouriteEvents {
                let event = Event(id: favouriteEvent.value(forKey: "id") as? Int ?? 0,
                                  pid: favouriteEvent.value(forKey: "pid") as? String ?? "",
                                  title: favouriteEvent.value(forKey: "title") as? String ?? "",
                                  subtitle: favouriteEvent.value(forKey: "subtitle") as? String ?? "",
                                  summary: favouriteEvent.value(forKey: "summary") as? String ?? "",
                                  latitude: favouriteEvent.value(forKey: "latitude") as? Double ?? 0.0,
                                  longitude: favouriteEvent.value(forKey: "longitude") as? Double ?? 0.0,
                                  eventURLString: favouriteEvent.value(forKey: "eventURLString") as? String ?? "",
                                  imageURLString: favouriteEvent.value(forKey: "imageURLString") as? String ?? "",
                                  detailPageURLString: favouriteEvent.value(forKey: "detailPageURLString") as? String ?? "",
                                  domain: favouriteEvent.value(forKey: "domain") as? String ?? Domain.coffeeChase.rawValue,
                                  costInfo: favouriteEvent.value(forKey: "costInfo") as? String,
                                  payType: favouriteEvent.value(forKey: "payType") as? String)
                events.append(event)
            }
            self.tableView.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    fileprivate func deleteSavedEvent(_ favouriteEvent: NSManagedObject?) {
        guard favouriteEvent != nil else {return}
        
        // delete data
        managedContext.delete(favouriteEvent!)
        
        // delete
        do {
            try managedContext.save()
            fetchFavouriteEvents()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
}
