//
//  HomeViewController.swift
//  whaazon
//
//  Created by Can on 30/11/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher
import SwiftDate
import TTRangeSlider
import Crashlytics


let PreselectAnnotationNotification = "PreselectAnnotationNotification"

class HomeViewController: BaseViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, EventDelegate, LocationDelegate, TTRangeSliderDelegate {
    
    private struct SegueIdentifier {
        static let channels = "channels"
        static let showTimeSlider = "showTimeSlider"
    }
    
    // parent view
    @IBOutlet weak var toggleViewsButtonItem: UIBarButtonItem!
    @IBOutlet weak var domainSelectionContainerView: UIView!
    @IBOutlet weak var domainSelectionButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var showingMap = true
    var isMapCentered = false // has map ever been centered to user's location at the beginning
    
    // map view
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var annotationListContainerView: UIView!
    @IBOutlet weak var annotationTableView: UITableView!
    
    let locationManager = CLLocationManager()
    var regionChangeIsFromUserInteraction = false
    var lastSelectedEvent: Event?
    var filteredEventsMap = [CLLocation: [Event]]() // used to populate Annotation List View if there are multiple annotations have the same coordinate
    var displayingAnnotations = [Event]()    // displaying overlapping annotation list
    
    // toolbar view
    @IBOutlet weak var toggleToolbarButton: UIButton!
    @IBOutlet weak var toggleToolbarButtonContainerView: UIView!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var toolbarViewTrailingConstraint: NSLayoutConstraint!
    
    // filter view
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterAttributeView: UIView!
    @IBOutlet weak var filterAttributeOptionView: UIView!
    @IBOutlet weak var filterAttributeCollectionView: UICollectionView!
    @IBOutlet weak var filterAttributeOptionCollectionView: UICollectionView!
    @IBOutlet weak var filterAttributeRangeSliderView: UIView!
    @IBOutlet weak var filterAttributeRangeSlider: TTRangeSlider!
    @IBOutlet var filterViewSwipeUpGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet var filterViewSwipeDownGestureRecognizer: UISwipeGestureRecognizer!
    var isFilterViewShowing = false
    let filterAttributeCollectionViewTag = 0
    let filterAttributeOptionCollectionViewTag = 1
    
    // list view
    @IBOutlet weak var listContainerView: UIView!
//    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    
    // time picker view
    @IBOutlet weak var timePickerContainerView: UIView!
    @IBOutlet weak var timePickerLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    var pickingLowerBoundTime = true
    
    // rangeslider view
    @IBOutlet weak var rangeSlider: TTRangeSlider!
    @IBOutlet weak var timezoneLabel: UILabel!
    
    // time slider
    private var startTime: Date?
    private var endTime: Date?
    /// The current date range on time slider
    private var timeSliderDateRange: DateRange? {
        guard let startTime = startTime, let endTime = endTime else { return nil }
        return DateRange(startDate: startTime, endDate: endTime)
    }
    
    // event list
    var events = [Event]()
    /// filtered by time. Medium status
    var timeFilteredEvents = [Event]()
    /// filtered by both time and category options. Final events to be displayed.
    var filteredEvents = [Event]()      // for map view
    var searchFilteredEvents = [Event]()// for list view
    var selectedEvent: Event?
    var selectedProperty: PropertyEvent?
    
    // which event array is being used by table view
    var currentDisplayingEvents: [Event] {
        if searchController.isActive && searchController.searchBar.text != "" {
            return searchFilteredEvents
        } else {
            return filteredEvents
        }
    }

    
    // MARK: - lift cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showVersionUpdateIfNeeded()
        
        addObservers()
        
        setupFilterView()
        setupMapView()
        setupRangeSliderView() // setup filter before setup map to make sure there are initial values of initial time range
        setupLocationManager()
        setupListView()
        setupGesture()
        
        timer.fire()
//        refreshDomain()
        channelsUpdated()
        NotificationCenter.default.addObserver(self, selector: #selector(channelsUpdated), name: NSNotification.Name(rawValue: NotificationName.eventUpdated), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        trackView("HomeViewController")
        showChannelSelectionIfNeeded()
        // refresh Filter View
//        channelsUpdated()
    }
    
    deinit {
        timer.invalidate()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationName.eventUpdated), object: nil)
    }
    
    // MARK: - setup
    
    func setupLocationManager() {
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.stopUpdatingLocation()
    }
    
    func setupMapView() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // View Area
        var region = MKCoordinateRegion()
        if let latestLocation = Preferences.getLatestMapCenter() {
            region.center = latestLocation
        } else if let currentLocation = locationManager.location {
            region.center = currentLocation.coordinate
        }
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
        isMapCentered = true
        // reset timezone if needed
        updateTimezoneIfNeededInMapView(mapView)
        
        // show map as default view
        showingMap = true;
        toggleViewsButtonItem.image = UIImage(named: "button_list")
        
        // annotation container view
        annotationListContainerView.isHidden = true
        annotationTableView.tableFooterView = UIView()
        annotationTableView.dataSource = self
        annotationTableView.delegate = self
        
        visibleRegion = visibleMapCoordinates()
    }
    
    func setupListView() {
        listContainerView.isHidden = true
        // tableview
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        // search controller
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func setupRangeSliderView() {
        let minValue = Preferences.getSelectedTimelineType().minValue
        let maxValue = Preferences.getSelectedTimelineType().maxValue
        let lowerValue = Preferences.getSelectedPresentType().lowerValue
        let upperValue = Preferences.getSelectedPresentType().upperValue
        
//        rangeSlider.delegate = self
//        rangeSlider.maxValue = Float(maxValue)
//        rangeSlider.minValue = Float(minValue)
//        rangeSlider.selectedMaximum = Float(upperValue)
//        rangeSlider.selectedMinimum = Float(lowerValue)
//        rangeSlider.updateLabelPositions()
        
        // update search criteria
//        searchCriteria.timeWin = "\(integerStringFromDouble(Double(rangeSlider.minValue) * 1000)),\((Double(rangeSlider.maxValue) * 1000))"
    }
    
    func setupFilterView() {
        toggleFilterView(false)
        
        // filter categories and options
        filterAttributeCollectionView.tag = filterAttributeCollectionViewTag
        filterAttributeOptionCollectionView.tag = filterAttributeOptionCollectionViewTag
        
        filterAttributeCollectionView.dataSource = self
        filterAttributeCollectionView.delegate = self
        filterAttributeOptionCollectionView.dataSource = self
        filterAttributeOptionCollectionView.delegate = self
        // gestures
        filterViewSwipeUpGestureRecognizer.addTarget(self, action: #selector(HomeViewController.switchFilter(gestureRecognizer:)))
        filterViewSwipeDownGestureRecognizer.addTarget(self, action: #selector(HomeViewController.switchFilter(gestureRecognizer:)))
        // load filter attributes
        refreshAttributeOptionView()
    }
    
    func setupGesture() {
    }
    
    // MARK: - IBAction
    
    // show Map/List
    @IBAction func toggleView(_ sender: AnyObject) {
        if showingMap {
            toggleViewsButtonItem.image = UIImage(named: "button_map_marker")
            // refresh tableView when switch to it
            tableView.reloadData()
        } else {
            toggleViewsButtonItem.image = UIImage(named: "button_list")
        }
        mapContainerView.isHidden = showingMap
        listContainerView.isHidden = !mapContainerView.isHidden
        showingMap = !showingMap
        
        trackButtonAction(showingMap ? "show map view" : "show list view")
    }

//    @IBAction func selectDomain(_ sender: AnyObject) {
//        showDomainActionSheet(sender);
//
//        trackButtonAction("switch domain")
//    }
    
    @IBAction func onCloseAnnotationListContainerView() {
        annotationListContainerView.isHidden = true
        
        trackButtonAction("choose an annotation from popup list")
    }
    
    // show/hide toobar view
    @IBAction func onToggleToolbar() {
        let isToolbarHidden = (self.toolbarView.frame.origin.x == toggleToolbarButtonContainerView.frame.origin.x)
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            guard let strongSelf = self else { return }
            if isToolbarHidden {
                strongSelf.toolbarView.center.x -= strongSelf.toolbarView.frame.width
            } else {
                strongSelf.toolbarView.center.x += strongSelf.toolbarView.frame.width
                // hide filter view if it's displaying
                if strongSelf.isFilterViewShowing {
                    strongSelf.toggleFilterView(false)
                }
            }
            strongSelf.toggleToolbarButton.setImage(!isToolbarHidden ? UIImage(named: "icon_left_arrow") : UIImage(named: "icon_right_arrow"), for: UIControlState())
        }) 
        
        trackButtonAction("toggle toolbar")
    }
    
    // toolbar
    
    @IBAction func onCenter() {
        if let currentLocation = locationManager.location {
            regionChangeIsFromUserInteraction = true
            var region = MKCoordinateRegion()
            region.center = currentLocation.coordinate
            region.span = Preferences.getMapRegionSpan()
            mapView.setRegion(region, animated: true)
            // remove last selected Event when jump location
            lastSelectedEvent = nil
//            refreshEvents()
        }
        
        trackButtonAction("center to current location")
    }
    
    @IBAction func onSetting() {
        performSegue(withIdentifier: "SegueHome2Setting", sender: self)
        
        trackButtonAction("setting")
    }
    /*
    @IBAction func onTimeline(_ sender: UIButton) {
        showTimelineActionSheet(sender)
        
        trackButtonAction("set timeline")
    }
    
    @IBAction func onPresent(_ sender: UIButton) {
        showPresentActionSheet(sender)
        
        trackButtonAction("set present time")
    }
    */
    @IBAction func onFilter() {
        toggleFilterView(!isFilterViewShowing)
        
        trackButtonAction("show filter")
    }
    
    @IBAction func onShowOnMap(_ sender: UIButton) {
        let eventId = sender.tag
        // find clicked event
        var clickedEvent: Event? = nil
        for event in filteredEvents {
            if event.id == eventId {
                clickedEvent = event
                break
            }
        }
        // show on Map
        if clickedEvent != nil {
            toggleView(sender)
            mapView.selectAnnotation(clickedEvent!, animated: true)
        }
        
        trackButtonAction("show listed event on map")
    }
    
    // Global search
    
    @IBAction func onGlobalSearch() {
        trackButtonAction("show global search")
    }
    
    @IBAction func onLeftMenu() {
        guard let slideMenuController = slideMenuController() else { return }
        slideMenuController.isLeftOpen() ? slideMenuController.closeLeft() : slideMenuController.openLeft()
        
        trackButtonAction("show left menu")
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            // event list view
            return currentDisplayingEvents.count
        } else {
            // overlapping annotation list view
            return displayingAnnotations.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            // event list view
            let reuseIdentifier = "eventCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
            if let cell = cell as? PropertyEventTableViewCell {
                let event = currentDisplayingEvents[indexPath.row]
                
                if let url = event.imageURL {
                    cell.propertyImageView.kf.setImage(with: url, placeholder: UIImage(named: "property_list_placeholder"))
                }
                cell.titleLabel.text = event.eventTitle
                if event.subtitle == nil {
                    event.subtitle = event.defaultOccurenceDateString
                }
                cell.subtitleLabel.text = event.subtitle
                cell.summaryLabel.text = event.summary
                cell.showOnMapButton.tag = event.id
            }
            
            return cell
            
        } else {
            // overlapping annotation list view
            let reuseIdentifier = "annotationCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)!
            
            let event = displayingAnnotations[indexPath.row]
            
            cell.textLabel?.text = event.eventTitle
            cell.detailTextLabel?.text = event.subtitle
            
            return cell
        }
    }

    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView {
            // event list view
            showEventDetail(currentDisplayingEvents[indexPath.row])
        } else {
            // overlapping annotation list view
            mapView.selectAnnotation(displayingAnnotations[indexPath.row], animated: true)
            onCloseAnnotationListContainerView()
        }
    }
    
    // MARK: - Search Controller
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        searchFilteredEvents = filteredEvents.filter { event in
            guard let title = event.eventTitle, let summary = event.summary else {return false}
            return title.lowercased().contains(searchText.lowercased()) || summary.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }

    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EventDetailViewController {
            vc.event = selectedEvent
        } else if let vc = segue.destination as? JumpLocationViewController {
            vc.delegate = self
            // remove last selected Event when jump location
            lastSelectedEvent = nil
            regionChangeIsFromUserInteraction = true
        } else if let vc = (segue.destination as? UINavigationController)?.topViewController as? SearchViewController {
            vc.delegate = self
        } else if segue.identifier == SegueIdentifier.channels,
            let chooseChannelViewController = segue.destination as? ChooseChannelViewController {
            chooseChannelViewController.delegate = self
        } else if segue.identifier == SegueIdentifier.showTimeSlider,
            let timeSliderViewController = segue.destination as? TimeSliderViewController {
            timeSliderViewController.delegate = self
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var should = true
        if SegueIdentifier.channels == identifier {
            // disable multi-channel feature if a specific domain is pre-set
            should = specificDomain == nil
        }
        return should
    }
    
    func showEventDetail(_ event: Event) {
        selectedEvent = event
        performSegue(withIdentifier: SegueHome2EventDetail, sender: self)
    }
    
    @IBAction func unwindToHomeWithSegue(_ segue: UIStoryboardSegue) {
    }
    
    // MARK: - Service Call
    
    func prepareSearchCriteria() {
        searchCriteria.area = displayingMapArea()
//        searchCriteria.timeWin = "\(integerStringFromDouble(Double(rangeSlider.minValue) * 1000)),\(integerStringFromDouble(Double(rangeSlider.maxValue) * 1000))"
        let oneYearsAgoInMillionSeconds = Date().addingTimeInterval(-31536000).timeIntervalSince1970 * 1000
        let oneYearsLaterInMillionSeconds = Date().addingTimeInterval(31536000).timeIntervalSince1970 * 1000
        searchCriteria.timeWin = "\(integerStringFromDouble(oneYearsAgoInMillionSeconds * 1000)),\(integerStringFromDouble(oneYearsLaterInMillionSeconds * 1000))"
        searchCriteria.category = "all"
//        searchCriteria.domain = Preferences.getSelectedDomain().rawValue
        searchCriteria.publishBaseTime = "\(Int(Date().timeIntervalSince1970 - 365 * 24 * 3600) * 1000))"
        searchCriteria.idList = "0"
    }
    
    fileprivate func integerStringFromDouble(_ number: Double) -> String {
        let string = String(number)
        if let index = string.range(of: ".")?.lowerBound {
            return string.substring(to: index)
        }
        return ""
    }
    
    /**
     Reload events from webservice
     */
    func refreshEvents(forceReloadFromServer: Bool = false) {
        guard let dateRange = timeSliderDateRange else { return }
        let coordinates = visibleMapCoordinates()
//        indicator.startAnimating()
        printThread(message: "******** refreshEvents forceReloadFromServer = \(forceReloadFromServer)")
        DispatchQueue.global().async {
            CacheManager.shared.syncLocalWithRemote(
                minLatE6: coordinates.1,
                maxLatE6: coordinates.3,
                minLngE6: coordinates.0,
                maxLngE6: coordinates.2,
                beginTime: Int(dateRange.startDate.timeIntervalSince1970 * 1000),
                endTime: Int(dateRange.endDate.timeIntervalSince1970 * 1000)) { [weak self] in
                    guard let strongSelf = self else { return }
    //                strongSelf.indicator.stopAnimating()
                    printThread(message: "******** syncLocalWithRemote with \(CacheManager.shared.events.count) events")
                    strongSelf.updateViewWithEvents(CacheManager.shared.events)
            }
        }
    }
    
    func removeEventsFromMapView() {
//        mapView.removeAnnotations(self.filteredEvents)
//        events.removeAll()
        filteredEvents.removeAll()
//        filteredEventsMap.removeAll()
    }
    
    func removeDisplayingEventsFromMapView() {
//        mapView.removeAnnotations(self.filteredEvents)
        filteredEvents.removeAll()
//        filteredEventsMap.removeAll()
    }
    
    /**
     re-filter events without reload from web service
     */
    @objc func filterAndRefreshEvents() {
        removeDisplayingEventsFromMapView()
        refreshEventsByFilterCriteria()
        updateAnnotations()
    }
    
    fileprivate func hasSameLocation(_ coordinate: CLLocationCoordinate2D, location: CLLocation) -> Bool {
        return abs(coordinate.latitude - location.coordinate.latitude) < 0.000001 && abs(coordinate.longitude - location.coordinate.longitude) < 0.000001
    }
    
    /**
     check if given coordinate has overlapping events
     */
    fileprivate func hasOverlappingEventAt(_ coordinate: CLLocationCoordinate2D) -> Bool {
        if let events = filteredEventsMap[keyByCoordinate(coordinate, inMap: filteredEventsMap)] {
            return events.count > 1
        }
        return false
    }
    
    /**
     return an existing key in map or create a new key
     */
    fileprivate func keyByCoordinate(_ coordinate: CLLocationCoordinate2D, inMap map: [CLLocation: [Event]]) -> CLLocation {
        var key = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        for akey in map.keys {
            if hasSameLocation(coordinate, location: akey) {
                key = akey
                break
            }
        }
        return key
    }
    
    
    // MARK: - EventDelegate
    /// events: if not passed in, use default values stored in AppData
    /// filterEvents: if not passed in, use default value false, which means not changing filters
    func updateViewWithEvents(_ events: [Event], filterEvents: Bool) {
        if filterEvents {
            
//            AppData.events = events
            printThread(message: "filterEvents = \(filterEvents)")
            updateViewWithEvents(events)
            
        } else {
            printThread(message: "filterEvents = \(filterEvents)")
            // set loaded events
            if Preferences.isSelected(domain: .All) {
                self.events.append(contentsOf: events)
            } else {
                self.events = events
            }
//            calculateOccurence()
            filteredEvents = events
            
            // refresh UI
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                // refresh map
//                mapView.addAnnotations(self.filteredEvents)
                strongSelf.updateAnnotations()
                // refresh list
                strongSelf.tableView.reloadData()
                // stop spinner
//                indicator.stopAnimating()
            }
            
        }
    }
    
    func updateViewWithEvents(_ events: [Event]) {
        // set loaded events
        if Preferences.isSelected(domain: .All) {
            self.events.append(contentsOf: events)
        } else {
//            printThread(message: "self.events:\n\(self.events)\n")
            let newEventSet = Set(events)
//            printThread(message: "newEventSet:\n\(newEventSet)\n")
            let oldEventSet = Set(self.events)
//            printThread(message: "oldEventSet:\n\(oldEventSet)\n")
            // keep existing events because they have already calculated mostRecentOccurence, so no need to calculate again
            let addSet = newEventSet.subtracting(oldEventSet)
//            printThread(message: "addSet:\n\(addSet)\n")
            let remainSet = oldEventSet.intersection(newEventSet)
//            printThread(message: "remainSet:\n\(remainSet)\n")
            self.events = Array(Set(self.events).union(remainSet.union(addSet)))
//            printThread(message: "Array(remainSet.union(addSet)):\n\(Array(remainSet.union(addSet)))\n")
//            printThread(message: "self.events:\n\(self.events)\n")
//            printThread(message: "Array(remainSet.union(addSet)):\n\(Array(remainSet.union(addSet)))\n")
            printThread(message: "self.events:\n\(self.events.count)\n")
        }
        // do second level refine of events by occurrence
        let now = Date()
        refineEventsWithTimeRange()
        let diff = Date().timeIntervalSince(now)
        printThread(message: "\n\n******** refineEventsWithTimeRange() time = \(diff)\n\n")
        
        // filter by criteria and show
        refreshEventsByFilterCriteria()
        let diff2 = Date().timeIntervalSince(now) - diff
        printThread(message: "\n\n******** refreshEventsByFilterCriteria() time = \(diff2)\n\n")
    }
    
    func updateViewWithError(_ errorMessage: String) {
        DispatchQueue.main.async {
            // stop spinner
//            self.indicator.stopAnimating()
            // show alert
            let alertController = UIAlertController(title: "Sorry", message: errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] (action) -> Void in
                guard let strongSelf = self else { return }
                strongSelf.refreshEvents()
            }
            alertController.addAction(okAction)
            alertController.addAction(retryAction)
//            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func refreshMapWithEvent(_ event: Event) {
        lastSelectedEvent = event
        // update time dimention
//        slideTimeRangeToEvent(event)
        // update location and refresh will be triggered
        centerMapToEvent(event)
    }
    
    // MARK: - LocationDelegate
    
    func centerMapViewToLocation(_ location: CLLocationCoordinate2D) {
        // View Area
        var region = MKCoordinateRegion()
        region.center = location
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: - CLLocationManagerDelegate
    
    
    
    
    
    // MARK: - MapView helper methods
    
    /// Update all pins image on mapView according to selected filter
    func updateAnnotationsPinImage() {
        for annotation in mapView.annotations {
            if let annotationView = mapView.view(for: annotation), let event = annotation as? Event {
                annotationView.image = event.icon
                annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 32, height: 24)
                annotationView.centerOffset = CGPoint(x: -16, y: -12)
            }
        }
    }
    
    fileprivate func preselectLastSelectedEventIfNeeded() {
        guard self.lastSelectedEvent != nil else { return }
        
        // get all events from map
        let displayingEvents = mapView.annotations.filter({$0 is Event}) as! [Event]

        for event in displayingEvents {
            if lastSelectedEvent == event {
                // pre-select
                mapView.selectAnnotation(event, animated: true)
                break
            }
        }
//        printThread(message: "\n\n displayingEvents = \(displayingEvents.map({$0.id})) \n lastSelectedEvent = \(lastSelectedEvent?.id) \n displayingEvents = \(displayingEvents)")
    }
    
    @objc func selectAnnotation(_ notification: Notification) {
        // deselect previous one
        if lastSelectedEvent != nil {
            mapView.deselectAnnotation(lastSelectedEvent, animated: false)
        }
        // select new event
        if let savedEvent = notification.userInfo!["savedEvent"] as? Event {
            // if event still exist, select it
            if filteredEvents.contains(savedEvent) {
                lastSelectedEvent = savedEvent
//            if let event = filteredEvents.filter({$0.id == savedEvent.id}).first {
//                mapView.selectAnnotation(event, animated: true)
//                lastSelectedEvent = event
                
            // otherwise, add saved one onto map and select it
            } else {
                mapView.addAnnotation(savedEvent)
//                mapView.selectAnnotation(savedEvent, animated: true)
                lastSelectedEvent = savedEvent
            }
            preselectLastSelectedEventIfNeeded()
            centerMapToEvent(savedEvent)
        }
    }
    
    func centerMapToEvent(_ event: Event) {
        regionChangeIsFromUserInteraction = true  // do refresh
        var region = MKCoordinateRegion()
        region.center = event.coordinate
        region.span = Preferences.getMapRegionSpan()
        mapView.setRegion(region, animated: true)
        // remove last selected Event when jump location and the event is out of visible area
//        if !event(self.lastSelectedEvent, visibleInMapView: mapView) {
//            lastSelectedEvent = nil
//        }
    }
    
    fileprivate func event(_ event: Event?, visibleInMapView mapView: MKMapView) -> Bool {
        guard event != nil else { return false }
        let visibleMapRect = mapView.visibleMapRect
        let visibleEvents = mapView.annotations(in: visibleMapRect)
        return visibleEvents.contains(event!)
    }
    
    // return coordinates of current displaying map area
    func displayingMapArea() -> String {
        let mRect = mapView.visibleMapRect
        let bottomLeft = getSWCoordinate(mRect)
        let topRight = getNECoordinate(mRect)
        return "\(Int(bottomLeft.latitude * 1000000)),\(Int(bottomLeft.longitude * 1000000)),\(Int(topRight.latitude * 1000000)),\(Int(topRight.longitude * 1000000))"
    }
    
    func getNECoordinate(_ mRect: MKMapRect) -> CLLocationCoordinate2D {
        return getCoordinateFromMapRectanglePoint(MKMapRectGetMaxX(mRect), mRect.origin.y)
    }
    
    func getNWCoordinate(_ mRect: MKMapRect) -> CLLocationCoordinate2D {
        return getCoordinateFromMapRectanglePoint(MKMapRectGetMinX(mRect), mRect.origin.y)
    }
    
    func getSECoordinate(_ mRect: MKMapRect) -> CLLocationCoordinate2D {
        return getCoordinateFromMapRectanglePoint(MKMapRectGetMaxX(mRect), MKMapRectGetMaxY(mRect))
    }
    
    func getSWCoordinate(_ mRect: MKMapRect) -> CLLocationCoordinate2D {
        return getCoordinateFromMapRectanglePoint(mRect.origin.x, MKMapRectGetMaxY(mRect))
    }
    
    func getCoordinateFromMapRectanglePoint(_ x: Double, _ y: Double) -> CLLocationCoordinate2D {
        return MKCoordinateForMapPoint(MKMapPointMake(x, y))
    }
    
    /// Get the coordinates if current visible area of map, return result is (left, bottom, right, top)
    private var visibleRegion = (0, 0, 0, 0)
    private func visibleMapCoordinates() -> (Int, Int, Int, Int) {
        let mRect = mapView.visibleMapRect
        let bottomLeft = getSWCoordinate(mRect)
        let topRight = getNECoordinate(mRect)
        return (Int(bottomLeft.longitude * 1000000), Int(bottomLeft.latitude * 1000000), Int(topRight.longitude * 1000000), Int(topRight.latitude * 1000000))
    }
    
    
    // MARK: - TTRangeSliderDelegate
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
//        if sender == rangeSlider {
//            refreshEvents()
//
//            trackButtonAction("sliding time range")
            
//        } else
    if sender == filterAttributeRangeSlider {
            if let attribute = FilterManager.getCurrentFilterAttribute() {
                attribute.selectedMaxValue = Double(selectedMaximum)
                attribute.selectedMinValue = Double(selectedMinimum)

                // notify to filter and refresh
                NotificationCenter.default.post(name: Notification.Name(rawValue: FilterCriteriaChangedNotification), object: nil)
            }
        }
    }
    
    
    // MARK: - Gestures
    
    func tapOnMapView(_ gestureRecognizer: UIGestureRecognizer) {
//        printThread(message: "tap on \(gestureRecognizer.view)")

        if let navigationController = navigationController {
            navigationController.isNavigationBarHidden = !navigationController.isNavigationBarHidden
        }
        toolbarView.isHidden = !toolbarView.isHidden
    }
    
    // MARK: - Filter
    
    // show/hide filter view
    func toggleFilterView(_ showView: Bool) {
        isFilterViewShowing = showView
        
        UIView.transition(with: filterView,
                          duration: 0.6,
                          options: UIViewAnimationOptions.transitionCrossDissolve,
                          animations: { [weak self] () -> Void in
                                guard let strongSelf = self else { return }
                                strongSelf.filterView.isHidden = !showView
                            },
                          completion: nil)

    }
    
    // MARK: - Observers
    
    fileprivate func addObservers() {
        removeObservers()   // avoid duplicate observers added
        NotificationCenter.default.addObserver(self, selector: #selector(filterAndRefreshEvents), name: NSNotification.Name(rawValue: FilterCriteriaChangedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.selectAnnotation(_:)), name: NSNotification.Name(rawValue: PreselectAnnotationNotification), object: nil)
    }
    
    fileprivate func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FilterCriteriaChangedNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: PreselectAnnotationNotification), object: nil)
    }
    
    // MARK: - Timer to clear cache
    
    let timer = Timer.scheduledTimer(withTimeInterval: 180, repeats: true) { (timer) in
        // clear cached events
        CacheManager.shared.reset()
    }

}

/// Filter Event
extension HomeViewController {
    
    /// Refilter visible events with time range.
    /// shouldRefreshUI == true : refresh annotations on map, normally happens when time range is updated, thus only work on previously filtered events
    /// shouldRefreshUI == false : happens when scrolled map and reloaded data. don't refresh UI and do another filtering with search criteria
    func refineEventsWithTimeRange(shouldRefreshUI: Bool = false) {
        // find most recent occurence among all occurences
        guard let timeSliderDateRange = timeSliderDateRange, !events.isEmpty else { return }
        printThread(message: "\n\nEvent No. before time filtering = \(events.count), with lower local date = \(timeSliderDateRange.startDate) and upper local date = \(timeSliderDateRange.endDate) at timezone = \(NSTimeZone.default) \n\n")
        
        var count = 0
        for event in events {
            // only calculate those in visible map region
            if event.longitudeE6 >= Double(visibleRegion.0),
                event.latitudeE6 >= Double(visibleRegion.1),
                event.longitudeE6 <= Double(visibleRegion.2),
                event.latitudeE6 <= Double(visibleRegion.3) {
                event.calculateMostRecentValidOccurence(in: timeSliderDateRange)
                count += 1
            }
        }
        printThread(message: "******** calculateMostRecentValidOccurence \(count) of \(events.count) events")
        
        timeFilteredEvents = events.filter({ $0.firstOccurence != nil })
        let startTrackTime = Date()
        let endTrackTime = Date()
        printThread(message: "\n\n Time used to refine = \(endTrackTime.timeIntervalSince(startTrackTime))")
        printThread(message: "\n\nEvent No. after time filtering = \(events.count)\n\n")
        
        if shouldRefreshUI {
            refreshEventsByFilterCriteria()
        }
    }

    func refreshEventsByFilterCriteria() {
        let startTrackTime = Date()
        printThread(message: "\n\nEvent No. before criteria filtering = \(self.timeFilteredEvents.count)\n\n")
        // set filtered events
        let domainFilters = FilterManager.currentDomainFilters()
        for domainFilter in domainFilters {
            var eventsInDomain = [Event]()
            var eventsNotInDomain = [Event]()
            timeFilteredEvents.forEach({
                if $0.domain == domainFilter.domain {
                    eventsInDomain.append($0)
                } else {
                    eventsNotInDomain.append($0)
                }
            })
            filteredEvents.removeAll()
            filteredEvents.append(contentsOf: domainFilter.filterEvents(eventsInDomain))
            // add events not in current filters domain directly
            filteredEvents.append(contentsOf: eventsNotInDomain)
        }
        let midTrackTime = Date()
        printThread(message: "\n\n Time used to refreshEventsByFilterCriteria = \(midTrackTime.timeIntervalSince(startTrackTime)), filteredEvents = \(filteredEvents.count)")
        // refresh UI
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            // refresh map
            strongSelf.updateAnnotations()
            // refresh list if the tableView is showing
            if !strongSelf.showingMap {
                strongSelf.tableView.reloadData()
            }
            // stop spinner
//            indicator.stopAnimating()
        }
        let endTrackTime = Date()
        printThread(message: "\n\n Time used to refreshEventsByFilterCriteria = \(endTrackTime.timeIntervalSince(midTrackTime)), filteredEvents = \(filteredEvents.count)")
        printThread(message: "\n\nEvent No. after criteria filtering = \(self.filteredEvents.count)\n\n")
    }
    
    /**
     remove invalid annotations and add new annotations, call this method in MAIN_QUEUE
     */
    fileprivate func updateAnnotations() {
        let startTrackTime = Date()
        let displayingEvents = mapView.annotations.filter({$0 is Event}) as! [Event]
        // remove old and add new
        let displayingEventSet = Set(displayingEvents)
        let filteredEventSet = Set(filteredEvents)
        // remove old
        let removeSet = displayingEventSet.subtracting(filteredEventSet)
        mapView.removeAnnotations(Array(removeSet))
        // add new
        let addSet = filteredEventSet.subtracting(displayingEventSet)
        mapView.addAnnotations(Array(addSet))
        
        // reset preselected event
        lastSelectedEvent = mapView.annotations.filter { (annotation) -> Bool in
            if let event = annotation as? Event, event == lastSelectedEvent {
                lastSelectedEvent = event
                return true
            }
            return false
        }.first as? Event
        
        /**
         save events in a [CLLocation: [Event]] map structure, so that we know if a coordinate has overlapped
         */
        func populateEventsMap(_ events: [Event]) -> [CLLocation: [Event]] {
            var map = [CLLocation: [Event]]()
            for event in events {
                let key = keyByCoordinate(event.coordinate, inMap: map)
                if var eventlist = map[key] {
                    eventlist.append(event)
                    map.updateValue(eventlist, forKey: key)
                } else {
                    map.updateValue([event], forKey: key)
                }
            }
            return map
        }
        
        // calculate overlapping annotations with updated filteredEvents
        filteredEventsMap.removeAll()
        filteredEventsMap = populateEventsMap(self.filteredEvents)
        preselectLastSelectedEventIfNeeded()
        let endTrackTime = Date()
        printThread(message: "\n\n Time used to updateAnnotations = \(endTrackTime.timeIntervalSince(startTrackTime))")
    }
    
}

// MARK: - Date and Timezone handling
extension HomeViewController {
    
    func updateTimezoneIfNeededInMapView(_ mapView: MKMapView) {
        let latestTimezone = Preferences.getMapTimezone()
        if let currentTimezone = TimezoneMapper.latLngToTimezone(mapView.centerCoordinate) {
            if latestTimezone != nil && (latestTimezone! == currentTimezone) {
//                printThread(message: "\nSame timezone: latestTimezone = \(Preferences.getMapTimezone()), currentTimezone = \(TimezoneMapper.latLngToTimezone(mapView.centerCoordinate))\n")
                // same time zone, do nothing
            } else {
                Preferences.setMapTimezone(currentTimezone)
                timezoneLabel.text = "Time Zone : \(currentTimezone.identifier)"
                // reset time range slider if timezone changed
//                rangeSlider.updateLabelValues()
//                printThread(message: "\nDifferent timezone: latestTimezone = \(latestTimezone), currentTimezone = \(currentTimezone)\n")
            }
        }
    }
    
}


// MARK: - MKMapViewDelegate

extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if Preferences.hasSavedMapCenter() {
            /*dispatch_once(&Tokens.onceToken) {
                let region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, latitudinalMeters, longitudinalMeters)
                mapView.setRegion(region, animated: true)
                self.isMapCentered = true
            }*/
        }
        visibleRegion = visibleMapCoordinates()
    }

    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        // detect if region change is caused by user's interaction
        let tempView = mapView.subviews.first
        let listOfGestures = tempView!.gestureRecognizers
        for recognizer in listOfGestures! {
            if recognizer.state == UIGestureRecognizerState.began || recognizer.state == UIGestureRecognizerState.ended {
                regionChangeIsFromUserInteraction = true
                break
            }
        }
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        // only reload if region change is caused by user's interaction
        guard regionChangeIsFromUserInteraction else {return}
        regionChangeIsFromUserInteraction = false
        
        // load properties
        if isMapCentered {
            // reset timezone if needed
            updateTimezoneIfNeededInMapView(mapView)
            // avoid refresh when user zoom in
            if mapView.region.span.latitudeDelta + 0.001 >= Preferences.getMapRegionSpan().latitudeDelta {
                // reload Events from server
                refreshEvents(forceReloadFromServer: true)
            }
            printThread(message: "current latitudeDelta: \(mapView.region.span.latitudeDelta), last latitudeDelta: \(Preferences.getMapRegionSpan().latitudeDelta)")
            // save this location
            Preferences.setLatestMapCenter(self.mapView.centerCoordinate)
            Preferences.setMapRegionSpan(self.mapView.region.span)
        }
        
        trackButtonAction("map scrolled")
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let event = annotation as? Event {
            
            let identifier = "Pin"
            let annotationView: MKAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            // pin
            annotationView.image = event.icon
            annotationView.frame = CGRect(x: annotationView.frame.origin.x, y: annotationView.frame.origin.y, width: 34, height: 25)
            annotationView.centerOffset = CGPoint(x: -16, y: -12)
            
            // callout
            annotationView.canShowCallout = true
            annotationView.calloutOffset = CGPoint(x: 0, y: 0)
//            annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            let eventPreviewView = EventPreviewView()
            eventPreviewView.titleLabel.text = event.eventTitle
            eventPreviewView.summaryLabel.text = event.summary
            eventPreviewView.timeLabel.text = event.subtitle?.trimmingCharacters(in: .whitespacesAndNewlines)
            if let imageURL = event.imageURL {
                eventPreviewView.imageView.kf.setImage(with: imageURL, placeholder: UIImage(named: "property_placeholder"))
            }
            eventPreviewView.tappedHandler = { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.showEventDetail(event)
            }
            annotationView.detailCalloutAccessoryView = eventPreviewView
            
            return annotationView
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
//        preselectLastSelectedEventIfNeeded()
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let event = view.annotation as? Event {
            showEventDetail(event)
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            // Don't proceed with custom callout
            return
        }
        
        // always hide annotation list view when tap a new annotation
        onCloseAnnotationListContainerView()
        
        guard let event = view.annotation as? Event else {return}
        if hasOverlappingEventAt(event.coordinate) {
            displayingAnnotations = filteredEventsMap[keyByCoordinate(event.coordinate, inMap: filteredEventsMap)] ?? [Event]()
            annotationListContainerView.isHidden = false
            annotationTableView.reloadData()
            
        } else {
        
            if view.annotation! is Event {
                lastSelectedEvent = event
            }
            // bubble the annotation
            UIView.animate(withDuration: 0.9 ,
                                       animations: {
                                        view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                },
                                       completion: { finish in
                                        UIView.animate(withDuration: 0.9, animations: {
                                            view.transform = CGAffineTransform.identity
                                        }) 
            })
        }
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        regionChangeIsFromUserInteraction = false
        // detect if region change is caused by user's interaction
        let tempView = mapView.subviews.first
        let listOfGestures = tempView!.gestureRecognizers
        for recognizer in listOfGestures! {
            if recognizer.state == UIGestureRecognizerState.began || recognizer.state == UIGestureRecognizerState.ended {
                regionChangeIsFromUserInteraction = true
                break
            }
        }
        // only do below if region change is caused by user's interaction
        guard regionChangeIsFromUserInteraction else {return}
        // always hide annotation list view when deselect a new annotation
        onCloseAnnotationListContainerView()
        // remove last selected annotation
        lastSelectedEvent = nil
    }

}

/// Search Controller
extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

// MARK: - Domain switching
extension HomeViewController: ChannelDelegate {
    @objc func channelsUpdated() {
        // clear cached events
        CacheManager.shared.reset()
        events = []
        // refresh data
        refreshEvents(forceReloadFromServer: true)
        // refresh Filter View
        FilterManager.setCurrentFilterAttribute(nil)
        refreshAttributeAndOptionView()
        filterAndRefreshEvents()
    }
    
    private func showChannelSelectionIfNeeded() {
        if Preferences.selectedDomains.isEmpty {
            performSegue(withIdentifier: SegueIdentifier.channels, sender: self)
        }
    }
}

// MARK: - Time Slider
extension HomeViewController: SliderViewDelegate {
    func sliderViewDidSlide(startTime: Date, endTime: Date) {
        // only update when time slider slides more than 15 mins
        if self.startTime == nil {
            self.startTime = startTime
        }
        if self.endTime == nil {
            self.endTime = endTime
        }
        if let lastStartTime = self.startTime,
            let lastEndTime = self.endTime,
            abs(lastStartTime.timeIntervalSince(startTime)) >= 60     // moved 1 mins
                || abs(lastEndTime.timeIntervalSince(endTime)) >= 60  // moved 1 mins
                || self.startTime == startTime  // initial showing of the timeslider
        {
            self.startTime = startTime
            self.endTime = endTime
            DispatchQueue.global().async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.userDefaults.timeSliderStartTime = startTime
                strongSelf.userDefaults.timeSliderEndTime = endTime
                
                // time update doesn't require fetching data from server
                if strongSelf.events.isEmpty {
                    strongSelf.refreshEvents()
                    printThread(message: "****** reload data from server")
                } else {
                    strongSelf.refineEventsWithTimeRange(shouldRefreshUI: true)
                    printThread(message: "****** use data on device")
                }
            }
            DLog("******* sliderViewDidSlide time refreshed with new startTime = \(startTime), endTime = \(endTime)")
        }
    }
}

