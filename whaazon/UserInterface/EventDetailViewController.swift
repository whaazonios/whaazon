//
//  EventDetailViewController.swift
//  whaazon
//
//  Created by Can on 23/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import UIKit
import CoreData

class EventDetailViewController: UIViewController {
    
    let likeImage = UIImage(named: "button_like")?.withRenderingMode(.alwaysOriginal)
    let likedImage = UIImage(named: "button_liked")?.withRenderingMode(.alwaysOriginal)

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var likeButtonItem: UIBarButtonItem!
    
    var event: Event?
    var favouriteEvent: NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let event = self.event else { return }
        
        navigationItem.title = event.eventTitle
        
        // load web page
        if let url = event.detailPageURL {
            webView.loadRequest(URLRequest(url: url as URL))
        }
        // fetch event and set like button
        fetchEvent(self.event)
    }
    
    // MARK: - UI update
    
    fileprivate func refreshLikeButton() {
        likeButtonItem.image = favouriteEvent != nil ? likedImage : likeImage
    }
    
    @IBAction func onLike() {
        fetchEvent(self.event)
        if self.favouriteEvent == nil {
            saveEvent()
        } else {
            deleteSavedEvent()
        }
    }
    
    
    // MARK: - CoreData
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    fileprivate func saveEvent() {
        
        // prepare data
        let entity =  NSEntityDescription.entity(forEntityName: "Event", in:managedContext)
        let favouriteEvent = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        favouriteEvent.setValue(event?.id, forKey: "id")
        favouriteEvent.setValue(event?.pid, forKey: "pid")
        favouriteEvent.setValue(event?.eventTitle, forKey: "title")
        favouriteEvent.setValue(event?.subtitle, forKey: "subtitle")
        favouriteEvent.setValue(event?.summary, forKey: "summary")
        favouriteEvent.setValue(event?.eventURL?.absoluteString, forKey: "eventURLString")
        favouriteEvent.setValue(event?.detailPageURLString, forKey: "detailPageURLString")
        favouriteEvent.setValue(event?.imageURL?.absoluteString, forKey: "imageURLString")
        favouriteEvent.setValue(event?.coordinate.latitude, forKey: "latitude")
        favouriteEvent.setValue(event?.coordinate.longitude, forKey: "longitude")
        favouriteEvent.setValue(event?.domain, forKey: "domain")
        favouriteEvent.setValue(event?.costInfo, forKey: "costInfo")
        favouriteEvent.setValue(event?.payType?.rawValue, forKey: "payType")
        
        // save
        do {
            try managedContext.save()
            likeButtonItem.image = likedImage
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    fileprivate func fetchEvent(_ event: Event?) {
        guard event != nil else {return}
        
        // fetch data
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
        fetchRequest.predicate = NSPredicate(format: "id == %d", event!.id)
        do {
            let results = try managedContext.fetch(fetchRequest)
            if let favouriteEvent = (results as! [NSManagedObject]).first {
                self.favouriteEvent = favouriteEvent
//                self.event = Event(id: favouriteEvent.valueForKey("id") as! Int,
//                                  title: favouriteEvent.valueForKey("title") as! String,
//                                  subtitle: favouriteEvent.valueForKey("subtitle") as! String,
//                                  summary: favouriteEvent.valueForKey("summary") as! String,
//                                  latitude: favouriteEvent.valueForKey("latitude") as! Double,
//                                  longitude: favouriteEvent.valueForKey("longitude") as! Double,
//                                  eventURLString: favouriteEvent.valueForKey("eventURLString") as! String,
//                                  imageURLString: favouriteEvent.valueForKey("imageURLString") as! String,
//                                  detailPageURLString: favouriteEvent.valueForKey("detailPageURLString") as! String)
            }
            refreshLikeButton()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        

    }
    
    fileprivate func deleteSavedEvent() {
        guard self.favouriteEvent != nil else {return}
        
        // delete data
        managedContext.delete(self.favouriteEvent!)
        
        // delete
        do {
            try managedContext.save()
            self.favouriteEvent = nil
            refreshLikeButton()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
}
