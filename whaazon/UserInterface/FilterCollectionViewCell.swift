//
//  FilterCollectionViewCell.swift
//  whaazon
//
//  Created by Can on 25/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
