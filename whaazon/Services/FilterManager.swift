//
//  FilterManager.swift
//  whaazon
//
//  Created by Can on 28/01/2016.
//  Copyright © 2016 Waigi. All rights reserved.
//

import Foundation
import SwiftyJSON


/**
*  Filter related business logics
*/
struct FilterManager {
    
    fileprivate static var domainFilters: [DomainFilter]?
    /// displaying attribute
    fileprivate static var currentFilterAttribute: FilterAttribute?
    
    
    static func isFilterOptionSelected(_ name: String) -> Bool {
        for option in (FilterManager.getCurrentFilterAttribute()?.attributeOptions)! {
            if FilterManager.getCurrentFilterAttribute()!.isSelectedOption(option) && name.lowercased().contains(option.name.lowercased()) {
                return true
            }
        }
        return false
    }
    
    /**
     Get current displaying filter attribute
     
     - returns: filter attribute or nil if there is no attributes at all
     */
    static func getCurrentFilterAttribute() -> FilterAttribute? {
        if currentFilterAttribute != nil {
            return currentFilterAttribute
        } else {
            return currentDomainFilterAttributes().first
        }
    }
    
    /**
     Set current displaying filter attribute
     
     - parameter attribute: filter attribute
     */
    static func setCurrentFilterAttribute(_ attribute: FilterAttribute?) {
        currentFilterAttribute = attribute
    }
    
    /**
     Current selected domain's full filter attribute list. If selected domain is All, merge all filters' attributes together
     
     - returns: all attributes of the selected domain
     */
    static func currentDomainFilterAttributes() -> [FilterAttribute] {
        var attributes = [FilterAttribute]()
        for filter in currentDomainFilters() {
            attributes += filter.attributes
        }
        return attributes
    }
    
    /**
     Get current selected Domain's filter attributes. If selected domain is All, merge all filters together
     
     - returns: current selected domain's filter attributes or nil if nothing match
     */
    static func currentDomainFilters() -> [DomainFilter] {
        if domainFilters == nil || (domainFilters ?? []).isEmpty {
            domainFilters = FilterManager.fetchFilterAttributes()
        }
        var filters = [DomainFilter]()
        if let filteringDomain = UserDefaults.standard.filteringDomain, !Preferences.selectedDomains.contains(filteringDomain) {
            UserDefaults.standard.filteringDomain = Preferences.selectedDomains.first
        }
        if let domainFilters = domainFilters,
            let filteringDomain = UserDefaults.standard.filteringDomain
        {
            for domainFilter in domainFilters {
//                if Preferences.isSelected(domain: .All) {
//                    filters = domainFilters
//                } else {
                    if let domain = Domain(rawValue: domainFilter.domain),
                        Preferences.isSelected(domain: domain),
                        filteringDomain == domain
                    {
                        filters.append(domainFilter)
//                        break
                    }
//                }
            }
        }
        return filters
    }
    
    /**
     Load all domains' filter attributes for creating FilterView
     
     - returns: DomainFilters
     */
    fileprivate static func fetchFilterAttributes() -> [DomainFilter] {
        guard AppData.domainFilters.isEmpty else { return AppData.domainFilters }
        var domainFilters = [DomainFilter]()
        guard let fileURL = Bundle.main.url(forResource: "FilterAttribute", withExtension: "json") else { return domainFilters }
        do {
            let data = try Data(contentsOf: fileURL, options: .uncached)
            let json = try JSON(data: data)
            for (_, subJson: domainJson) in json {
                let domainFilter = DomainFilter(json: domainJson)
                printThread(message: domainFilter.attributes.description)
                domainFilters.append(domainFilter)
            }
            return domainFilters
        } catch {
            printThread(message: "Failed to read location.json")
        }
        AppData.domainFilters = domainFilters
        return domainFilters
    }
    
    /// Filter attributes of the specific domain
    static func filterAttributes(of domain: Domain) -> FilterAttribute? {
        if let filteringDomain = UserDefaults.standard.filteringDomain,
            filteringDomain == domain {
            return getCurrentFilterAttribute()
        } else {
            return fetchFilterAttributes().filter{ $0.domain == domain.rawValue }.first?.attributes.first
        }
    }
    
}
