//
//  TickBoxView.swift
//  whaazon
//
//  Created by Can Zhan on 7/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import UIKit

protocol TickBoxViewDelegate: class {
    /// Tick box is tapped
    func tickBoxViewDidChange(isTicked: Bool, tickBoxView: TickBoxView)
}

/// A checkbox with animated tick
@IBDesignable class TickBoxView: UIView {
    
    // MARK: - Vars
    
    /// TickBoxViewDelegate
    weak var delegate: TickBoxViewDelegate?
    
    /// Is the tick box is round cornered or as a square
    @IBInspectable var isRoundCornered = false {
        didSet {
            commonInit()
        }
    }
    
    /// Tick Box border color when not ticked
    @IBInspectable var untickedBorderColor: UIColor = .lightGray {
        didSet {
            commonInit()
        }
    }
    
    /// Tick Box background color when not ticked
    @IBInspectable var untickedBackgroundColor: UIColor = .groupTableViewBackground {
        didSet {
            commonInit()
        }
    }
    
    /// Tick Box border color when ticked
    @IBInspectable var tickedBorderColor: UIColor = .darkGray {
        didSet {
            commonInit()
        }
    }
    
    /// Tick Box background color when ticked
    @IBInspectable var tickedBackgroundColor: UIColor = .groupTableViewBackground {
        didSet {
            commonInit()
        }
    }
    
    /// Tick color
    @IBInspectable var tickColor: UIColor = .darkGray
    
    /// Tick width
    @IBInspectable var tickWidth: CGFloat = 3
    
    /// Ticking status
    @IBInspectable var isTicked = false {
        didSet {
            drawTick()
        }
    }
    
    /// True: allow user to untick it from UI, False: can only untick programmatically, used to control a group of tickers and there should always be and only be one box ticked.
    // TODO: refactoring this feature by creating a new group options
    @IBInspectable var isUntickByUserAllowed: Bool = true
    
    /// The layer to draw tick
    private var shapeLayer = CAShapeLayer()
    
    /// The previous layer to be removed after the new one is finished drawing
    fileprivate var oldShapeLayer: CAShapeLayer?
    
    /// Path to draw tick
    private lazy var tickPath: UIBezierPath = {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: frame.width / 5, y: frame.height / 2))
        path.addLine(to: CGPoint(x: (frame.width / 5) * 2, y: (frame.height / 5) * 3.5))
        path.addLine(to: CGPoint(x: (frame.width / 5) * 4, y: (frame.height / 5) * 1))
        return path
    }()
    
    /// Path to draw tick
    private lazy var reversedTickPath: UIBezierPath = {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: frame.width / 5 - 1, y: frame.height / 2 - 1))
        path.addLine(to: CGPoint(x: (frame.width / 5) * 2, y: (frame.height / 5) * 3.5))
        path.addLine(to: CGPoint(x: (frame.width / 5) * 4 + 1, y: (frame.height / 5) * 1 - 1))
        return path
    }()
    
    // MARK: - Computed Vars
    
    private var borderColor: UIColor {
        return isTicked ? tickedBorderColor : untickedBorderColor
    }
    
    private var currentBackgroundColor: UIColor {
        return isTicked ? tickedBackgroundColor : untickedBackgroundColor
    }
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // tap gesture
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TickBoxView.toggleTickFromUI)))
        // border and background
        refreshBorderAndBackground()
    }
    
    private func refreshBorderAndBackground() {
        // border
        if isRoundCornered {
            applyBorder(1.0, borderColor, 5.0)
        } else {
            applyBorder(1.0, borderColor, 0.0)
        }
        // background
        backgroundColor = currentBackgroundColor
    }
    
    // MARK: - Draw Tick
    
    @objc private func toggleTickFromUI() {
        if !isTicked || (isTicked && isUntickByUserAllowed) {
            toggleTick()
        }
    }
    
    private func toggleTick() {
        isTicked = !isTicked
        // border and background
        refreshBorderAndBackground()
        // draw
        drawTick()
        // notify delegate
        delegate?.tickBoxViewDidChange(isTicked: isTicked, tickBoxView: self)
    }
    
    private func drawTick() {
        oldShapeLayer = shapeLayer
        shapeLayer = CAShapeLayer()
        shapeLayer.backgroundColor = currentBackgroundColor.cgColor
        shapeLayer.lineWidth = isTicked ? tickWidth : tickWidth + 1
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = isTicked ? tickColor.cgColor : currentBackgroundColor.cgColor
        shapeLayer.path = isTicked ? tickPath.cgPath : reversedTickPath.cgPath
        // ShapeLayer
        layer.addSublayer(shapeLayer)
        // show animation
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.duration = 0.2
        animation.delegate = self
        shapeLayer.add(animation, forKey: "TickAnimation")
    }
    
    /// Operation Functions
    
    func tick() {
        if isTicked == false {
            toggleTick()
        }
    }
    
    func untick() {
        if isTicked == true {
            toggleTick()
        }
    }
}

extension TickBoxView: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        oldShapeLayer?.removeFromSuperlayer()
    }
}

