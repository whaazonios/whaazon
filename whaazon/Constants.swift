//
//  Constants.swift
//  whaazon
//
//  Created by Can on 2/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftDate

/// load data from local JSON file if in mock mode
let mockMode = false
/// allow specific domain only if this value is set
let specificDomain: Domain? = nil//.RealEstate
/// if there is no domain been selected, set these as default values
let defaultDomains = [Domain.CityOfOZ, Domain.coffeeChase]

let googleAnalyticsTrackingId = "UA-75965839-1"

/* ViewController Identifiers */
let WONavigationControllerHome = "WONavigationControllerHome"

// Storyboard Ids
struct StoryboardId {
    static let FilterViewController = "FilterViewController"
    static let HomeViewController = "HomeViewController"
    static let DisclaimerViewController = "DisclaimerViewController"
    static let FavouriteViewController = "FavouriteViewController"
    static let AboutViewController = "AboutViewController"
    static let LeftMenuViewController = "LeftMenuViewController"
    static let HomeNavigationController = "HomeNavigationController"
    static let LeftMenuNavigationController = "LeftMenuNavigationController"
    static let DisclaimerNavigationController = "DisclaimerNavigationController"
    static let AboutNavigationController = "AboutNavigationController"
    static let FeedbackNavigationController = "FeedbackNavigationController"
    static let PortalNavigationViewController = "PortalNavigationViewController"
    static let PortalTabBarController = "PortalTabBarController"
}

// Segues
let SegueHome2EventDetail = "SegueHome2EventDetail"
let SegueFavourite2EventDetail = "SegueFavourite2EventDetail"
let SegueSearch2EventDetail = "SegueSearch2EventDetail"
let SegueHome2Setting = "SegueHome2Setting"

// Screen
let screenSize: CGRect = UIScreen.main.bounds

// Date
let DATE_FORMAT = "dd MM yyyy"
let INTERVAL_FORMAT = "dd MM yyyy H:m:s"
let DISPLAY_FORMAT = "EEE d MMM yyyy h:mm aa"
let DISPLAY_FORMAT2 = "h:mm aa" // Mon 17 Oct 2016 11:00 am - 12:00 pm
/// "3 September - 30 September"
let DAY_MONTH_DATE_FORMAT = "dd MMM yyyy"

// Server
//let serverURL = "http://172.20.10.2:8080/MapForEventsServer1"    // DEV
//let serverURL = "http://m4e.chen.id.au:8081/MapForEventsServer1"    // UAT

#if DEVELOPMENT
    let serverURL = "http://m4e.chen.id.au:8081/MapForEventsServer1"    // UAT
#else
    let serverURL = "http://m4e.whaazon.com.au:8080/MapForEventsServer1"// PROD
#endif

#if DEVELOPMENT
let serverDomain = "m4e.chen.id.au"    // UAT
#else
let serverDomain = "m4e.whaazon.com.au"// PROD
#endif

// SearchCriteria
var searchCriteria = SearchCriteria()

struct Tokens {
    static var onceToken: Int = 0
}

let latitudinalMeters: CLLocationDistance = 800
let longitudinalMeters: CLLocationDistance = 800

// enums

enum Domain: String {
    case All = "All"
    case CityOfOZ = "Cities Of Oz"
    case RealEstate = "RealEstate"
    case Garden = "Garden"
    case Whaazon = "Whaazon"
    case foodAndDrinkSpecials = "Food and Drink Specials"
    case coffeeChase = "Coffee Chase"
    
    static let allValues = [CityOfOZ, RealEstate, foodAndDrinkSpecials, coffeeChase]
    
    var values: (description: String, icon: UIImage, logo: UIImage) {
        switch self {
        case .All:
            return ("All", UIImage(named: "icon_domain_default")!, UIImage(named: "icon_domain_default")!)
        case .CityOfOZ:
            return ("City of OZ", UIImage(named: "city_of_oz_icon_misc") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "logo cities of oz")!)
        case .RealEstate:
            return ("Open House", UIImage(named: "icon_domain_realestate") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "logo realestate")!)
        case .Garden:
            return ("Garden", UIImage(named: "icon_domain_garden") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "logo my ads")!)
        case .Whaazon:
            return ("Whaazon", UIImage(named: "icon_domain_garden") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "logo my ads")!)
        case .foodAndDrinkSpecials:
            return ("Food and Drink Specials", UIImage(named: "ctxt_fooddrink") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "foodspecials")!)
        case .coffeeChase:
            return ("Coffee Chase", UIImage(named: "icon_domain_coffee_chase") ?? UIImage(named: "icon_domain_default")!, UIImage(named: "logo coffee chase")!)
        }
    }
}

// defines minValue and maxValue of time rangeSlider
enum TimelineType: String {
    case OneDay = "1 day"
    case ThreeDays = "3 days"
    case SevenDays = "7 days"
    case Fortnight = "fortnight"
    case OneMonth = "1 month"
    case ThreeMonths = "3 months"
    case SixMonths = "6 months"
    case OneYear = "1 year"
    
    static let allValues = [OneDay, ThreeDays, SevenDays, Fortnight, OneMonth, ThreeMonths, SixMonths, OneYear]

    var minValue: Double {
        let now = Date()
        switch self {
        case .OneDay:
            return (now - 1.days).timeIntervalSince1970
        case .ThreeDays:
            return (now - 3.days).timeIntervalSince1970
        case .SevenDays:
            return (now - 7.days).timeIntervalSince1970
        case .Fortnight:
            return (now - 14.days).timeIntervalSince1970
        case .OneMonth:
            return (now - 1.months).timeIntervalSince1970
        case .ThreeMonths:
            return (now - 3.months).timeIntervalSince1970
        case .SixMonths:
            return (now - 6.months).timeIntervalSince1970
        case .OneYear:
            return (now - 1.years).timeIntervalSince1970
        }
    }
    
    var maxValue: Double {
        let now = Date()
        switch self {
        case .OneDay:
            return (now + 1.days).timeIntervalSince1970
        case .ThreeDays:
            return (now + 3.days).timeIntervalSince1970
        case .SevenDays:
            return (now + 7.days).timeIntervalSince1970
        case .Fortnight:
            return (now + 14.days).timeIntervalSince1970
        case .OneMonth:
            return (now + 1.months).timeIntervalSince1970
        case .ThreeMonths:
            return (now + 3.months).timeIntervalSince1970
        case .SixMonths:
            return (now + 6.months).timeIntervalSince1970
        case .OneYear:
            return (now + 1.years).timeIntervalSince1970
        }
    }
    
}

// defines lowerValue and upperValue of time rangeSlider
enum PresentType: String {
    case Next15Mins = "next 15 mins"
    case Next30Mins = "next 30 mins"
    case Next1Hour = "next 1 hour"
    case Next2Hours = "next 2 hours"
    case Next3Hours = "next 3 hours"
    case Next6Hours = "next 6 hours"
    case Next12Hours = "next 12 hours"
    case Next1Day = "next 1 day"
    case Next2Days = "next 2 days"
    case Next3Days = "next 3 days"
    case Next1Week = "next 1 week"
    case Next2Weeks = "next 2 weeks"
    case Next1Month = "next 1 month"
    case Next2Months = "next 2 months"
    case Next3Months = "next 3 months"
    case Next6Months = "next 6 months"
    case Next1Year = "next 1 year"
    
    static let allValues = [Next15Mins, Next30Mins, Next1Hour, Next2Hours, Next3Hours, Next6Hours, Next12Hours, Next1Day, Next2Days, Next3Days, Next1Week, Next2Weeks, Next1Month, Next2Months, Next3Months, Next6Months, Next1Year]
    static let oneDayValues = [Next15Mins, Next30Mins, Next1Hour, Next2Hours, Next3Hours, Next6Hours, Next12Hours, Next1Day]
    static let threeDaysValues = [Next6Hours, Next12Hours, Next1Day, Next2Days, Next3Days]
    static let sevenDaysValues = [Next6Hours, Next12Hours, Next1Day, Next2Days, Next3Days, Next1Week]
    static let fortnightValues = [Next1Day, Next2Days, Next3Days, Next1Week, Next2Weeks]
    static let oneMonthValues = [Next1Day, Next2Days, Next3Days, Next1Week, Next2Weeks, Next1Month]
    static let threeMonthsValues = [Next1Week, Next2Weeks, Next1Month, Next2Months, Next3Months]
    static let sixMonthValues = [Next2Weeks, Next1Month, Next2Months, Next3Months, Next6Months]
    static let oneYearValue = [Next1Month, Next2Months, Next3Months, Next6Months, Next1Year]
    
    static func valuesOfTimeline(_ timeline: TimelineType) -> [PresentType] {
        switch timeline {
        case .OneDay: return oneDayValues
        case .ThreeDays: return threeDaysValues
        case .SevenDays: return sevenDaysValues
        case .Fortnight: return fortnightValues
        case .OneMonth: return oneMonthValues
        case .ThreeMonths: return threeMonthsValues
        case .SixMonths: return sixMonthValues
        case .OneYear: return oneYearValue
        }
    }
    
    var lowerValue: Double {
        return Date().timeIntervalSince1970
    }
    
    var upperValue: Double {
        let now = Date()
        switch self {
        case .Next15Mins:
            return (now + 15.minutes).timeIntervalSince1970
        case .Next30Mins:
            return (now + 30.minutes).timeIntervalSince1970
        case .Next1Hour:
            return (now + 1.hours).timeIntervalSince1970
        case .Next2Hours:
            return (now + 2.hours).timeIntervalSince1970
        case .Next3Hours:
            return (now + 3.hours).timeIntervalSince1970
        case .Next6Hours:
            return (now + 6.hours).timeIntervalSince1970
        case .Next12Hours:
            return (now + 12.hours).timeIntervalSince1970
        case .Next1Day:
            return (now + 1.days).timeIntervalSince1970
        case .Next2Days:
            return (now + 2.days).timeIntervalSince1970
        case .Next3Days:
            return (now + 3.days).timeIntervalSince1970
        case .Next1Week:
            return (now + 7.days).timeIntervalSince1970
        case .Next2Weeks:
            return (now + 14.days).timeIntervalSince1970
        case .Next1Month:
            return (now + 1.months).timeIntervalSince1970
        case .Next2Months:
            return (now + 2.months).timeIntervalSince1970
        case .Next3Months:
            return (now + 3.months).timeIntervalSince1970
        case .Next6Months:
            return (now + 6.months).timeIntervalSince1970
        case .Next1Year:
            return (now + 1.years).timeIntervalSince1970
        }
    }
    
}

// MARK: - Notifications

struct NotificationName {
    static let eventUpdated = "eventUpdated"
}
