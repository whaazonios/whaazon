//
//  Protocals.swift
//  whaazon
//
//  Created by Can on 9/12/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//

import Foundation
import MapKit

protocol EventDelegate {
    
    var events: [Event] { get }
    
    func updateViewWithEvents(_ events: [Event], filterEvents: Bool)
    func updateViewWithEvents(_ events: [Event])
    func updateViewWithError(_ errorMessage: String)
    func refreshMapWithEvent(_ event: Event)
}

protocol LocationDelegate {
    
    func centerMapViewToLocation(_ location: CLLocationCoordinate2D)
    
}
