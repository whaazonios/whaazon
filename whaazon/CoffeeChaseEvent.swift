//
//  CoffeeChaseEvent.swift
//  whaazon
//
//  Created by Can Zhan on 7/2/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class CoffeeChaseEvent: Event {
    
    //TODO: implement a generic method in Event class
    override var icon: UIImage {
        var image: UIImage?
        if let attribute = FilterManager.filterAttributes(of: .coffeeChase) {
            if let options = attribute.selectedAttributeOptions?.values.sorted(by: { $0.order < $1.order }) {
                for option in options {
                    //TODO: icon urls are string of local file name at the moment, use online urls later
                    let iconName = option.iconSelected.absoluteString
                    
                    switch attribute.attributeName {
                    case "Type":
                        if tags.lowercased().contains(option.value.lowercased()) {
                            image = UIImage(named: iconName)
                        }
                    default:
                        break
                    }
                    if image != nil {
                        break
                    }
                }
            }
        }
        return image ?? domainType.values.icon
    }
    
    override init(json: JSON) {
        super.init(json: json)
    }
    
    override func evaluateIn(_ attributeName: String, value: Any, caseSensitive: Bool) -> Bool {
        if attributeName == "Type" {
            if let stringValue = value as? String {
                if caseSensitive {
                    return tags.contains(stringValue)
                } else {
                    return tags.lowercased().contains(stringValue.lowercased())
                }
            }
        }
        return false
    }
    
}
