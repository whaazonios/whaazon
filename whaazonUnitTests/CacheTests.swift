//
//  CacheTests.swift
//  whaazonUnitTests
//
//  Created by Can Zhan on 25/7/18.
//  Copyright © 2018 Waigi. All rights reserved.
//

import XCTest
@testable import whaazon

class RectTileTesselationTest: XCTestCase {
    
    func testNoTile() {
        // Create an empty tesselation
        let ts1 = Tesselation()
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        let sbbox = ts1.findSuperBoundingRectForRegion(0, 0, 100, 100, &spaceTiles, &solidTiles)
        
        let spaceTile = spaceTiles.first!
        XCTAssert(spaceTile.left == 0)
        XCTAssert(spaceTile.bottom == 0)
        XCTAssert(spaceTile.right == 100)
        XCTAssert(spaceTile.top == 100)
    }
    
    func testAddOneSolidTile() {
        // Create an empty tesselation
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: 0))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 0, right: 0, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 0, right: Int.max, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 100, right: Int.max, top: Int.max))
        solidTiles.append(RectTile.createSolidTile(left: 0, bottom: 0, right: 100, top: 100, data: 1))
        let tsRef = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        
        XCTAssertEqual(ts1, tsRef)
    }
    
    func testAddAnotherSolidTileAbove() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 0, bottom: 100, right: 100, top: 200)
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: 0))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 0, right: 0, top: 200))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 0, right: Int.max, top: 200))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 200, right: Int.max, top: Int.max))
        solidTiles.append(RectTile.createSolidTile(left: 0, bottom: 0, right: 100, top: 200, data: 1))
        let tsRef = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        
        XCTAssertEqual(ts1, tsRef)
    }
    
    func testAddSolidTileAboveRight() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 200, bottom: 200, right: 300, top: 300)
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: 0))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 0, right: 0, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 0, right: Int.max, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 100, right: Int.max, top: 200))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 200, right: 200, top: 300))
        spaceTiles.append(RectTile.createSpaceTile(left: 300, bottom: 200, right: Int.max, top: 300))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 300, right: Int.max, top: Int.max))
        solidTiles.append(RectTile.createSolidTile(left: 0, bottom: 0, right: 100, top: 100, data: 1))
        solidTiles.append(RectTile.createSolidTile(left: 200, bottom: 200, right: 300, top: 300, data: 1))
        let tsRef = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        
        XCTAssertEqual(ts1, tsRef)
    }
    
    func testAddSolidTileRight() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 200, bottom: 0, right: 300, top: 100)
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: 0))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 0, right: 0, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 0, right: 200, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: 300, bottom: 0, right: Int.max, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 100, right: Int.max, top: Int.max))
        solidTiles.append(RectTile.createSolidTile(left: 0, bottom: 0, right: 100, top: 100, data: 1))
        solidTiles.append(RectTile.createSolidTile(left: 200, bottom: 0, right: 300, top: 100, data: 1))
        let tsRef = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        
        XCTAssertEqual(ts1, tsRef)
    }
    
    func testAddSolidTileRightSlightBelow() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 200, bottom: -20, right: 300, top: 80)
        
        // Create a tesselation with known tiles
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: Int.min, right: Int.max, top: -20))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: -20, right: 200, top: 0))
        spaceTiles.append(RectTile.createSpaceTile(left: 300, bottom: -20, right: Int.max, top: 80))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 0, right: 0, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 0, right: 200, top: 80))
        spaceTiles.append(RectTile.createSpaceTile(left: 100, bottom: 80, right: Int.max, top: 100))
        spaceTiles.append(RectTile.createSpaceTile(left: Int.min, bottom: 100, right: Int.max, top: Int.max))
        solidTiles.append(RectTile.createSolidTile(left: 0, bottom: 0, right: 100, top: 100, data: 1))
        solidTiles.append(RectTile.createSolidTile(left: 200, bottom: -20, right: 300, top: 80, data: 1))
        let tsRef = Tesselation(spaceTiles: spaceTiles, solidTiles: solidTiles)
        
        print("ts1:\n\(ts1)\n\nexpected ts:\n\(tsRef)")
        
        XCTAssertEqual(ts1, tsRef)
    }
    
    func testFindSuperBoundingRect() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 40, bottom: 80, right: 100, top: 100)
        ts1.addTile(value: 1, left: 80, bottom: 0, right: 100, top: 60)
        ts1.addTile(value: 1, left: -10, bottom: 0, right: 60, top: 20)
        
        print("ts1:\n\(ts1)")
        
        let bb = ts1.findSuperBoundingRectForRegion(90, 59, 100, 81)
        
        XCTAssertEqual(bb.left, -10)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 100)
        XCTAssertEqual(bb.top, 100)
    }
    
    func testFindSuperBoundingRect1() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 40, bottom: 80, right: 100, top: 100)
        ts1.addTile(value: 1, left: 80, bottom: 0, right: 100, top: 60)
        ts1.addTile(value: 1, left: -10, bottom: 0, right: 60, top: 20)
        ts1.addTile(value: 1, left: -30, bottom: 95, right: 5, top: 105)
        print("ts1:\n\(ts1)")
        
        let bb = ts1.findSuperBoundingRectForRegion(90, 59, 110, 81)
        
        XCTAssertEqual(bb.left, -30)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 110)
        XCTAssertEqual(bb.top, 105)
    }
    
    func testConvertBoundingRectToSolidTile() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 40, bottom: 80, right: 100, top: 100)
        ts1.addTile(value: 1, left: 80, bottom: 0, right: 100, top: 60)
        ts1.addTile(value: 1, left: -10, bottom: 0, right: 60, top: 20)
        ts1.addTile(value: 1, left: -30, bottom: 95, right: 5, top: 105)
        print("ts1:\n\(ts1)")
        
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        // bb is used in convertBoundingRectToSolidTile() to clear old tiles and add a new bigger tile with old
        // data + new data. spaceTiles are used to generate query string to do server side query. solidTiles are
        // used to get old data which is going to be used when merging with new data.
        let bb = ts1.findSuperBoundingRectForRegion(90, 59, 110, 81, &spaceTiles, &solidTiles)
        spaceTiles.forEach{ print($0) }
        solidTiles.forEach{ print($0) }
        
        XCTAssertEqual(bb.left, -30)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 110)
        XCTAssertEqual(bb.top, 105)
        
        // create a new solid tile which bounds all smaller solid and space tiles, and clear the smaller ones.
        try? ts1.convertBoundingRectToSolidTile(bb, 1)
        print(ts1)
    }
    
    func testFindSuperOfTwoSolidTouchingTiles() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 50, bottom: 100, right: 100, top: 150)
        print("ts1:\n\(ts1)")
        
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        var bb = ts1.findSuperBoundingRectForRegion(60, 90, 90, 110, &spaceTiles, &solidTiles)
        spaceTiles.forEach{ print($0) }
        solidTiles.forEach{ print($0) }
        
        XCTAssertEqual(bb.left, 0)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 100)
        XCTAssertEqual(bb.top, 150)
        
        print("ts1:\n\(ts1)")
        
        bb = ts1.findSuperBoundingRectForRegion(60, 90, 100, 100, &spaceTiles, &solidTiles)
        
        XCTAssertEqual(bb.left, 0)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 100)
        XCTAssertEqual(bb.top, 100)
    }
    
    func testFindSuperOfDiagonallyTouchingTiles() {
        let ts1 = Tesselation()
        ts1.addTile(value: 1, left: 0, bottom: 0, right: 100, top: 100)
        ts1.addTile(value: 1, left: 100, bottom: 100, right: 200, top: 200)
        print("ts1:\n\(ts1)")
        
        var spaceTiles = [RectTile]()
        var solidTiles = [RectTile]()
        var bb = ts1.findSuperBoundingRectForRegion(90, 90, 110, 110, &spaceTiles, &solidTiles)
        spaceTiles.forEach{ print($0) }
        solidTiles.forEach{ print($0) }
        
        XCTAssertEqual(bb.left, 0)
        XCTAssertEqual(bb.bottom, 0)
        XCTAssertEqual(bb.right, 200)
        XCTAssertEqual(bb.top, 200)
        
        print("ts1:\n\(ts1)")
        
        bb = ts1.findSuperBoundingRectForRegion(120, 90, 130, 100, &spaceTiles, &solidTiles)
        
        XCTAssertEqual(bb.left, 120)
        XCTAssertEqual(bb.bottom, 90)
        XCTAssertEqual(bb.right, 130)
        XCTAssertEqual(bb.top, 100)
    }
    
}
