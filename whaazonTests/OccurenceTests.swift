//
//  OccurenceTests.swift
//  whaazonTests
//
//  Created by Can Zhan on 8/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import XCTest
import SwiftDate
@testable import Whaazon

class OccurenceTests: XCTestCase {
    
    func testSingleFirstOccurenceIn() {
        // start time : "2019-3-12 14:30:00"
        let today = DateInRegion(year: 2019, month: 3, day: 12, hour: 14, minute: 30, region: Region.current).date
        // start time : "2019-3-19 14:30:00"
        let aWeekLater = today.addingTimeInterval(TimeInterval.day * 7)
        let range = DateRange(startDate: today, endDate: aWeekLater)
        var occurence = Occurence()
        occurence.occurType = .SINGLE
        
        occurence.intervalsInDay = ["11:00:00-13:00:00"]
        let occurenceRange1 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange1)
        XCTAssertEqual(occurenceRange1!.startDate.day, today.day + 1)   // only match on tomorrow
        XCTAssertEqual(occurenceRange1!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange1!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.startDate.second, 0)
        XCTAssertEqual(occurenceRange1!.endDate.day, today.day + 1)   // only match on tomorrow
        XCTAssertEqual(occurenceRange1!.endDate.hour, 13 - 11)
        XCTAssertEqual(occurenceRange1!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-13:00:00", "14:00:00-15:00:00"]
        let occurenceRange2 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange2)
        XCTAssertEqual(occurenceRange2!.startDate.day, today.day)
        XCTAssertEqual(occurenceRange2!.startDate.hour, 14 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange2!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.startDate.second, 0)
        XCTAssertEqual(occurenceRange2!.endDate.day, today.day)
        XCTAssertEqual(occurenceRange2!.endDate.hour, 15 - 11)
        XCTAssertEqual(occurenceRange2!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-14:30:00"]
        let occurenceRange3 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange3)
        XCTAssertEqual(occurenceRange3!.startDate.day, today.day)   // only match on tomorrow
        XCTAssertEqual(occurenceRange3!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange3!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange3!.startDate.second, 0)
        XCTAssertEqual(occurenceRange3!.endDate.day, today.day)   // only match on tomorrow
        XCTAssertEqual(occurenceRange3!.endDate.hour, 14 - 11)
        XCTAssertEqual(occurenceRange3!.endDate.minute, 30)
        XCTAssertEqual(occurenceRange3!.endDate.second, 0)
    }

    func testDailyFirstOccurenceIn() {
        // start time : "2019-3-12 14:30:00"
        let today = DateInRegion(year: 2019, month: 3, day: 12, hour: 14, minute: 30, region: Region.current).date
        // start time : "2019-3-19 14:30:00"
        let aWeekLater = today.addingTimeInterval(TimeInterval.day * 7)
        let range = DateRange(startDate: today, endDate: aWeekLater)
        var occurence = Occurence()
        occurence.occurType = .REPEAT_DAILY
        
        occurence.intervalsInDay = ["11:00:00-13:00:00"]
        let occurenceRange1 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange1)
        XCTAssertEqual(occurenceRange1!.startDate.day, today.day + 1)   // only match on tomorrow
        XCTAssertEqual(occurenceRange1!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange1!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.startDate.second, 0)
        XCTAssertEqual(occurenceRange1!.endDate.day, today.day + 1)   // only match on tomorrow
        XCTAssertEqual(occurenceRange1!.endDate.hour, 13 - 11)
        XCTAssertEqual(occurenceRange1!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-13:00:00", "14:00:00-15:00:00"]
        let occurenceRange2 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange2)
        XCTAssertEqual(occurenceRange2!.startDate.day, today.day)
        XCTAssertEqual(occurenceRange2!.startDate.hour, 14 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange2!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.startDate.second, 0)
        XCTAssertEqual(occurenceRange2!.endDate.day, today.day)
        XCTAssertEqual(occurenceRange2!.endDate.hour, 15 - 11)
        XCTAssertEqual(occurenceRange2!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-14:30:00"]
        let occurenceRange3 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange3)
        XCTAssertEqual(occurenceRange3!.startDate.day, today.day)   // only match on tomorrow
        XCTAssertEqual(occurenceRange3!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange3!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange3!.startDate.second, 0)
        XCTAssertEqual(occurenceRange3!.endDate.day, today.day)   // only match on tomorrow
        XCTAssertEqual(occurenceRange3!.endDate.hour, 14 - 11)
        XCTAssertEqual(occurenceRange3!.endDate.minute, 30)
        XCTAssertEqual(occurenceRange3!.endDate.second, 0)
    }
    
    func testRepeatWeeklyFirstOccurenceIn() {
        // start time : "2019-3-12 14:30:00"
        let today = DateInRegion(year: 2019, month: 3, day: 12, hour: 14, minute: 30, region: Region.current).date
        // start time : "2019-3-19 14:30:00"
        let aWeekLater = today.addingTimeInterval(TimeInterval.day * 7)
        let range = DateRange(startDate: today, endDate: aWeekLater)
        var occurence = Occurence()
        occurence.occurType = .REPEAT_WEEKLY
        occurence.daysInWeek = [.sunday]
        
        occurence.intervalsInDay = ["11:00:00-13:00:00"]
        let occurenceRange1 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange1)
        XCTAssertEqual(occurenceRange1!.startDate.day, 17)   // only match on the first Sunday
        XCTAssertEqual(occurenceRange1!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange1!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.startDate.second, 0)
        XCTAssertEqual(occurenceRange1!.endDate.day, 17)   // only match on the first Sunday
        XCTAssertEqual(occurenceRange1!.endDate.hour, 13 - 11)
        XCTAssertEqual(occurenceRange1!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange1!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-13:00:00", "14:00:00-15:00:00"]
        let occurenceRange2 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange2)
        XCTAssertEqual(occurenceRange2!.startDate.day, 17)
        XCTAssertEqual(occurenceRange2!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange2!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.startDate.second, 0)
        XCTAssertEqual(occurenceRange2!.endDate.day, 17)
        XCTAssertEqual(occurenceRange2!.endDate.hour, 13 - 11)
        XCTAssertEqual(occurenceRange2!.endDate.minute, 0)
        XCTAssertEqual(occurenceRange2!.endDate.second, 0)
        
        occurence.intervalsInDay = ["11:00:00-14:30:00"]
        let occurenceRange3 = Occurence.firstOccurenceIn(range, with: occurence)
        XCTAssertNotNil(occurenceRange3)
        XCTAssertEqual(occurenceRange3!.startDate.day, 17)   // only match on the first Sunday
        XCTAssertEqual(occurenceRange3!.startDate.hour, 11 - 11) // convert Sydney local time to UTC time by minus 11
        XCTAssertEqual(occurenceRange3!.startDate.minute, 0)
        XCTAssertEqual(occurenceRange3!.startDate.second, 0)
        XCTAssertEqual(occurenceRange3!.endDate.day, 17)   // only match on the first Sunday
        XCTAssertEqual(occurenceRange3!.endDate.hour, 14 - 11)
        XCTAssertEqual(occurenceRange3!.endDate.minute, 30)
        XCTAssertEqual(occurenceRange3!.endDate.second, 0)
    }

}
