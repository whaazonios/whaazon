//
//  whaazonTests.swift
//  whaazonTests
//
//  Created by Can on 25/11/2015.
//  Copyright © 2015 Waigi. All rights reserved.
//
/*
import XCTest
import Alamofire
import SwiftyJSON

@testable import whaazon

class whaazonTests: XCTestCase {
    
    let baseURL = "http://m4e.whaazon.com.au:8080/MapForEventsServer1/realestate/"
    let area = "-33878344,151213748,-33871622,151220614"; // Area where events within are to be returned
    let timeWin = "1445662253652,9223372036854775807";    // Time interval where events  with overlapping time are to be returned
    let category = "all";									// always set to all in this context
    let domain = "RealEstate";							// Name of the domain
    let publishBaseTime = "1445662253652";				// return only the events published after this time, so old data are filtered
    let idList = "0";										// exclusion list, events with id in this comma separated list will not be returned.
    
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testReasestateDomainClient() {
        let expection = self.expectation(description: "Service Call Finished Expectation")
        let searchCriteria = "getEventsByCriteria/\(area)/time/\(timeWin)/category/\(category)/domain/\(domain)/publishBaseTime/\(publishBaseTime)/idList/\(idList)"
        
        
        Alamofire.request(.GET, baseURL + searchCriteria).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    let json = JSON(data)
                    print(json)
                    for (index, subjson):(String, JSON) in json {
                        let event = PropertyEvent(json: subjson)
                        print("\(index):\(event)")
                    }
                }
            case .failure(let error):
                print(error)
            }
            
            expection.fulfill()
        }
        
        self.waitForExpectations(timeout: 60.0) { (error: NSError?) -> Void in
            if error != nil {
                XCTFail("Expectation Failed due to timeout with error:\(error?.description)")
            }
        }
        /*
        XCTestExpectation *expectation = [self expectationWithDescription:@"Service Call Finished Expectation"];
        
        
        [[WOPropertyDomainClient sharedInstance] propertiesMatchingArea:self.area
        timeWin:self.timeWin
        category:self.category
        domain:self.domain
        publishBaseTime:self.publishBaseTime
        idList:self.idList
        success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"Properties found : %@", responseObject);
        if (responseObject) {
        NSMutableArray *properties = [NSMutableArray new];
        NSDictionary *json = (NSDictionary *)responseObject;
        for (NSDictionary *aProperty in json) {
        WOProperty *property = [WOProperty propertyFromJson:aProperty];
        if (property) {
        [properties addObject:property];
        NSLog(@"%@", property.description);
        }
        }
        NSLog(@"%lu valid properties.", properties.count);
        }
        XCTAssertNotNil(responseObject);
        [expectation fulfill];
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Network error");
        XCTAssertNil(error);
        }
        ];
        
        [self waitForExpectationsWithTimeout:60.0 handler:^(NSError *error) {
        if(error) {
        XCTFail(@"Expectation Failed due to timeout with error: %@", error);
        }
        }];
*/
    }
    
    
}
*/
