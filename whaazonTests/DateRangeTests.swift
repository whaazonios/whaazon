//
//  DateRangeTests.swift
//  whaazonTests
//
//  Created by Can Zhan on 6/3/19.
//  Copyright © 2019 Waigi. All rights reserved.
//

import XCTest
@testable import Whaazon

class DateRangeTests: XCTestCase {

    func testInit() {
        let range1 = DateRange(theDay: Date(), intervalsInDay: "12:30:0-14:30:0")
        XCTAssertNotNil(range1)
        let range2 = DateRange(theDay: Date(), intervalsInDay: "12:30-14:30")
        XCTAssertNil(range2)
        let range3 = DateRange(theDay: Date(), intervalsInDay: "12:30:0")
        XCTAssertNil(range3)
    }
    
    func testHasIntersection() {
        guard let range1 = DateRange(theDay: Date(), intervalsInDay: "12:30:0-14:30:0"),
            let range2 = DateRange(theDay: Date(), intervalsInDay: "13:30:0-14:30:0"),
            let range3 = DateRange(theDay: Date(), intervalsInDay: "14:30:0-15:30:0"),
            let range4 = DateRange(theDay: Date(), intervalsInDay: "10:30:0-12:30:0"),
            let range5 = DateRange(theDay: Date(), intervalsInDay: "10:30:0-12:29:0")
            else {
                XCTFail()
                return
            }
        XCTAssert(range1.hasIntersectionWith(range2))
        XCTAssert(range1.hasIntersectionWith(range3))
        XCTAssert(range1.hasIntersectionWith(range4))
        XCTAssert(range2.hasIntersectionWith(range3))
        XCTAssertFalse(range1.hasIntersectionWith(range5))
    }

}
